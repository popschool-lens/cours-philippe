# Cours de réseau

Philippe Pary, 2017

## Notion d’adresse IP

Les réseaux sont routés. Vous êtes reliés de point en point, les routes ne sont pas définies par avances.

Pensez à un GPS qui capte les infos traffic et vous fait changer de route en fonction des accidents ou de l’engorgement…

Chaque ordinateur est repéré sur le réseau par une adresse IP. Une adresse IP est une notion logicielle: ce n’est pas lié au matériel. L’adresse IP dont vous disposez n’a rien à voir avec votre matériel.

### IPv4

Le protocole IP historique est appelé IPv4, il a été définit en 1981 et en 2016 il reste le protocole le plus utilisé.

Les adresses sont notées sur 4 octets (valeur entre 0 et 255). Exemple: 82.230.128.1

Il existe une notion de sous-réseau. Un certain nombre de bits sont utilisés pour définir le réseau local. Par exemple: 192.168.0.0/24 définit un réseau local entre 192.168.0.0 et 192.168.0.255. Une machine de ce réseau n’a pas un accès direct à une machine dont l’adresse IP est 192.168.1.5, même si elles sont branchées sur le même hub !

Quelques adresses remarquables:
* 0.0.0.0 : adresse universelle (~= tout)
* 10.0.0.0/8 et 192.168.0.0/16: réseaux privés (pour la maison, des bureaux …)
* Toutes les adresses en 0 désignent le réseau en lui-même et ne doivent pas être utiliées (ex: 192.168.0.0)
* Toutes les adresses en 255 servent à contacter toutes les machines du réseau à la fois (ex: 192.168.0.255)
* 127.0.0.1 : désigne votre propre ordinateur ! *there is no place like 127.0.0.1*

Si on est sur le même réseau qu’un ordinateur, on peut lui parler directement. Si ce n’est pas le cas, on doit avoir une route pour y accéder.

Lancez la commande suivante:

    # route -n

Regardez quelle est l’adresse de la passerelle pour `0.0.0.0`. C’est l’adresse du routeur interne de Pop (ou de votre box si vous êtes à la maison)

    $ traceroute -m 60 bad.horse

(brrr…)

Prenons du recul deux secondes. 4 x 8bits = 32bits. 2 puissance 32 = 4 milliards d’adresses IP possible. Vous le sentez le problème ?

### IPv6

Le nouveau protocole IP est appelé IPv6, il a été définit en 1998. Il commence à peine à se répendre, notamment au travers de l’IoT.

Elles s’écrivent sous une longue litanie d’hexaoctets: 2001:db8:0:85a3:0:0:ac1f:8001

On peut aussi ne pas écrire les 0 qui se suivent: 2001:db8:0:85a3::ac1f:8001

Nota: le protocole IPv5 a été abandonné en cours de route

Les adresses sont notées sur 16 octets. On passe de 4'294'967'296 IPv4 à 40'282'366'920'938'463'463'374'607'431'768'211'456  IPv6.  
Il faudrait 667 millions de milliards d'appareils connectés sur chaque millimètre carré de la surface de la Terre pour épuiser IPv6…

Quelques adresses remarquables:
* fc00:: : adresses locales
* 2000::/3 : adresses Internet (le /3 indique que seul le premier 2 est important)
* ::1 : votre propre ordinateur ! *there is no place like ::1*

### DNS

Retenir des adresses IP, c’est pénible. On a donc inventé 1983 le *Domain Name Service*

On associe une chaîne de caractères, un nom de domaine (alias DN) à une adresse IP.

Un nom de domaine se lit de la droite vers la gauche: google.fr est frère de lemonde.fr, alors que google.de n’a rien à voir avec lui !

Le service repose sur 13 serveurs racines gérés par des entités US, européennes et japonnaises. Ils orientent vers les serveurs gérant les TLD (top-level DNS) : .fr, .com, .org …

Les serveurs qui gèrent les TLD orientent vers les serveurs pour chaque domaine réservé: lemonde.fr, google.fr etc.

Le propriétaire de chaque nom de domaine gère sa zone DNS et peut créer des sous-domaine: m.lemonde.fr, www.lemonde.fr etc.

Et concrètement, votre fournisseur d’accès fait du cache DNS : il vous donne l’adresse de serveurs DNS placés sur son réseau dont l’accès est plus rapide pour vous. En revanche, il connaît tous noms de domaines que vous visitez, il peut également vous mentir (par décision de justice pour bloquer un site web par exemple).

Vous pouvez utiliser les DNS proposés par Google (uniquement les blocages US) ou ceux d’OpenDNS (pas de blocages). Par contre, Google comme OpenDNS peuvent connaître tous les noms de domaines que vous visitez.

Vous pouvez créer un serveur DNS qui vous soit propre pour échapper aux DNS de votre FAI. Dans ce cas, l’un des 13 serveurs racine et l’un des serveur gérant les TLD saura quel noms de domaines vous visitez.

Internet a été créé par des universitaires et des militaires qui avaient oublié de penser au p0rn …

Faites les commandes suivantes:

    $ dig m.lemonde.fr
    $ dig www.google.fr

Il y a plusieurs types d’enregistrements DNS:

* A: ipV4
* AAAA: ipV6
* CNAME: alias d’un autre enregistrement
* MX: pour les mails (*touche pas à ça, petit con*)
* TXT: pour vérifier que le domaine vous appartienne bien ou pour sécuriser des échanges, on vous en demandera de temps en temps 

    $ dig MX pop.eu.com

(Oh, on connaît le fournisseur mail de pop …)

Il existe une base internationale des noms de domaines qui permet d’avoir quelques informations sur un domaine

    $ whois pop.eu.com

(oh bah ça alors, on connaît le registrar, la date de création et le nom de la personne qui gère !)

### Et en dessous, comment ça marche ?

Très vite alors hein …

Chaque carte réseau a une adresse matérielle (adresse MAC). Les routeurs retiennent l’asssociation entre une adresse IP et une adresse Mac. 

Votre modem connaît l’adresse de votre nœud de raccordement (DSLAM), votre nœud de raccordement est raccordé au backbone de votre FAI (fournisseur d’accès Internet) qui est connecté au reste du monde via des accords de peering (contrats passés entre ceux qui gèrent l’infrastructure de télécommunications)

Encore en dessous, il y a des protocoles pour les signaux électroniques (comment on transforme du courrant électrique ou des oscillations de lumière en 0 et 1) et après des normes électriques/optiques … bref tout est logique d’un bout à l’autre. Aucun petit lutin magique derrière tou ça, hélas.

Localement, si deux ordinateurs prennent la même IP, ça crée un conflit réseau (two MAC one IP, c’est pire que two girls one cup)

C’est donc sur les frêles épaules des routeurs que repose le lien entre votre ordinateur et le reste d’Internet.


### Pro-tip: dépanner une panne réseau …

Ouhlala, votre ordinateur ne se connecte plus à un site web

1. Tentez un `$ ping google.com` -> vous êtes connecté, c’est le site web qui est mort (ou qui vous a bloqué)
2. Tentez un `$ ping 8.8.8.8` -> vos DNS sont morts
3. Tentez de pinger votre route -> votre routeur est mort
4. Vous n’avez même pas une adresse IP locale -> votre routeur est mort
5. Tentez `$ ping 127.0.0.1` -> votre système d’exploitation rend l’âme. Ça ne devrait jamais arriver …

