# Introduction à Bash

Philippe Pary. PopSchool Lens  
http://pop.eu.com CC-BY-SA

## Présentation des scripts Bash

Il est possible de créer des fichiers visant à automatiser certaines tâches:
* sauvegarde nocturnes
* corrections des droits sur un répertoire
* récupération de fichiers sur un FTP
etc.

Un script Bash est un ensemble de commandes. Il peut carrément être utilisé comme langage de programmation avec des structures de contrôle et des variables.

ABS, Advanced Bash Scripting, est une bible pour la création de scripts: <http://abs.traduc.org/abs-fr/>

## Rédaction d’un premier script

1. Créer un fichier appelé *script0.sh* et y copier le contenu suivant  
    ls
2. Dans un terminal, lancer le fichier en allant dans le répertoire où il se situe  
    $ sh script0.sh
3. Lancer à nouveau le fichier mais en utilisant cette fois  
    $ ./script0.sh

Si un message vous dit *Permission non-accordée*, exécutez la commande suivante, sur laquelle nous reviendront ultérieurement:  
    $ chmod +x script0.sh

## Rédaction d’un premier script utile

Ce script déplace le contenu d’un dossier ~/tmp dans un dossier ~/tmp.bkp qui est d’abord vidé.

    #!/bin/bash
    
    rm -Rf ~/tmp.bkp
    mv ~/tmp ~/tmp.bkp
    mkdir ~/tmp

1. Créer le fichier
2. Le lancer manuellement
3. Prendre un café, se dire que c’était vraiment facile (ou paniquer)

Pour votre culture:  
*#!/bin/bash* est un **sha-bang**. Rien de sexuel, ça signifie que toutes les lignes du script seront précédées de */bin/bash*

## Tâches planifiées

Il arrive que des scripts soient tout de même utiles lancés à la main, mais la plupart du temps ils sont surtout utiles quand lancés via une tâche planifiée.

C’est le rôle de cron (ChRONo) et des fichiers crontab (cron table). On trouve deux grands types de fichiers crontab:
* /etc/crontab et les contenu des dossiers /etc/cron\* : ce sont les cron système
* /var/spool/crontabs/\* : ce sont les cron des utilisateurs

Attention, CRON ne peut se lancer que si l’ordinateur est allumé. Éteint ou en veille, la commande ne se lancera pas. Et elle ne sera pas lancée quand l’ordinateur sera rallumé: si l’ordinateur n’est pas allumé à l’heure planifiée pour la tâche, elle n’est simplement pas éxecutée.

Chaque ligne d’un crontab est composée comme suit:  

    # m h  dom mon dow   command

* m: minute. À quelle minute lancer la commande. C’est obligatoire
* h: heure. À quelle heure lancer la commande. On peut mettre \* pour dire *« toutes les heures »*
* dom: day of month. Quel jour du mois (1 à 31). On peut mettre \*
* mon: month. Quel mois (1 à 12). On peut mettre \*
* dow: day of week. Quel jour de la semaine (1 à 7). On peut mettre \*

cront tourne autour du logiciel *crontab*. Ce logiciel a deux paramètres principaux: *-l* pour voir le fichier, *-e* pour éditer le fichier.  
Comme d’habitude, *man crontab* ou *crontab -h* vous montrera bien plus d’options !

Nous allons faire lancer notre petit script utile toutes les semaines, le lundi matin à 9H.

1. Lancer la commande suivante:  
    crontab -e
2. À la dernière ligne, indiquer les paramètres pour que la commande se lance seule chaque lundi matin à 9H

## Conclusion

On sait créer un script, on sait ce qu’est un sha-bang, on sait créer une tâche planifiée.

Reste à connaître plus d’options et savoir faire des scripts un peu plus évolués
