# Cours de JS3: il faut avancer …

## les tableaux

Un tableau est une liste de variables. Un tableau de variables quoi …

On y accéde via un opérateur numérique: de 0, premier élément, à N, dernier élément. Le tableau est donc de longueur N+1 …

    var myTab = ['pomme', 3, true, 'poire', -1.5]
    for (i=0; i < myTab.length; i++) {
        console.log(myTab[i]);
    }

Vous l’avez ?

### Exercice 13

Vous vous souvenez que `getElementsByTagName` et `getElementsByClassName` renvoient des tableaux d’objet ? Si non, vous l’apprenez

0. On git
1. HTML/CSS/JS
2. On crée 4 bouton: *all green*, *red go green*, *blue go green* et *reset*
2. On crée 10 `div`, une sur deux est de class `red` et l’autre de classe `blue`
3. Le bouton *all green* rend toutes les div vertes
4. Le bouton *red go green* rend vertes les div *red*
5. Le bouton *blue go green* rend vertes les div *blue*
6. Le bouton *reset* remet tout comme au début

tip: utilisez la propriété `style`. Pour le reset : `element.style = ''`

## forEach (et fonctions de callback)

Il existe un foreach très pratique qui parcourt un tableau (notez l’admirable syntaxe)

    monTableau = [1, 3, 5, 7];
    monTableau.forEach(console.log);

Le première paramètre de forEach est une fonction. Elle peut prendre de 0 à 3 arguments:

    var myFunction = function(valeur,position,tableauInitial) {
        console.log("Numéro en cours: " + valeur + " , position en cours: " + position);
        console.log("Tableau sur lequel on boucle: " + tableauInitial);
    };
    monTableau = [1, 3, 5, 7];
    monTableau.forEach(myFunction);

On remarque que la fonction est passée comme un argument, c’est ce qu’on appelle une fonction de *callback*. Les arguments de la fonction de callback dépendent de la fonction que l’appelle. `forEach` passe 3 arguments, c’est comme ça … d’autres fonctions passeront d’autres arguments (nombre, type etc.)

Remarquez ici, une fonction se comporte comme une variable. C’est très différent de PHP ou les variables et fonctions sont deux choses totalement distinctes. C’est normal, en réalité ce sont tous les deux des objets. En Javascript, tout est objet.

On va en causer en masse à la reprise en août :)

### Exercice 14

Refaites le 13, mais avec un forEach

tip: il faut convertir la liste des éléments en tableau via la fonction `Array.from()`

## La logique booléenne

George Boole a créé en 1854 l’algébre de Boole et la logique booléeenne. Il créa également plus tard un jeu et un type de films.

Le but est de créer des conditions *vrai* ou *faux*. Pas de neutre. On s’en sert dans les `if` / `else if` / `else`

* `true` est vrai …
* `false` est faux …
* 1 est vrai
* 0 est faux
* les nombres positifs sont vrais
* les nombres négatifs sont faux

Comparateurs logiques. On compare deux à deux des éléments
* &&, AND : ET logique. Les deux conditions comparées doivent être vraies pour que le résultat soit vrai
* ||, OR: OU logique. L’une des deux conditions doit être vraie pour que le résultat soit vrai

Exemple: tous les garçons de plus de 21 ans et les filles de plus de 18 ans

    if ((sex == "M" && age > 21) || (sex == "F" && age > 18))

### Exercice 15

Ici un exercice très convainquant …

## JSON

JSON est un format de données. JavaScript Objet Notation.

Ça permet de poser en texte plat des variable même complexes comme des tableaux ou des objets.

C’est un format assez simple à écrire pour un humain. RTFM si vous voulez en savoir plus

Deux fonctions à connaître:

* JSON.stringify(myVar): encode une variable en JSON
* myVar = JSON.parse(myJSONVar): lit du JSON et le transforme en variable javascript

### Exercice 16

Se dire que ça nous fait une belle jambe.

0. Regarder sa jambe
1. La trouver belle
2. Se le dire dans un somptueux soliloque

## Local storage

Le local storage, et son cousin le session storage, sont un moyen d’enregistrer des données sur l’ordinateur qui exécute le JavaScript.

Il se base sur deux fonctions:
* localStorage.setItem("localVariableName",myVar)
* myVar = localStorage.getItem("localVariableName")

Ceci permet de sauvegarder une variable entre deux visites de la page, ou du passage d’une page à l’autre.

### Exercice 17

0. Git
1. Supplier Maxime pour qu’il partage son code `jul`
2. Constater que Maxime n’est qu’une morue, aspirer son site web (au pire, depuis <https://pachyderme.net/spinjul/>
3. Modifier le site pour utiliser du localStorage et sauvegarder le score de l’utilisateur à chaque clic
4. Modifier le site pour charger le score sauvegardé à chaque chargement de la page

## Les objets

On a vu une variable. Une variable contient une valeur, c’est facile … *i = 5*, *myName = document.getElementById('nameInput').value* …

On a vu un tableau. Un tableau contien une série de valeurs, ça se comprend … *myDivs = document.getElementsByClassName('red')*

Un objet c’est le super saiyan niveau 3 du tableau et de la variable

[Un objet JS, illustration](https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fvignette4.wikia.nocookie.net%2Fdragonball%2Fimages%2F9%2F9a%2FSS3_full.png%2Frevision%2Flatest%2Fscale-to-width-down%2F400%3Fcb%3D20150626092007&f=1)

Un objet contient plusieurs valeurs, mais aussi des fonctions (appelées *méthodes*)

### Exercice 18

On va autopsier un élément HTML …

1. Récupérez du code de quelque part
2. Claquez des `console.log` sur plein d’éléments
3. Observez méticuleusement
4. Lisez <https://developer.mozilla.org/fr/docs/Web/API/HTMLElement> et prenez en compte toute la profondeur …
5. C’est tout. Sachez que ça existe, on creuse le sujet à la rentrée !

## Gros TP de l’après-midi

Je vous préviens, vous allez avoir besoin de mon aide. Puis de celle de Daishi comme je ne serai plus là dans 4 jours

0. Git, HTML/JS/CSS
1. Créez un formulaire pour générer des héros … Un nom (libre), une agilité, une force et une endurance. La somme des 3 doit faire 100
2. Le bouton enregistrer ajoute une div avec les informations du héros et enregistre en local storage
3. Faites en sorte qu’au chargement de la page, une div s’affiche pour chaque héro enregistré localement
4. Créez un second formulaire avec deux champs `select` qui contiennent chacun tous les héros
5. En cas d’ajout d’un héros, les deux champs sont complétes, évidemment
6. En cas de validation, comparez la force et l’endurance de chaque héros; le plus grand des deux l’emporte. Mais il y a une chance de *100-agilité du défenseur* d’aboutir à un match nul
7. Affichez le résultat du cobat
8. Parce que vous êtes des gueudins, ajoutez la possibilité d’ajouter une description au héros, une classe (qui pourrait influer les caractéristiques!) et même pour les plus téméraires, une image

