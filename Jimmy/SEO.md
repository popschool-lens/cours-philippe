# Atelier SEO

Philippe Pary

## SEO ?

search engine optimisation: être trouvé sur les moteurs de recherches

Objectif: n°1 ou n°2 sur google

* 98% de parts de marché en France : les autres moteurs de recherche n’ont aucun intérêt …
* 80% des clics concernent le 1er lien, 98% sont dans les 5 premiers. Être en seconde page, c’est avoir disparu …

Avant de se lancer …

* la SEO demande beaucoup d’efforts techniques
* … et un investissement dans le temps (pas un stage de 2 mois donc)
* selon votre site web, ça peut être inutile: réseaux sociaux, bouche à oreille …

## Référencement payant

Je vais passer rapidement sur ce point: c’est payer de la pub. Des campagnes google adwords par exemple. *Référencement payant*, c’est le terme marketing pour ne pas dire *payer de la publicité*

Grosso merde, retenez que ça marche par enchères et par mots-clefs. C’est souvent assez abordable, on parle de dizaines d’euros de budget…

## Référencement naturel

Le but est de faire connaître son site web par son contenu et qu’il soit *indexé* par les moteurs de recherche pour son contenu

### indexation du web (alias *crawling*)

Les moteurs de recherchent parcourent le web avec des logiciels appelés *spiders*. Ce sont des *bots*.

Ils parcourent le web de lien en lien, analysent les pages pour connaître leur contenu. Ils regardent aussi la popularité de la page (nombre de liens qui y amènent depuis l’extérieur) pour estimer son *ranking* (Wikipédia a un ranking très élevé)

Lors d’une recherche d’un utilisateur, le moteur de recherche regarde les pages pertinentes sur un domaine et les trie par leur ranking.

## Améliorer son contenu

C’est le très gros du travail …

### La taxonomie et la structure

+ coder proprement, les spiders apprécient les pages bien codées
+ respecter les titres (hN), les `div` sont des divisions de la page, les `p` des paragraphes de texte
+ n’oublier aucun `alt` sur les images : les images ne sont pas vues par les spiders
+ utiliser un vocabulaire riche et varié en thème avec votre sujet afin d’être référencé même sur des mots rares. Vous avez un salon de coiffure ? Pensez à ce que *brushing*, *tresses* ou le nom des footballers avec des coupes à la mode soient présents !
+ avoir une section `head` propre: `title` pertinent, les `meta` lang et charset sont importants
+ avoir des URLS pertinentes: page1.php n’a aucun sens par contre nos-offres-coiffure-homme.php en a plus
+ avoir des textes de liens pertinents: *<a href="…">cliquer sur ce lien</a>* ne dit rien *<a href="…">découvrez notre offre pour les hommes</a>* c’est mieux
+ **ultimate pour le seo** avoir un site web accessible : <https://www.w3.org/WAI/>

- démultiplier les pages pour essayer d’améliorer artificiellement la présence: mieux vaut moins de pages avec plus de contenu qu’une profusion de pages similaires qui vous amènera des sanctions de ranking
- tenter de cacher du texte (blanc sur fond blanc, `div` en `hidden`…) : vous allez vous faire **défoncer la gueule très très violemment** voir complètement retirer du moteur de recherche. En effet, ces analyses CSS coûtent `cher` à réaliser pour les `spiders` et ne sont pas systématiques. Vous pourriez passer entre les mailles du filet, mais si vous vous faites attraper …

### Responsive, pour de vrai

Les moteurs de recherchent favorisent **beaucoup** les sites réellement responsive

+ avoir un CSS prévu pour les petits écrans
+ gérer la vitesse de chargement: images réduites pour les petits écrans, CSS et JS minifiés
+ respecter les recommandations AMP: <https://www.ampproject.org/>

### Javascript

Javascript est détesté par les spiders: il coûte très cher à évaluer …

+ prévoir une version de votre site web fonctionnant sans javascript
- avoir des liens fonctionnant avec des `onClick` ou tout autre événement
- les contenus accédés en AJAX ne seront pas vus: prévoyez un accès sans AJAX

## Fichiers particuliers

### Fichier robots.txt

Le fichier robots.txt est lu par les spiders qui peuvent le prendre en compte. Il indique quels contenus ignorer

ex:
    User-agent: *
    Disallow: /wp-admin/
    Disallow: /wp-content/
    Allow: /wp-content/uploads

Attention:
* Les bots peuvent ignorer le robots.txt: ce sont des demandes que vous formulez
* Tout le monde peut lire ce fichier: il serait bête d’y écrire l’adresse d’un dossier que vous souhaitez garder sercret …

### Fichier .htaccess

Pour les serveurs apache, le fichier .htaccess vous permet de bloquer l’accès à un dossier ou à un fichier, à mettre en place des redirections par adresse IP ou mot de passe, modifier le comportement de PHP, mettre en place des redirections d’une URL vers une autre etc.


## En vrac

* Les spiders ne savent pas utiliser les formulaires
* Les paramètres GET font des URLs et donc des pages différentes : attention aux contenus dupliqués qui vous plomberaient
* Attention à la pérénité de vos pages, ayez au moins une page 404 qui ramène vers votre page d’accueil et qui dispose d’un moteur de recherche interne
* HTTPS par défaut : google favorise les sites accessibles en HTTPS et encore plus ceux qui redirigent de HTTP à HTTPS !
* Faites un choix entre www.mondomaine.com et mondomaine.com : ce seront des contenus dupliqués sinon (et ouais)
* En cas de multilingue, préférez des sous-domaine (en.mondomaine.com, de.mondomaine.com …) qui éviteront d’être jugés en contenus dupliqués tout en permettant d’être bien référencé dans toutes les langues (évitez la page traduite selon l’utilisateur: vous ne seriez référencé que sur la langue par défaut)
* Dans votre moteur de recherche, tapez site:www.mondomaine.com pour voir toutes les pages connues pour ce nom de domaine
* Utilisez les outils google professionnels pour renseigner les informations sur une entreprise (horaires, photographies …)

## Améliorer son ranking

+ avoir des liens depuis l’extérieurs de sites qui ont un bon ranking
+ être lié depuis des pages externes ayant un rapport avec le thème du site (ex: article de presse sur votre entreprise)
- être lié depuis des annuaires payants (déréférencement)

Concrètement c’est un travail de longue haleine: se faire lier depuis les sites proches (association de commerçants, CCI etc.), à chaque fois que quelqu’un parle de vous lui demander de rajouter un lien vers votre site web etc.

Un site statique aura très vite fait le tour de ce qui peut être fait en ranking. Si vous avez une actualité sur le web (articles récents etc.), une stratégie consiste à penser aux relais éventuels quand vous rédigez votre actualité. Un bloggeur aura-t-il envie de faire un article sur votre actualité ou votre nouveau contenu ?

## Les outils SEO

Ils visent à vous faire comprendre ce qui va et ne va pas sur votre site web …

* Google analytics, Piwik: traking de l’activité de vos utilisateurs. Mais en perte de vitesse (respect de la vie privée)
* Analyseurs de logs du serveur web: AWStats, Octopussy, Urchin …
* Outils d’analyse: yoast, screaming frog, moz.com …


