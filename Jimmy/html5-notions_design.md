# Notions de design

Philippe Pary 2017

# Box-sizing:

<https://developer.mozilla.org/fr/Apprendre/CSS/Introduction_%C3%A0_CSS/Le_mod%C3%A8le_de_bo%C3%AEte> 

<https://developer.mozilla.org/fr/docs/Web/CSS/box-sizing?v=control>

* Margin: espaces entre la bordure de l’élément et les autres éléments (extérieur)
* Padding: espaces entre la bordure de l’élément et son contenu (intérieur)
* Bordure: notez que la bordure peut avoir sa propre largeur …

Par défaut, la `width` calcule la largeur du contenu, sans les bordures, sans le padding, sans les margins (`content-box`)

On peut utiliser le `box-sizing: border-box` pour que la `width` calcule la largeur avec le padding, avec les bordures mais sans les margins.

1. Créez un projet github.com, `push` après chaque étape
2. Créez une page web avec une `<div>` principale qui contiendra tout le reste du code
3. Ajoutez-y deux `<div>`, attribuez leur un `float: left;`, une bordure de 20px noire, un padding de 20px, un margin de 20px, ainsi qu’une largeur de 80%;
4. À la première indiquez explicitement un `box-sizing: content-box` (explicitement car c’est la valeur par défaut) et à l’autre indiquez un `box-sizing: border-box`
5. Constatez la différence. Utilisez la console de développement sur votre PC de dev pour regarder les informations de modèle de boîte.

**Concrètement, le fonctionnement que vous allez préférer est `border-box`**

# Notions de design

Voici quelques notions jetées en vrac …

## Un logo

Facile à reconnaître, il est généralement présent en haut à gauche, la première zone où va le regard.

On a également la possibilité de définir un `favicon` qui identifiera la page (flux RSS, onglets …)

Le `favicon` peut être en JPG, PNG ou ICO (le ICO est un BMP ou un PNG)

1. Trouvez ou créez un fichier de logo
2. Redimensionnez le avec la commande `convert`
3. Créez une page web, ajoutez-y votre logo dans un header ainsi qu’une favicon

(pour savoir la syntaxe d’une favicon ou vous souvenir de `convert`, RTFM)

## Une typographie

On dit que le choix d’une typographie adaptée, c’est avoir fait la moitié de l’effort de design. C’est pas loin de ne pas être faux.

* Jamais plus de trois typos par page (titres, texte, alt-text)
* Si vous voulez faciliter la lecture, `align: left`. `justify` trouble la lecture car il fait varier la taille des espaces.
* Toujours pour la lecture, mettez le texte à lire sur 80 lettres (`width:80em;`), c’est un constat scientifique : au delà des 80 signes, l’œil est moins attentif
* Mettez votre texte dans un gris foncé plutôt qu’en noir, ça fait bien … `#222222` par exemple

Les familles de typos:
* Évitez les typographies manuscrites ou trop originales. C’est toujours une mauvaise idée
* Les typos de type *sans* (sans empatement en bas des lettres) sont plus agréables à l’œil et conseillées pour les titres
* Les types de type *serif* à empattement sont conseillées pour les textes à lire
* Les polices de type *monospace* sont utiles pour afficher du code informatique

Notions jetées en vrac:
* Interlignage : hauteur entre chaque ligne (`line-height`)
* Gris typographique: au plus c’est sombre, au plus le texte est serré et compliqué. Ex: Le monde diplomatique a un niveau de gris typographique élevé, Closer a un niveau de gris typographique très léger
* Graisse: la mise en gras. Via `bold`, `bolder` ou des chiffres, de `100` (normal) à `900`. `bold` vaut `400`
* Italique, souligné … est-il besoin de les présenter ?
* `<em>` n’est pas une simple mise en italique, c’est une emphase, un mot important. `<strong>` n’est pas une simple mise en gras, c’est un mot qu’on fait ressortir visuellement (pour faciliter la lecture en diagonale par exemple)

1. Créez un nouveau projet sur github.com
2. Créez une page HTML
3. Mettez votre nom en titre de la page
4. Générez 5 paragraphes de *lipsum* via <http://lipsum.com> ou via un plugin lipsum de votre éditeur de texte
5. Affichez ce texte sur 80 colonnes
6. Trouvez une typographie *serif* pour le texte, une en *sans* pour le titre (via Google fonts, dafont.com ou tout autre site).
7. Utilisez les typos dans votre CSS. Il doit être présent dans votre projet web et non pas récupérée en CDN (via un site web externe). Une en *serif* le texte, une en *sans* pour le titre (via Google fonts, dafont.com ou tout autre site).

## Choix des couleurs

La mode est aux palettes ultra-simplifée: 2 couleurs outre le noir&blanc. Ça simplifie l’accessibilité (daltonisme et autres déficiences visuelles)

Notez que ces deux couleurs peuvent être variées en luminosité ou intensité pour des effets (par exemple pour un :hover)

La mode précédente était basée sur des trios de couleurs, on trouve encore de nombreux outils pour trouver des trios harmonieux.

1. Reprenez votre projet
2. Allez sur un outil du genre <http://paletton.com> et créez une palette (bicolore, tricolore ou autre)
3. Adaptez votre CSS à deux couleurs, observez l’efficacité du résultat

## Et en vrac totalement à l’arrache

* Le visiteur parcours l’écran en Z : de en haut à gauche vers en bas à droite.
* Ne surchargez pas visullement. Au moins il y en a, au plus votre site est agréable à regardez
* Ne surchargez pas d’effets de style. Un CSS compliqué, ça se ressent. Ça fait un site bordélique. Les jolis sites tiennent sur très peu de CSS …
* Pas d’auto-play de son. Jamais. Jamais.
* Grande image d’arrière-plan. Vidéo d’arrière plan, ça marche aussi !
* Observez les sites que vous visitez, tentez de reproduire quand quelque chose vous plaît

1. Reprenez votre projet
2. Mettez un menu de haut de page en `flex-box` comme fait lors du cours html4. Positionnez ce menu en `fixed` pour qu’il soit toujours visible. Pensez à y ajouter votre logo.  
Liez vers des sections de votre code via des ancres (`<a name="introduction"></a>` à l’endroit où commence l’intro, `<a href="#introduction">intro</a>` pour créer le lien vers l’introduction). N’oubliez pas de créer une ancre pour le haut de page que vous mettrez sur le logo
3. Créer une div de 100% de hauteur avec une image ou une vidéo. Ajoutez un texte par-dessus qui indique de quoi parle votre page
4. Ajoutez une icône avec une flêche vers le bas afin d’inciter l’utilisateur à scroller. Mettez un lien vers la prochaine section sur cette flêche.
5. Appliquez vos deux typos au texte et aux titres de cette page
6. Trouvez un logo au projet, mettez la dans le menu haut de page et en `favicon` de la page
7. Ça commence à avoir de la gueule, non ?
