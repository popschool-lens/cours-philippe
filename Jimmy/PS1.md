# Usage de Prestashop

Philippe Pary
CC-BY-SA

## Objectifs du cours

* Comprendre les bases du e-commerce
* Appréhender Prestashop, un solution e-commerce phare
* Monter en compétence sur la personnalisation de Prestashop

## Quelques généralités

* Stratégie de vente
* Rassurance client
* Mode de paiement
* Livraison
* Disponibilité
* SAV

### Stratégie de vente

Accompagnez votre client dans sa réflexion. Vraiment, n’hésitez pas à refuser l’affaire: une boutique en ligne coûte cher et les retours financiers peuvent être longs à développer … voir ne jamais exister.

Votre client a-t-il besoin d’une boutique à lui ? Ne peut-il pas préférer une bonne stratégie de présence sur Amazon ou le bon coin ? Son métier a-t-il des *marketPlaces* connues (le petit artisanat en regorge !) etc.

**Une boutique e-commerce est un gros investissement financier et humain**

Ça peut ruiner un petit commerce et créer une situation où tout le monde est perdant …

### Rassurance client

Attention, il y a client (le commercant qui vous paie) et client (l’acheteur qui paie le client)

On est certains de créer une boutique e-commerce ?

Alors on va sortir **un site web nickel**. Pas un pixel de jeu dans le CSS, tout est optimisé, HTTPS configuré partout, témoignages clients, commentaires et évaluations des produits, animation sur les réseaux sociaux, mise à jours régulière du site web etc.

Le site e-commerce doit rassurer le client potentiel : s’il a le moindre doute, il ira chez un concurrent. Que le CSS soit moche, que la dernière mise à jour ait 6 mois, qu’il n’y ait que des commentaires négatifs (ou pire: pas de commentaire du tout) et c’est mort.

Pour les mises à jours, l’animation sur les réseaux sociaux etc. votre client doit vous désigner un interlocuteur qui sera *community manager* et qui consacrera au moins une demi-journée par semaine au sujet (bon … ça sera le vendredi après-midi … bon, c’est mieux que rien)

**Un site e-commerce coûte cher car on ne peut badiner avec rien**

Le site e-commerce n’est pas plus compliqué que le site web classique, c’est juste que le client ne doit renoncer à aucune des options.Si votre client a des oursins dans les poches, dénoncez immédiatement le contrat. Un site e-commerce qui ne marche pas peut nuire à votre image pro.

### Mode de paiement

On touche au nerf de la guerre. Les paiements …

* Vous pouvez vous contenter de paypal: ça fait un peu peur, mais ça passe
* Incitez votre client à renoncer rapidement aux frais de ports. C’est psychologique, les gens n’aiment pas payer les frais de port
* Incitez votre client à souscrire à une assurance et à le faire savoir partout sur le site web. Savoir que la MAAF assure les achats sur cette boutique, ça va rassurer à mort les clients potentiels !
* Les modules de paiement des banques sont payants, mais si la boutique fait un peu de volumme (1000€ et plus par mois), ça vaut largement le coût. Ça fait nettement plus sérieux
* TESTEZ EN VRAI, et régulièment. Pas vous évidement, le client hein. Tant pis pour les frais bancaires perdus. Il est important que votre client sache que ça marche et que les clients finaux qui se plaignent sont des boulets. Vous n’y êtes pour rien ! (je vous assure, ce point est important)
* Soyez force de proposition sur les CGV web de votre client. Si ce dernier maîtrise certainement la vente physique, il ne connaît rien aux règles du web … (délais de rétractation, engagements en obligation de moyen et résultat, remboursement, retours …)


### Livraison

Interdisez à votre client de promettre la lune !

Indiquez clairement sur le site web un délais de livraison raisonnable. Ça se calcule comme ça: 
* temps moyen de prise en compte d’une commande par votre client. C’est souvent 1 à 2 jours …
* temps moyen de livraison du fournisseur de votre client. Car même s’il jure le contraire, il sera souvent en rupture de stock. Les gens commandent sur Internet ce qui est dur à trouver en magasin … 2 à 3 jours …
* temps moyen de préparation de la commande. Une fois ce qui manquait arrivé, il faut encore faire un colis, mettre un bon de livraison dedans, ajouter des goodies qui ne coûtent rien (stickers & cie) … 1 à 2 jours
* temps moyen de livraison par Chronopost. Car votre client ne va jamais payer UPS ou un autre service rapide :) 2 à 3 jours

On arrive donc à un délais de livraison qui varie de 6 jours ouvrés au mieux à 10 jours ouvrés au pire. Une bonne pratique est de promettre la livraison sous 15 jours.

Incitez votre client à mettre des petits goodies qui ne coûtent rien, du genre stickers (75€ les 1000…). Ça fait vraiment plaisir quand on ouvre son colis d’avoir des petits bonus à la con :)

Incitez le également à mettre en place un cash’n carry: les clients achètent en ligne et peuvent venir retirer le lendemain dans le magasin. Ça permet d’avoir un commerce virtuellement ouvert H24.

### Disponibilité

Votre client vous assure qu’il a tout en stock et que ça va aller. Il se plante.

Sur Internet les gens commandent ce qui est rare et qu’il n’aura certainement pas en stock.

Murphy aidant, il aura une commande sur Internet d’un produit le jour même où le dernier exemplaire aura été vendu de la boutique. 

Vérifiez le sérieux de la disponibilité des produits de votre client très rapidement, réservez-vous le droit de rompre le contrat s’il est toujours en rupture de ses produits. Là encore pour préserver votre image pro.

### Service après-vente.

Vous n’êtes pas le service après-vente de la boutique. Point.

Si vous vous retrouvez à l’être, facturez un à deux salairés à temps plein à votre client: entre 50000€ et 75000€ par an. Ou renoncez au contrat pour préserver vos marges et surtout votre santé mentale.

## Installez le logiciel (30m)

Comme Wordpress en fait. Les doigts dans le nez.

## Découvrez le logiciel (1H)

Une heure ne suffit pas à appréhender ce qui est disponible. Pire encore que pour Wordpress…

**ALORS PASSEZ Y UNE PUTAIN D’HEURE**

## Installez une version de développement (30m)

La routine habituelle quoi

## Personnaliser le logiciel (1H)

0. (pushover) Trouver le dossier des templates
1. (easy) Modifier le template comme un barbare (directement les fichiers du template)
2. (normal)Copier un template et créer le sien (clean)
3. (hard) Créer un fichier de template qui peut être installé par un camarade ou sur une nouvelle installation du logiciel
4. (godmode) Créer from scratch son fichier template ou personnaliser un template existant au point de pouvoir expliquer chaque fichier

## Intégrer un module Paypal (3H)

Il vous faut passer votre compte paypal en pro: <https://www.paypal.com/fr/merchantsignup/upgradeBusinessAccount>

Mettez-le dans un mode de démonstration, faites une transaction réelle à 1 centime

Quelques explications:
* Passez par le module paypal de Prestashop qui fera une demande d’identifiants à PayPal, demande que vous validerez
* Côté Paypal vous devez générer des identifiants: un identifiant, un mot de passe et une signature qui doivent garantir les échanges entre Prestashop et Paypal. Tout ça aura été fait au point précédent.
* Il existe un mode *sandbox*/*bac à sable* pour simuler des achats/ventes sans que ne circule d’argent. Ça peut largement suffire, faire une vraie transaction, c’est pour le plaisir de constater que vous avez causé un débit sur votre compte, fut-il modeste !
* Attention: paypal coûte cher en frais. Préférez-y le TPE virtuel de la banque, la banque propose probablement un module pour Prestashop. Il est certainement payant, mais vous vous en foutez, c’est le client qui paie. Paypal c’est la solution du pauvre (ou de l’interdit bancaire, ce qui est assez fréquent)
