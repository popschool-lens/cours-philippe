# Trouver du travail ou un stage

Philippe Pary, mai 2018

## Se préparer

### Avoir une adresse mail correcte

* Avoir un mail du genre: nom.prenom@mondomaine.fr
* Avoir son nom qui s’affiche correctement (identité d’expéditeur dans le client mail)
* Créer l’alias sur son client mail (gmail, outlook, thunderbird etc.)

Un passage de quelqu’un au tableau, le reste de la classe fait en même temps

### Se contrôler en ligne

**STALKEZ VOUS**

* Via google
* Sur facebook
* Sur snapchat
* Sur twitter
* Partout

Traquez-vous, demandez à vos amis de vous traquer. Les RH sont les pires stalkers du monde (mais bon, parfois c’est payant de laisser traîner des trucs que vous assumez, genre des photos au carnaval de Dunkerque. Ça augmente le capital sympathie)

Un peu d’exercice

### Avoir un beau CV

* En ligne avec une adresse du genre cv.mondomaineperso.fr
    * c’est votre preuve de compétence HTML/CSS :)
* Avoir une belle version PDF … envisagez de le faire comme une maquette papier ! ⚠ vérifiez que la version s’imprime correctement en noir et blanc
    * indispensable hélas, en plus il sera imprimé …

## Être présent en ligne pour être contacté

### LinkedIn

https://www.linkedin.com

Réseau social d’entreprise

Vous créez tous un compte tout de suite. Vous ajoutez tout le personnel de PopSchool, Arnaud Flament et les personnes travaillant à la maison syndicale que vous avez rencontrées et vos responsables de projets collectifs. Et votre famille si vous la trouvez dessus.

### Les Job Boards

Pour trouver du travail, vous pourrez ignorer Pole Emploi.

Les job boards sont des sites webs, majoritairement privés (sauf l’APEC) et gratuits (conformément à la loi) proposant des offres d’emploi et où les recruteurs peuvent découvrir votre CV.

Allez vous créer un compte sur les sites suivants:
* monster.fr
* keljob.com
* apec.fr (c’est un peu en lien avec Pole Emploi en fait …)
* regionsjob.com
* cadremploi.fr
* meteojob.com

Vous allez être contactés dans les semaines qui viennent, n’oubliez pas de répondre. Ne fermez jamais définitivement une porte !

### Être présent en ligne

* être actif sur twitter
* participer à un projet libre
* créer des tutos
* avoir une chaîne youtube

Si vous êtes actifs sur Internet et que vous indiquez être en recherche d’emploi, vous serez contactés.

## Postuler

### Intitulés des postes

Vous êtes des développeurs webs :
* Intégrateur HTML
* Web designer
* Développeur front office
* Développeur back office
* Développeur full stack

Selon l’intérêt que vous portez au sujet vous pouvez également faire:
* Administrateur système
    * dev op
* Administrateur base de données (DBA)
* Technicien support

Cette liste est évidement non-exhaustive

#### Quand postuler

Si vous avez 50% des compétences techniques demandées. Vous pouvez totalement ignorer le reste (exigence d’expérience, de diplôme etc.)

Vous pouvez signer un CDI là-maintenant-tout-de-suite-cassez-vous-de-là-que-je-prenne-des-vacances. Bref, vous pouvez quitter PopSchool si on vous propose un poste de dev tout de suite :)

#### Sites d’annonces

* [Euratechnologies à Lille](http://www.euratechnologies.com/euratechjob/offres)
* [Plaine Image à Roubaix](http://www.plaine-images.fr/offres-demploi/)
* tous les jobs boards qu’on a vus avant

#### Maraude

Aller sur les annuaires d’entreprises, visiter leurs sites web. Souvent on y trouve une section emploi.

#### Candidature libre

Vous pouvez envoyer un CV à toute entreprise qui vous semble intéressante même si elle ne recrute pas. Elle pourrait transmettre votre CV à des confrères !

### Que dire ?

**Faites très attention à votre orthographe**

* Que vous cherchez un stage
* Des détails sur le stage : durée (2 mois), mode (temps plein), éventuellement rémunération (à vous de voir entre rien, facultatif ou 550€/mois)
* Évoquez PopSchool (6 mois, titre pro développeur web niveau III)
* Que vous êtes grand débutant
* Personnalisez le mail en montrant que vous vous êtes un minimum intéressé à l’entreprise (rien n’est plus gonflant que d’avoir l’impression que le mail est un copier/coller)
* Dites du bien de vous en quelques mots
* Dites au revoir (Bonne journée, Cordialement, Bien à vous …)

Points d’attention :  
* Inutile de dire votre nom : il apparaît dans le client mail
* Je vous ai dit de faire attention à l’orthographe ?
* Inutile de faire un mail trop long, il ne sera pas lu. Allez droit au but

Créez-vous un template de mail : tous les textes sont prêts et relus, il ne vous reste plus qu’à remplir la section personnalisée sur l’entreprise (sans faire trop de faute d’orthographe, on se comprend ? :))

#### Exemples concrets

(here be exemples concrets from my inbox)

## Conseils

* Postulez beaucoup
* Tenez une liste des entreprises contactées pour éviter les doublons
* N’ayez pas peur : vous ne serez jamais pris si vous ne postulez pas
* Postulez énormément, genre plusieurs fois par jour
* Soyez vous-même, ne vous forcez pas à *faire des phrases*
* Sur votre CV, n’hésitez pas à mettre vos expériences pro même en *job de merde*. Ils prouvent votre capacité à travailler même sur des jobs peu intéressants; votre capacité à vous lever tôt, avoir une hiérarchie pressante, des contraintes de productivité etc.
* Postulez sans arrêt, hein
