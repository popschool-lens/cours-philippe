# Introduction à JavaScript

Philippe Pary, 2018

Pour chaque exercice, pensez à utiliser git, évidemment

## Exercice 1

À l’aide d’un bouton, changez la couleur de fond de la page à l’aide d’un attribut [onclick=](https://developer.mozilla.org/docs/Web/API/GlobalEventHandlers/onclick) qui vient modifier la valeur *document.body.style.backgroundColor*

## Exercice 2

1. Affichez un formulaire de saisie JavaScript ([window.prompt()](https://developer.mozilla.org/docs/Web/API/Window/prompt)) demandant un nom
2. Affichez le résultat via console.log sous forme « *Hi, my name is … * »

## Exercice 3

Amélioration de l’exercice précédent :  
1. Créez un champ `<input>` avec un `id=myName` et un `<button>`
2. Affichez le contenu du champ via le bouton avec un attribut, *onclick* ([console.log](https://developer.mozilla.org/en-US/docs/Web/API/Console/log))

tip: le paramètre de console.log est `document.getElementById("myName").value` ou `document.querySelector("#myName").value`

## Exercice 4

1. Créez un fichier HTML
2. Creez un fichier CSS qui dit que les div sont une fois rouges, une fois bleues
3. Mettez un bouton que quand on clique, ça ajoute une div à la page !

tip: n’oubliez pas de mettre une `width` et une `height` sur vos `<div>` sinon elles sont de taille 0x0 et donc invisibles !

tip: div:nth-child(odd) { background: red; } div:nth-child(even) { background: blue; }

tip: document.body.appendChild(document.createElement('div'));

## Un peu de théorie …

### Contexte

Première spécification JS en 1995 (rappel: HTML et WWW en 1991, CSS en 1994-95)  
Comme CSS, lié au navigateur majoritaire de l’époque, NetScape, devenu Firefox.

### Principe

JavaScript est un langage de script (comme PHP) : il n’est pas compilé. Les fichiers Javascript sont en texte plat et sont interprêtés par le moteur JavaScript du navigateur web (SpiderMonkey pour Firefox, V8 pour Chrome etc.)  
Comme pour le code CSS, le code JavaScript peut être embarqué dans la page HTML via les balises &lt;script&gt;&lt;/script&gt; ou via des fichiers spécifiques via la balise [&lt;script&gt;](https://developer.mozilla.org/fr/docs/Web/HTML/Element/script). Par convention, les fichiers ont pour extension *.js*

**Tout est exécuté sur la machine de l’utilisateur**

Conséquence, vous ne pouvez pas avoir confiance dans un résultat calculé en JavaScript car l’utilisateur peut voir les variables, modifier le code etc.  
Par exemple, si vous développez un jeu de pendu en pur JavaScript, l’utilisateur peut connaître le mot tiré au sort ou encore truquer son compteur de nombre d’essais via la console de débogage.

Si des éléments nécessitent d’être certains, vous devez les faire côté serveur. En PHP par exemple.

En JavaScript vous pouvez faire les actions sans gravité : effets visuels, amélioration des formulaires, mise à jour automatique de la page, appels à des API etc.  
JavaScript s’appuie massivement sur [DOM](https://developer.mozilla.org/docs/Web/API/Document_Object_Model) pour s’intégrer dans une page web.

### DOM ?

**Document object model**, la structure d’une page web. C’est une interface de programmation qui propose une structure des documents et une méthode pour accéder aux différentes valeurs.  

On peut accéder aux élements de la page web par 3 billets : par leur type (*&lt;div&gt;*, *&lt;a&gt;* …), par classe (valeur de l’attribut *class=*) ou par identifiant (valeur dde l’atttribut *id=*)

En clair vous avez des valeurs (document.style pour accéder au style de la page) et des fonctions (document.getElementByClass pour trouver tous les éléments ayant une classe donnée)

#### Sélecteur universel

<https://developer.mozilla.org/fr/docs/Web/API/Document/querySelector>

Il est récent et ne marche pas sur les anciens navigateurs. Si vous vous en servez, vous devez utiliser un *polyfill*

La syntaxe de sélection est la même que pour CSS

#### Sélecteur par id

<https://developer.mozilla.org/fr/docs/Web/API/Document/getElementById>

Renvoie un seul élément

#### Sélecteur par type d’élément

<https://developer.mozilla.org/fr/docs/Web/API/document/getElementsByTagName>

Renvoie un tableau d’éléments (pour le moment, on va éviter)

#### Sélecteur par classe

<https://developer.mozilla.org/fr/docs/Web/API/Document/getElementsByClassName>

Renvoie un tableau d’éléments (pour le moment, on va éviter)

#### Sélecteur par mot-clef `this`

<https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/L_op%C3%A9rateur_this>

Renvoie l’élément dans lequel on se trouve.

Mini-exo:

    <input id="myNumber" type="number" onChange="console.log('Nouvelle valeur:' + this.value)" placeholder="5">


## Exercice 5

Refaites l’exercice 3, mais en mieux !

1. Virez le bouton !
2. Utilisez l’attribut *[onchange](https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onchange)* sur la div
3. Utilisez `this.value` comme paramètre pour le `console.log()`

## Exercice 6

Allez, on rigole pour de bon …

1. Créez un grand nombre de `<div>`
2. Sur chacune mettez un `onClick="this.style.background=" et mettez une couleur au hasard après le *=*
3. Amusez-vous à cliquer
4. Améliorez tout ça en utilisant soit `flex` soit `grid`, bref, jouez avec CSS

### Théorie

JS pour un front-dev, c’est drôle pour modifier en live le code HTML et CSS !

Alors on va voir un peu comment s’y prendre.

#### Accéder aux valeurs d’un éléments

* `element.style` : la valeur de l’attribut `style`. Attention, `style` et `css` sont souvent source de confusion !
* `element.value` : la valeur des éléments de formulaires (`input` etc.)
* `element.innerHTML` : tout ce qui est contenu entre deux balises, y compris les autres balises HTML
* `element.firstChild`, `element.lastChild`, `element.childNodes`: premier, dernier et liste de tous les éléments enfants d’un élément.
* `element.parentNode` : div, je suis ton père
* `element.textContent` : le texte contenu dans l’élément ainsi que dans tous ses enfants, débarassé de toute balise HTML

Avec ça, on a de quoi faire, non ?

#### Créer des éléments à la volée

On peut créer les éléments à la volée

    newDiv = document.createElement("div");
    newDiv.id = "green";

On peut les ajouter à un autre éléments

    document.getElementById("myDiv").appendChild(newDiv);

### Exercice 7

1. HTML/CSS/JS
2. Ajoutez un bouton et une div avec pour id `myDiv`
3. Créez un CSS avec des propriétés pour les div paires (`div:nth-child(odd)`) et impaires (`div:nth-child(even)`). N’oubliez pas de leur donner une hauteur et largeur
3. À la div `myDiv`, associez un événement `onClick` qui ajoute une nouvelle `<div>` à l’intérieur `myDiv` (`myDiv` va donc contenir plein de `<div>` quand on va se mettre à cliquer)

### TP

Faites n’importe quoi avec ce qu’on a vu en JS et avec tout ce qui vous traverse l’esprit ou n’importe quoi que vous trouvez sur Internet. C’est un temps prévu pour le nawak

