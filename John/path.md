# Introduction aux dossiers pour le développement web

Par Philippe Pary, 15 mars 2018

## Arborescence

Les ordinateurs utilisent pour ranger les fichiers un système de dossiers en arborescence.

    /  
    |-var  
    |-etc  
    |-home  
        |-philippe  
            |-Documents  
                |-Banque  
                |-Personnel  
                |-Sites web  
            |-Téléchargements  
            |-Images  
                |-chatons  
                |-vacances  
                |-comptabilité  

## Se déplacer dans les dossiers

On utilise la commande `cd` (change directory) pour changer de dossier

    cd Documents

Il existe des raccourcis dont voici la liste:

* / : la racine du système (le dossier le plus élevé)
* . : le dossier dans lequel on se trouve
* .. : le dossier parent
* ~ : son dossier utilisateur (/home/<nomdelutilisateur>, par exemple /home/popschool)

On peut combiner les éléments en ajoutant `/` entre eux dans le but de changer de plusieurs dossiers à la fois

    cd Documents/Banque
    cd /var
    cd
    cd ~/Images/chatons
    cd Téléchargements/..

Notez: 
* taper `cd` sans aucun argument revient à taper `cd ~`
* taper Téléchargements/.. revient … à ne rien faire (on rentre dans un dossier enfant puis on remonte dans le dossier parent)

## Connaître le contenu d’un dossier

On utilise la commande `ls` (list segments) pour lister le contenu d’un dossier

On peut utiliser `ls` pour lister le contenu d’un dossier dans lequel on ne se trouve pas:

    ls Documents
    ls /etc
    ls ~

## Perdu ?

Si vous vous sentez perdu, bash dans sa configuration habituelle vous indique le dossier dans lequel vous vous trouvez avant l’invite de commande

    philippe@laetitia:~/Documents/POP/Cours/popschool-lens/Jimmy$

Vous pouvez également utiliser la commande `pwd` qui affichera votre position

## Racine web

Votre site web est également une arborescence …

    /  
    |-/img  
        |-favicon.png  
        |-logo.png  
        |-background.png  
    |-/css  
        |-main.css  
        |-responsive.css  
    |-/js  
        |-main.js  
        |-customer.js  
    |-index.html  

La racine de votre site web correspond à un dossier sur votre ordinateur/serveur

* Celui où a été lancée la commande `php -S 0.0.0.0:8008`
* Celui configuré comme étant la racine web par le serveur Web (Apache, Nginx …)

Les technologies front-end sont limitées à ce qui est présent dans la racine web, pour des raisons de sécurité. Ce qui est avant la racine web est inaccessible aux visiteurs !

Les technologies back-end peuvent accéder à tout votre ordinateur/serveur en revanche … elles sont bien plus dangereuses à manipuler donc :)

## Pimp my ride

Pour finir sur une note légère … vous pouvez customiser votre terminal bash.

Ouvrez le fichier `~/.bashrc`. C’est un fichier caché, vous pouvez afficher ces fichiers grâce au raccourci ctrl+H.

Ajoutez `color_prompt=yes` au début du fichier, ouvrez un nouveau terminal. Il y a de la couleur, c’est joli :')

Internet regorge de tutoriels et d’astuce pour personnaliser votre terminal, je vous laisse fouiller
