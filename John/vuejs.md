# VueJS

Philippe Pary

## Kézako ?

Vue est un framework JavaScript **inspiré d’Angular**. Il **se concentre sur la partie *view* ** du modèle MVC (model, view, controller)

Ça veut donc dire qu’on doit gérer le modèle nous-même (API, services) et qu’on doit largement se démerder sur le controller.

Pour rappel:
* Model: les données à représenter. Grosso merdo, pour nous c’est la base de données et tout le code qui sert à s’y connecter.
* View: twig, vuejs … bref la présentation des choses. Dans le dev web c’est HTML + CSS
* Controller: toute l’intelligence de l’application (contrôles de sécurité, calculs, vérifications etc.)

Notre objectif de la semaine est de préparer une application qui se branchera à l’API que vous développerez ces deux prochaines semaines.

L’API n’existe pas **nous travaillerons depuis des mocks**. Les mocks sont des fichiers en texte plat contenant les données. Ils servent d’ersatz d’API.

Un peu de doc:

* <https://vuejs.org/v2/guide/>
* <https://www.grafikart.fr/formations/vuejs/decouverte>
* <https://www.pierrefay.fr/formation-vuejs.html>

## Utiliser VueJS totalement à l’arrache …

1. Créez une page web classique
2. Ajouter le lien CDN suivant: `<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>`
3. Créez un ficher `script.js` avec le code suivant:
    var app = new Vue({
     el: '#app',
     data: {
        message: 'Hello Vue!'
        }
    })
4. Ajoutez le code suivant à votre page HTML
    <div id="app">
      {{ message }}
    </div>
5. Lancez votre page HTML. Miracle


## Utiliser VueCLI

1. `sudo npm install -g vue-cli`
2. Créez le dossier où travailler
3. `vue init browserify` (full featured vue, avec plein de fonctions magiques)
4. Suivez les instructions
5. Vous les avez bien suivies les instructions ?
6. Faut lire ce qui est écrit à l’écran hein …
7. Vous vous souvenez du test de lecture ?
8. Bon … faites un `ls`. Y’a du monde …

* Dans le dossier `node` il y a des packages nécessaires au projet. Y’en a plein à cause des killer features de `browserify`
* Dans le dossier `dist` on trouvera les fichiers à envoyer sur la production
* Dans le dossier `src` on mettra notre code
* Le `index.html` peut être un peu customisé, mais on ne va pas y passer trop de temps …

* Baladez vous un peu dans `src` …
* Lancez `npm run dev`

## Suivre le tutoriel de Pierre Fay

Parce que payer des gens pour réinventer une roue carrée, c’est totalement con. Son tuto est bien fait, allez y.

Vous pouvez aussi suivre celui de graphikart

## Préparer l’application front de vision des élèves

Organisons-nous un peu …

### Réfléchir avant d’agir

1. Faites des maquettes de l’application et faites la valider par le formateur (et on validera un point du titre pro !)
2. Réalisez la maquette fonctionnelle
3. Rédigez le mockfile de vos données en JSON (l’API sera en JSON !)

#### JSON

Voici du JSON

    {
      "status": "ok",
      "data":
        {
            "types": [
                {
                    "id": 1,
                    "name": "dog"
                },
                {
                    "id": 2,
                    "name": "cat"
                },
                {
                    "id": 3,
                    "name": "fish"
                }
            ],
            "pets": [
                {
                    "id": 1,
                    "type": 1,
                    "name": "Medor",
                    "sex": 0,
                    "birthdate": "2007-03-12"
                },
                {
                    "id": 3,
                    "type": 1,
                    "name": "Simone",
                    "sex": 1,
                    "birthdate": "2014-05-01"
                },
                {
                    "id": 25,
                    "type": 3,
                    "name": "Bubule",
                    "sex": 1,
                    "birthdate": "2017-10-02"
                },
                {
                    "id": 28,
                    "type": 2,
                    "name": "Le chien",
                    "sex": 0,
                    "birthdate": "2018-03-09"
                }
            ]
        }
    }

Voici quoi mettre dans un mockfile (par exemple data.js)

    var petsJSON = JSON.parse(`{ "status": "ok", "data": […] }`);

Et voici comment comprendre ce qu’il y a dans la variable `petsJSON` (ça ne devrait rien vous apprendre !)

    console.log(petsJSON);

Évidemment, vous devez adapter l’exemple à votre schéma de base de données …

Si vous avez du mal, prenez le mock proposé ci-dessus et:
* Faites une maquette et faites la valider par un formateur
* Réalisez la maquette fonctionnelle
* Intégrez l’application en VanillaJS
* Venez en cour de soutien pour qu’on bascule de VanillaJS à VueJS !

Votre application devrait:
* afficher tous les types d’animaux
* afficher tous les animaux
* afficher tous les animaux avec leur type en toutes lettres

## Intégrer l’application

Une fois le mockfile prêt, vous pouvez selon votre niveau soit directement la mettre en œuvre en VueJS, soit passer par une étape VanillaJS et attendre le cours de jeudi qui sera un pas à pas pour réaliser l’application.
