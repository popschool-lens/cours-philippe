# Cours de SQL, support de cours

Philippe Pary, 2018

## Introduire la notion de base de données

### TP

* Se connecter à PHPMyAdmin
* Créer une base `TP-SQL`
* Créer une table `students`
* Ajouter trois champs
    * *id* de type int, auto-incremental (A.I) et primaire
    * *firstname* de type varchar(50)
    * *lastname* de type varchar(50)
    * *birthdate* de type date
    * *sex* de type tinyint

### Serveur

Stockage de données, accès multiple, DBA, SGDB (DBMS), BDD (DB)

Le serveur est l’ordinateur qui fait tourner le logiciel serveur de base de données. On s’y connecte avec un client, que ce soit la commande `mysql` ou via `mysqli`, `PDO`, `Doctrine` …

### Base

Une base est une unité dans le serveur de base de données. Chez OVH, plusieurs sites webs sont sur un même serveur : ils ont des bases séparées et ne peuvent donc pas voir ce qu’il y a dans la base du voisin !

### Table

Une table est l’équivalent d’une feuille de calcul dans un tableur. C’est là que sont définies les types de données et enregistrées les données.

### Colonne

La colonne est la définition d’un type de donnée: une adresse (du texte), une date de naissance (une date), un sexe (booléen ou tinyint pour les plus militant·e·s), un prix (nombre) etc.

Parler du typage, clef primaire, indexation, unique, auto-increment etc.

### Ligne

Les lignes sont les données à proprement parler :)

## Requêtes de base

On a 4 requêtes de base:
* INSERT
* SELECT
* UPDATE
* DELETE

L’acronyme est donc *CRUD* (si si …) pour *Create, read, update, delete*

### TP

Via l’interface de PHPMyAdmin

* Créer 5 élèves. Lire attentivement la requête affichée par PHPMyAdmin
* Modifier 2 élèves. Lire attentivement la requête affichée par PHPMyAdmin
* Supprimer un élève. Lire attentivement la requête affichée par PHPMyAdmin
* Rechercher un élève. Lire attentivement la requête affichée par PHPMyAdmin

### Notion de requête

Langage SQL, différences entre les technologies. Jeter les notions de CRUD, d’indexation, de jointure

### INSERT

Créer des données

`INSERT INTO students (firstname, lastname, birthdate, sex) VALUES ('Jean-Kévin', 'Delaglande', '2001-04-04', 0);`

### SELECT

Sélectionner des données (et pas forcément sur une seule ligne ni sur un seul tableau !)

`SELECT * FROM students WHERE sex=0;`

`SELECT name, birthdate FROM students;`

### UPDATE

Mettre à jour des données

`UPDATE students SET birthdate='1999-04-04' WHERE id=1;`

### DELETE

Supprimer des données

`DELETE FROM students WHERE lastname='Delaglande';`

### TP

Via l’outil *Requête* de PHPMyAdmin

* Créer 5 élèves.
* Modifier 2 élèves.
* Supprimer un élève.
* Rechercher un élève.

## Jointures

### TP

Créer une table `promotions`

* `id` de type int, auto-incremental (A.I) et primaire
* `name` de type varchar(255)
* `startdate` de type date
* `enddate` de type date

Insérer les nom des deux promotions

Modifier la table `eleves`

* Ajouter une colone `promotion_id`
* Affecter une promotion à chaque élève

### JOIN

`SELECT * FROM students JOIN promotions on students.promotion_id = promotions.id;`

## Exercice

### Niveau 1: Pratiquer CRUD

* Ajouter des élèves et des promotions, les modifier et les supprimer, notamment via des clauses *WHERE*
* Rajouter des colonnes aux tables: sexe, adresse (trois colonnes: *address*, *postcode* et *city*) …
* Remplissez à fond la base de données et sauvegardez la (`Exporter`), vous pourrez ainsi rétablir la sauvegarde en cas de fausse manipulation
* Continuer à faire des requêtes pour vous spécialiser avec WHERE
    * Par début de code postal, par exemple sélectionner les 59
    * Dont la date de naissance est dans les années 80
    * Dont la promotion se déroule notamment en 2017

### Niveau 2: Étendre, travailler les jointures

Créer une table `subjects` pour stocker les matières et une table `validation` pour stocker quelles matières a validé chaque élèves

Remplir ces tables à la main

Savoir faire des requêtes (ex: quel élève de la promotion Aaron a validé HTML ?)

### Niveau 3: Foreign keys constraits

Ajoutez des contraintes de clefs aux tables et des `NOT NULL` aux colonnes où c’’est nécessaire. RTFM

Quelques requêtes avancées:

    * Nombre d’élèves ayant validé HTML par promotion
    * Moyenne d’âge par sexe, moyenne d’âge par promotion
    * Nombre de personnes ayant le même prénom, par prénom évidemment
