# TP de VueJS : application de gestion des élèves

Philippe Pary, 2018

## Objectif

Créer en pas à pas une application de gestion des élèves

Nous sommes censés avoir les éléments suivants:
* Une maquette fonctionnelle en HTML/CSS
* Des fichiers contenant les données en JSON pour imiter une API (mockfiles)

## bootstrap du projet

On va créer un nouveau projet Vue

* mkdir vuejs-tp
* cd vuejs-tp
* vue init browserify
* npm install
* npm run dev

## Design de la page

On ajoute à `index.html` le code HTML et CSS nécessaire pour appliquer le style global, ainsi que l’en-tête et le pied de page

## Premier composant

On va créer un premier composant qui va lister les promotions pour le moment.

On vire tout ce qui concerne `Hello.vue` et on remplace par un `Promotion.vue`

* récupérer les données du JSON promotions
* les avoir en données du composant
* afficher la liste des promotions

## Le composant de promotion

* on remplace l’HTML affichant les promotions par une balise `<promotion>`
* on crée le composant `Promotion`
* on transfère les informations via des `props` dans `Promotion` et via un `v-bind`, exemple `<promotion v-for="promotion in promotions" v-bind:key="promotion.id" v-bind:id="promotion.id" v-bind:name="promotion.name"></promotion>`

## Afficher les élèves

Dans le composant promotion

* récupérer les données du JSON students
* les avoir en données du composant
* afficher la liste des élèves en filtrant avec un `v-if` (exemple `<div v-for="student in students" v-if="student.promotion_id==this.id">…</div>`

## Le composant student

* on remplace l’HTML affichant les étudiants par une balise `<student>`
* on crée le composant `Student`
* on transfère les informations via des `props` dans `Student` et via un `v-bind`, exemple `<student v-for="student in students" v-if="student.id==this.id" v-bind:key="student.id" v-bind="student"></student>`

## Afficher un élève

Dans le composant student

* juste mettre le code pour afficher un étudiant :)
