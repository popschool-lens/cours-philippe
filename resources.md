# À revoir

* Graven développement sur Youtube
* https://www.youtube.com/channel/UC5cs06DgLFeyLIF_II7lWCQ

# Générique

* <https://github.com/yrgo/awesome-eg>
* <https://davidl.fr/manifesto.html>
* <http://books.goalkicker.com/>
* <https://exercism.io/>
* <https://codex.wordpress.org/Know_Your_Sources> liste des ressources du projet wordpress (EN)
* <https://www.luc-damas.fr/hop/pedagogie-houblonnee-client-serveur>
* <https://learngitbranching.js.org/> Jeu pour apprendre les branches git

## HTML/CSS

* <http://caniuse.com>
* <https://www.codewars.com/>

## Design

* <https://simpleicons.org/>


# CSS

## Sélecteurs

* <https://flukeout.github.io/>

## Flexbox

* <http://flexboxfroggy.com>
* <http://flexboxzombies.com>

## Grid

* <http://cssgridgarden.com/>
* <http://grid.malven.co/>

## Bootstrap

* <https://bootstrap.build/app/v4.0/>

## Misc

* <http://gallery.theopalgroup.com/selectoracle/> Explicateur de CSS


# PHP

* <https://www.functions-online.com> complément à la doc pour les débutants

# JS

## VanillaJS

## VueJS

* <https://vuejs.org/v2/guide/>
* <https://www.grafikart.fr/formations/vuejs/decouverte>

## AngularJS // Angular

* <https://angular.io/tutorial> Angular2
* <https://www.tinci.fr/blog/apprendre-angular-en-un-jour-le-guide-ultime/> Angular1
* <https://www.grafikart.fr/formations/angularjs> Angular1, ATTENTION, ANGULAR 1.2 (si vous prenez 1.5 ou 1.6, il va falloir adapter le code)

## Misc

* <https://medium.freecodecamp.org/modern-frontend-hacking-cheatsheets-df9c2566c72a> style sheet frameworks et vanillaJS
* <https://addyosmani.com/blog/script-priorities/> manières de charger un fichier Javascript

# Autres

## Design

* <https://www.layoutit.com>
* <https://wireframe.cc/>
* <https://iconsvg.xyz/>

## Bash

* <http://luffah.xyz/bidules/Terminus/> jeu d’aventure pour apprendre la ligne de commande (<https://github.com/luffah/terminus>)

## Python

* <https://www.python.org/about/gettingstarted/> Tuto officiel
* <http://cscircles.cemc.uwaterloo.ca/ > Méthode classique
* <https://checkio.org/> Jeu pour apprendre Python (EN)
* <https://fr.tuto.com/python/gratuit-python-les-bases-pour-debutant-python,46272.html> Tutoriel vidéo

# Mobile

* <https://www.mydevice.io/> Savoir ce qui est supporté par votre téléphone ou tablette

## Cordova

* <https://ccoenraets.github.io/cordova-tutorial> Complet mais compliqué. Mode texte
* <https://www.grafikart.fr/tutoriels/cordova/cordova-angular-454> Simple mais foutraque. Morceaux d’angular à la fin. Mode vidéo

## Ionic

* <http://ionicframework.com/docs/intro/tutorial/> Ionic 2 tutorial
* <https://www.grafikart.fr/tutoriels/cordova/ionic-framework-641> Ionic 1. Mode vidéo
* <https://ccoenraets.github.io/ionic-tutorial/> Ionic ?. Complet mais compliqué. Mode texte

# SQL

* <http://sql.sh/>

# Wordpress

* <https://www.alsacreations.com/actu/lire/1742-tutoriels-videos-gratuits-creer-un-theme-wordpress.html>

# Algorithmes

* <https://tomorrowcorporation.com/humanresourcemachine>
* <http://www.epistorygame.com/>
* <http://store.steampowered.com/app/246070/Hack_n_Slash/>

## Bonnes pratiques

* <https://github.com/yrgo/awesome-eg>
* <https://search.google.com/test/mobile-friendly>
* <https://developers.google.com/web/progressive-web-apps/>


# Vrac …

## Entrepreunariat

* <https://www.awelty.fr/pages/combien-coute-un-site-web.html>
* <https://itunes.apple.com/us/app/estimapp-how-much-to-make/id1099000514?mt=8&ref=producthunt>
* <https://m.xkcd.com/1827/>

## Vim

* <http://www.openvim.com/>
* <https://vim-adventures.com/>

## RegExp

* <https://regex101.com/>

## RGPD

* <https://gdprchecklist.io/>

## LULZ

* <https://www.theuselesswebindex.com/>
* <https://stat.ripe.net/widget/routing-history#w.resource=as15562&w.starttime=2017-01-15T00%3A00%3A00&w.endtime=2017-06-23T00%3A00%3A00&show=Maxmized>

## Adminsys

* <https://nginxconfig.io/> générateur de configuration NGinx
* <https://linuxfr.org/users/spacefox/journaux/le-microprocesseur-ce-monstre-de-puissance-qui-passe-son-temps-a-attendre> la vie des processeurs

## Français

* <https://www.francaisfacile.com>
* <http://tazzon.free.fr/dactylotest/>
* <https://www.lecturel.com/clavier/mots-par-minute.php>
* <https://yourlogicalfallacyis.com/fr>

## Securité

* <https://securityheaders.com> test des headers
* <https://scanner.secapps.com/> test général front-end
* <https://github.com/FloeDesignTechnologies/phpcs-security-audit> toolkit d’audit PHP
* <https://github.com/mozfreddyb/eslint-config-scanjs> eslint pour sécurité issu de la MoFo

