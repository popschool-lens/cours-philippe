# Être présent en ligne dans un contexte professionnel

Philippe Pary, juillet 2019

# La présence en ligne

## La présence à titre privé

Vous allez chercher un boulot, c’est votre vie professionnelle.

Vous avez une vie privée, encore heureux.

Attention à ce que votre vie privée ne vienne pas masquer qui vous êtes professionnellement

* Auto-stalkez vous : cherchez vous sur Internet, demandez vous si le contenu vous nuit professionnellement
    * Corrigez en passant sous pseudonyme à titre privé
    * Faites déréférencer le contenu si vous avez honte :)
* Demandez à votre voisin de vous stalkez, stalkez votre voisin
    * se stalker soi-même c’est comme se lécher le coude, c’est dur à faire …
    * alors que se stalker à deux **dans un cadre de consentement mutuel** c’est tellement mieux

N’oubliez pas que le stalk outre passe les moteurs de recherche : ajoutez également l’autre à votre répertoire téléphonique et lancez une recherche sur les réseaux sociaux à partir du numéro… c’est parfois étonnant …

## Les réseaux sociaux classiques

Les employeurs vont vous chercher sur Internet, ils vont peut-être vous y trouver : twitter, deviantArt, instagram, snapchat… n’oubliez pas qu’ils auront votre numéro de téléphone ce qui vous rend très facile à retrouver !

Quelques options:
* laisser traîner des comptes privés que vous assumez totalement
* créer des comptes artificiels, un peu morts mais qui transmettent votre image
* vous y mettre, par exemple commencer à utiliser Twitter pour un travail de veille

## Les réseaux sociaux professionnels

Le réseau social de référence pour l’informatique est LinkedIn. Ça vous sert pour être présent en ligne dans le monde pro, faire de la veille, trouver du boulot, glander quand on se fait un peu chier sans que le patron n’ait rien à y redire.

* Vous vous inscrivez à LinkedIn
* Vous remplissez votre profil
* Vous ajoutez tout le personnel de PopSchool Lens, tous vos camarades de classe et tout le monde que vous connaissez
* Vous ajoutez tout le personnel de PopSchool en général
* Vous demandez à vos formateurs et camarades de recommander vos compétences

Quand vous chercherez en stage (intervention calée en août) on exploitera un peu plus LinkedIn

## Les réseaux sociaux d’entreprise

Il existe de nombreux réseaux sociaux d’entreprise (RSE). À PopSchool c’est Slack.

Vous devez y être exemplaire durant la période d’essai (parce qu’on peut vous virer sans motif) mais également après (ça serait con de louper une augmentation à cause d’une tendance un peu trop poussée à la gauloiserie)

Le RSE ce n’est pas un réseau social entre copains, c’est le monde professionnel. Un comportement professionnel y est de rigueur.

* regardez comment se comportent vos collègues sur le RSE
    * ont-ils mis leur vrai nom ? un pseudonyme sérieux ? un pseudonyme à la con ? Leur arrive-t-il d’en changer ?
    * ont-ils mis leur vraie photo en avatar ? Une illustration ? Des illustration humoristique ?
    * comment parlent-ils ? L’écriture abrégée est-elle de mise ? Font-ils des threads ? Réagissent-ils par emoji ? Répondent-ils « ok » ou « merci » ?
* soyez attentif à respecter l’organisation du RSE
    * respectez les thèmes des sections mises en place (canaux pour Slack)
    * s’il y a une charte, lisez la attentivement : elle parlera toujours du vécu de l’entrepris et des sujet sensibles
    * une seule remarque sexiste ou raciste peut donner lieu à un licenciement immédiat
    * évitez de parler politique, religion et faites gaffe si vous parlez football (surtout à Lens)
    * _rien n’est moins partagé que le bon sens, les choses qui vont sans dire vont mieux en les disant, personne ne comprend le second degré, personne n’a le sens de l’humour_. Vous êtes prévenus :)

# Le CV en ligne

Étant des développeurs webs, il fait sens d’avoir un CV en ligne … c’est même une preuve de compétence

* Inspirez-vous des CV des anciens, y’a vraiment du bon
* N’hésitez pas à trouver un thème HTML/CSS à décliner si vous n’avez pas de goût
    * Vous pouvez même utiliser du vanilla Bootstrap, ça fait le job hein :)
* Mettez des liens vers des preuves sur vos compétences (vers un site web, vers une application web, vers un repo git etc.)
* Le fond importe plus que la forme … sauf si vous avez des prétentions dans le design cela va de soit :)
* Vous avez de l’espace illimité, profitez-en !
    * simplexifiez : ça veut dire cacher du contenu derrière des boutons pour que ça ait l’air simple en apparence mais profond en réalité

N’oubliez pas d’avoir un CV en PDF qui soit imprimable

* PDF : c’est le seul format qui vous garantit que ça va être chez le recruteur comme chez vous. Les .doc & cie changent d’un PC à l’autre !
* Imprimable : il sera imprimé. Les RH sont comme ça …
* Il peut néanmoins être original si vous le souhaitez
    * imitation d’un zoning
    * imitation d’une wireframe
    * mais attention à ce que ça reste imprimable !
