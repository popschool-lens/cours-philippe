# Culture générale informatique

Cours distillé au long cours, Philippe Pary 2018. CC-BY-SA (most recent)

## Objectifs

Ce cours doit introduire aux enjeux du droit d’auteur et du respect de la vie privée. Il présente donc l’informatique, et particulièrement son histoire, en mettant en avant les tensions qui existent autour de la notion de droit d’auteur et du droit à l’intimité et à la vie privée.

Il suit une trame essentiellement historique. La lecture de *hackers* de Steven Levy est chaudement recommandée.

## Partie 3 : les années 90 et 2000

Cette section de l’histoire informatique nous intéresse particulièrement : le logiciel libre émerge en même temps qu’Internet. Après la question du droit d’auteur dans les années 70-80, la question du respect de la vie privée émerge. Le code libre à disposition devient gigantesque

### Les réseaux de communication sont installés

Après un travail de titan dans les années 60-70, le réseau téléphonique a terminé d’être déployé dans les années 80.

Internet se base sur ce réseau téléphonique. 

Les fameux modem 56K se basent sur les 64K nécessaires pour transporter de la voix et garantis par le réseau téléphonique. 64K-8K de protocole = 56K de débit ! (oui, on était honnêtes à l’époque … ou naïfs)

!["Boris Eltsine debout sur un tank, août 1991](images/eltsinetank1991.jpeg)

💥 Lors du pustch de 1991, qui précipitera la chute de l’URSS, les putshistes avaient pris le contrôle de la télévision, de la radio et coupé les lignes téléphoniques … mais pas celles dédiées à Internet qui avaient été oubliées. C’est au travers des universités, particulièrement celles de Léningrad (Saint Petersbourg) et Moscou, que nous sont parvenus en live les images notamment celle de Boris Elstine reprenant le contrôle de la situation

### La naissance des géants

Les années 90 et 2000 voient émerger de nouveaux géants de l’informatique.

IBM, le géant historique, subit un procès anti-trust aux États-Unis qui aboutit à son démentellement dans les années 80, entraînant notamment la fin de son monopole sur l’architecture PC. Microsoft profite de cet environnement, et des vissicitudes de son rival Apple, pour s’imposer en situation de monopole sur le marché des postes de travail (monopole acquis définitivement avec Windows 95). Ce monopole ne sera remis en cause qu’avec l’émergence des smartphones et tablettes (iOS et Android)

!["une station de travail typique du milieu des années 90"](images/1995-computer.jpeg)

Apple, après des errements entre 1985 et 1995, s’impose comme le symbole de l’innovation auprès du grand public : iMac, iPod, iPhone, iPad, iTunes etc.

!["Le premier ipod"](images/first-ipod.jpeg)

D’autres grandes sociétés informatiques apparaissent: CapGémini, Atos, Oracle, SAP …

À la fin des années 90 et au cours des années 2000 apparaissent les géants lié au web : Yahoo!, Google, Amazon, facebook etc.

### Structuration du logiciel libre

!["Tux, mascotte du projet Linux"](images/tux.png)

Le monde du partage se structure également. 1991 voit apparaître le noyau Linux. 

Les distributions (regroupement de logiciels libres formant un tout cohérent) apparaissent. Avec l’appui de Pixar, Debian est fondé en 1993. Red Hat est fondé en 1994. Aujourd’hui encore ce sont les deux projets-mère les plus centraux dans l’usage professionnel de Linux.

Les logiciels libres structurent l’apparition d’Internet (80% de parts de marché pour Apache au début des années 2000)

L’EFF est fondée en 1990, la FFII en 1999.

On peut citer le projet Mozilla (1998) et l’instauration de la Mozilla Foundation, née des ruines de NetScape ravagé par Microsoft, en 2003

!["Le premier logo de Wikipédia"](images/premierlogowikipedia.png)

On notera l’apparition d’IMDB en 1990, de Wikipedia en 2001 ou d’OpenStreetMap en 2004.

💡 Nota bene: Wikipédia avec un **é** désigne toujours la version francophone de l’encyclopédie. Inutile d’écrire *Wikipedia francophone* ! Par contre à l’oral, faut le préciser :)

### Internet

!["Les magnifiques inventeurs du Web : sir Tim Berners-Lee et Robert Cailliau"](images/bernersleecailliau.jpeg)

Le WWW est créé en 1991 et entraîne une rapide démocratisation d’Internet (FDN créé en 1993, premières offres grand public par France Télécom en 1996, Free en 1999)

On passe de 8,5 millons d’internautes en 2000 (14,4% de la population) à 25 millions (40%) en 2004 pour arriver à 44 millions (69%) en 2010

À l’émergence d’Internet, dans les années 90, nombre d’entreprises ont tenté d’imposer un système fermé, généralement inspiré du modèle économique du Minitel (Compuserve, AOL, MSN …)

Passé cette période, Internet croit comme un réseau décentralisé. Pour chaque service, il existe une vraie concurrence :
* Acheter ? Amazon, Ebay, Groupon, iBazar …
* Chercher ? Yahoo!, AltaVista, Google …
* Discuter ? forums, IRC, caramail, MSN …

!["l’iconique modem Wanadoo, symbole des premières heures de l’ADSL"](images/modem-wanadoo.jpeg)

Les années 2000 voient se généraliser la technologie ADSL permettant des débits acceptables pour diffuser des flux vidéos : jusqu’à 1Mb/s contre 64Kb/s avant !

### Lutte contre le terrorisme et méga-corporations contre vie privée

!["11 septembre 2001, attentat du world trade center"](images/911.jpeg)

Le changement de braquet dans la lutte contre le terrorisme international à partir de 2001 entraîne de nombreuses mesures attentatoires aux libertés individuelles (Patriot Act aux USA)  

Dans le même temps, les géants du web, voyant passer des pans entiers de la vie privée des utilisateurs, se mettent à la valoriser (publicité, statistiques etc.) attirant les convoitises intéressées (développement scientifique, renseignements publics et privés)  

!["Julian Assange, un des premiers wistleblower modernes"](images/julianassange.jpeg)

Ces dérives sont alors incomprises du grand public. Mais on vit une aire pré-snowden avec la fondation de  wikileaks en 2006, de la quadrature du net en 2008 etc. Internet permet l’apparition d’une nouvelle génération de lanceurs d’alerte (*wistleblowers*)


### On en retient

Les années 90 sont celles de la structuration économique de l’informatique et de l’émergence d’Internet & logiciel libre (les deux sont intrinsèquement liés)

Internet s’impose au courrant des années 2000 mais de nombreuses dérives ont émergé, la prise de conscience est plus tardive.

### Ce qui se passe après

* Généralisation des smartphones (avant 2005, tout le monde n’a pas un téléphone et encore moins un smartphone)
* Généralisation d’Internet (avant 2005 on est à moins de 30% de gens équipés)
* Recentralisation d’Internet autour de rares fournisseurs (les GAFAM)
… et c’est déjà énorme !
