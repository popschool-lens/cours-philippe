# Droits d’auteur, respect de la vie privée : les resources

Philippe Pary, 2017© CC-BY-SA

Ce cours est pragmatique, il ignore des parties théoriques pourtant très importantes. À vous de creuser

## Glaner des resources pour ses projets

### Resources ?

* Images
* Typos
* Sons
* Colorset 
etc.

### Sans détails, on ignore

Les resources sans précision de licence c’est haram.

On s’en sert, pas, c’est comme si ça n’existait pas

### Avec détails …

#### Licences libres

GPL, BSD, Creative Commons

On peut y aller avec quelques réserves :

* Un logiciel GPL reste libre, pas de version proprio possible
* On ne retire pas le lien téléchargement d’un logiciel AGPL
* On ne peut pas utiliser un creative commons non-commerciale dans un cadre professionnel.

#### Licence non-libres

Il faut contrôler la possibilité d’usage commercial/professionnel

## Créer ses propres resources

C’est sur mesure, c’est pas cher. Et fondamentalement, c’est le client qui paie :)

**Faire créer sur mesure, c’est la bonne idée**

* Vérifier les conditions générales de ventes, transfert de droit d’auteur (vous lirez parfois *propriété intellectuelle*)
* Tant que vous n’avez pas réglé la facture, souvent le transfert des droits n’est pas effectif : attention

## Quels risques ?

Jusque 300 000€ en théorie. En pratique, plusieurs milliers d’euros d’arrangement hors tribunal (non-refacturable au client …), plus une perte énorme de crédibilité auprès des clients et des pairs

## Quelques sources 

Pour se cultiver: 
* <https://vimeo.com/187047931>

Brainstorm: 

* https://www.fontsquirrel.com/
* https://commons.wikimedia.org
* Google Images avec filtre licences libres (section « droit d’usage » des options de recherche)
* https://framasoft.org/ porte d’entrée vers le libre
    * https://framalibre.org/taxonomy/term/44
