# Trouver du travail ou un stage

Philippe Pary, juillet 2022

## Se préparer

### Avoir une adresse mail correcte

* Avoir un mail du genre: nom.prenom@mondomaine.fr (mais simplement nom.prenom@fournisseurmail.com est déjà très bien)
* Avoir son nom qui s’affiche correctement (identité d’expéditeur dans le client mail)
* Créer l’alias sur son client mail (gmail, outlook, thunderbird etc.) 💡 si vous configurez bien le client mail, vous pouvez envoyer ET recevoir avec votre nom de domaine. Ça s’appelle un *alias*

Un passage de quelqu’un au tableau, le reste de la classe fait en même temps

### Se contrôler en ligne

⚠⚠⚠ **STALKEZ VOUS** ⚠⚠⚠

* Via google
* Sur Insta
* Sur TikTok
* Sur facebook
* Sur snapchat
* Sur twitter
* Partout

Traquez-vous, demandez à vos amis de vous traquer. Les RH sont les pires stalkers du monde (mais bon, parfois c’est payant de laisser traîner des trucs que vous assumez, genre des photos au carnaval de Dunkerque. Ça augmente le capital sympathie)

Un peu d’exercice

### Avoir un beau CV

* En ligne avec une adresse du genre cv.mondomaineperso.fr (ou même simplement sur githubpages ou netilify, ça va très bien)
    * c’est votre preuve de compétence HTML/CSS !
* Avoir une belle version **PDF obligatoirement** … envisagez de le faire comme une maquette papier ! ⚠ vérifiez que la version **s’imprime correctement en noir et blanc**
    * indispensable hélas, en plus il sera imprimé en noir et blanc …

## Être présent en ligne pour être contacté

### LinkedIn

https://www.linkedin.com

Réseau social d’entreprise

Vous créez tous un compte tout de suite. Vous ajoutez tout le personnel de PopSchool et les personnes travaillant au LLV, votre famille, vos amis, vos anciens collègues, camarades d’école etc. On imagine mal la portée réelle de son réseau *+2* !

### Les Job Boards

Pour trouver du travail, vous pourrez ignorer Pole Emploi.

Les job boards sont des sites webs, majoritairement privés (sauf l’APEC) et gratuits (conformément à la loi) proposant des offres d’emploi et où les recruteurs peuvent découvrir votre CV.

Allez vous créer un compte sur les sites suivants:
* welcometothejungle.com
* indeed.fr
* apec.fr (c’est un peu en lien avec Pole Emploi en fait …)
* etc.

Vous allez être contactés dans les semaines qui viennent, n’oubliez pas de répondre. Ne fermez jamais définitivement une porte !

### Être présent en ligne

* avoir un github actif !
* être actif sur twitter, linkedIn etc.
* participer à un projet libre
* créer des tutos
* avoir une chaîne youtube

Si vous êtes actifs sur Internet et que vous indiquez être en recherche d’emploi, vous serez contactés.

## Postuler

### Intitulés des postes

Vous êtes des développeurs webs :
* Web designer
* Développeur front end
* Développeur back end
* Développeur full stack

Selon l’intérêt que vous portez au sujet vous pouvez également faire:
* Administrateur système
    * dev ops
* Administrateur base de données (DBA)
* Technicien support
* UI/UX Designer si vous avez des appétences pour la conception graphique
* Intégrateur HTML (job bof bof, mais ça existe)

👉 Cette liste est évidement non-exhaustive

#### Quand postuler

Si vous avez 50% des compétences techniques demandées. Vous pouvez totalement ignorer le reste (exigence d’expérience, de diplôme etc.)

Vous pouvez signer un CDI là-maintenant-tout-de-suite-cassez-vous-de-là-que-je-prenne-des-vacances. 

👨🏾‍🎓 vous pouvez quitter PopSchool si on vous propose un poste de dev tout de suite. On s’arrangera pour le titre pro !

#### Sites d’annonces

* tous les jobs boards qu’on a vus avant
* [Euratechnologies à Lille](http://www.euratechnologies.com/euratechjob/offres)
* [Plaine Image à Roubaix](http://www.plaine-images.fr/offres-demploi/)

Le LLV rêve d’en avoir un : proposez leur de venir en stage (prévoyez 6 mois pour être sérieux)

#### Maraude

Aller sur les annuaires d’entreprises, visiter leurs sites web. Souvent on y trouve une section emploi.

À Lens vous pouvez parcourir :
* https://www.club-tactic.fr/
* https://lafrenchtech.com/en/community-no-content-en/artois/

#### Candidature libre

Vous pouvez envoyer un CV à toute entreprise qui vous semble intéressante même si elle ne recrute pas. Elle pourrait transmettre votre CV à des confrères !

### Que dire ?

**Faites très attention à votre orthographe**

* Que vous cherchez un stage
* Des détails sur le stage : durée (2 mois), mode (temps plein), éventuellement rémunération (à vous de voir entre rien, facultatif ou 550€/mois)
* Évoquez PopSchool (6 mois, titre pro développeur web niveau 3)
* Que vous êtes grand débutant
* Personnalisez le mail en montrant que vous vous êtes un minimum intéressé à l’entreprise (rien n’est plus gonflant que d’avoir l’impression que le mail est un copier/coller)
* Dites du bien de vous en quelques mots
* Dites au revoir (Bonne journée, Cordialement, Bien à vous …)

Points d’attention :  
* Inutile de dire votre nom : il apparaît dans le client mail
* Je vous ai dit de faire attention à l’orthographe ?
* Inutile de faire un mail trop long, il ne sera pas lu. Allez droit au but
* Le corps de mail (ou la zone pour écrire d’un formulaire de contact) font office de lettre de motivation : inutile d’en ajouter une !
    * Si vous ne postulez pas par mail et qu’il n’y a pas de zone pour écrire, alors ajoutez votre texte **au format PDF**

Créez-vous un template de mail : tous les textes sont prêts et relus, il ne vous reste plus qu’à remplir la section personnalisée sur l’entreprise (sans faire trop de faute d’orthographe, on se comprend ? :))  
Faites relire votre template par vos proches qui sont à l’aise avec l’écrit, mais soyez attentif à ce que ça vous aille bien. C’est VOTRE lettre de motivation, pas  celle d’un autre ! Les tournures de phrases, les mots etc. doivent vous plaire 

![Template de lettre de motivation](images/template-ldm.png)


#### Exemples concrets

(here be exemples concrets from my inbox)

## Conseils

* Postulez beaucoup, au moins une  fois par jour
* Tenez une liste des entreprises contactées pour éviter les doublons. Il y a des logiciels en ligne pour ça !. Il y a des logiciels en ligne pour ça !
* N’ayez pas peur : vous ne serez jamais pris si vous ne postulez pas
* Postulez énormément, genre deux fois par jour
* Soyez vous-même, ne vous forcez pas à *faire des phrases*
* Sur votre CV, n’hésitez pas à mettre vos expériences pro même en *job de merde*. Ils prouvent votre capacité à travailler même sur des jobs peu intéressants; votre capacité à vous lever tôt, avoir une hiérarchie pressante, des contraintes de productivité etc.
* Postulez sans arrêt, hein, genre plusieurs fois par jour
