# Cours de JavaScript avancé

Philippe Pary, 2022

## Objectifs du cours

Assez rigolé, on ne peut rien faire avec ce qu’on a appris. Bon, avec ce qu’on va apprendre là, on progresse un peu. ’fin on peut faire un jeu solo quoi…

➡ apprendre à créer des objets  
➡ exécuter des requêtes au travers du grand Internet  
➡ comprendre les fichiers JSON, et éventuellement les argonautes  
➡ utiliser le localStorage  
➡ si vous êtes sages et gentils, utiliser la géolocalisation, la caméra ou encore le vibreur

## Les objets

On a vu comment créer une variable tout simple:

    var myVar = 5;

On a vu comment créer un tableau:

    var myArray = ["patato","woody","buzz"];

On a même vu comment créer une fonction:

    function myFunction(parameter) {
        console.log(parameter);
    }

ou bien

    var myFunction = function(parameter) {
        console.log(parameter);
    }

Nous avons manipulé les objets sans apprendre à en créer en revanche … rhooooo !

    var myObject = {
        initialValue: 5,
        printValue: function() {
            console.log(this.value);
        }
    }

La notation JavaScript permet d’accéder aux propriétés d’un objet (variable, fonction, sous-objet) par un `.`

    doccument.body.innerHTML = "Hello world!";

On peut enchaîner à l’infini ces propriétés séparées par des `.`

    document.querySelector("#myDiv").style.width = "100%";

Voici une décomposition visuelle de ce qui se passe avec tous ces `.` …

![Merci Justine M !](images/jean-pierre.jpg)

## Les requêtes sur Internet: fetch

Javascript prend son envol moderne avec la possibilité d’aller chercher ou d’envoyer des données vers un serveur. Vous croiserez encore le terme _AJAX_ (rien à voir avec Amsterdam, c’est _Asynchronous Java And XML_) ou _Web2.0_ (c’est soooo 2005 de dire ça)

Vous pourriez tomber nez à nez avec l’infâme `XMLHttpRequest` aussi connu sous le nom de `xhr`. Vous ne devez jamais prononcer son nom et, si vous le croisez, fuyez avant qu’il n’ait le temps d’aspirer votre âme.

Tous les sites modernes abusent de ces possibilités. C’est comme ça que Facebook peut savoir combien de temps vous restez bloqués sur un article ou si vous passez trop souvent la souris sur quelqu’un. Ou même qu’ils enregistrent les messages non-envoyés.

<https://developer.mozilla.org/fr/docs/Web/API/Fetch_API>

### Exercice 1: aller chercher une image

⚠ cet exercice ne présente que peu d’intérêt: ce que nous allons faire pourrait être fait sans `fetch`, en modifiant la valeur `src` de la balise image directement. Le but est de vous apprendre `fetch` sans avoir à apprendre `JSON` immédiatement

Nous allons récupérer dynamiquement le logo de Wikipédia (https://www.wikipedia.org/portal/wikipedia.org/assets/img/Wikipedia-logo-v2.png)

➡ Créer une page HTML, mettre une image avec un `id` et mettre un bouton  
➡ Créer un script JS, ajouter un listener click au bouton et lui donner le code ci-dessous  
➡ Ça devrait marcher :)

    var myImage = document.querySelector("#myImage");
    fetch("https://www.wikipedia.org/portal/wikipedia.org/assets/img/Wikipedia-logo-v2.png")
        .then(response => response.blob())
        .then(function (myBlob) {
            var objectURL = URL.createObjectURL(myBlob);
            myImage.src = objectURL;
        });

La solution est disponible ici: <http://jsfiddle.net/uf4h3sgq/>

➡ les images sont des _blobs_ c’est à dire un tas de texte hexadécimal. La fonction URL.createObjectURL n’est vraiment pas à retenir, ça crée une adresse à l’image qu’on a téléchargée  
➡ on vient de récupérer une image par Internet

## JSON

Le JSON est un format de données. Ça signifie _JavaScript Objects Notations_

Avant que vous ne me le demandiez, non ça n’exporte pas les fonctions. Elles se perdent dans l’opération.

Le format est assez simple, prenons un JSON totalement vide:

    {}

Prenons un JSON avec une variable _status_ qui vaut _ok_

    { "status": "ok" }

Prenons un JSON avec le tableau des élèves contenu dedans

    {
        "status": "ok",
        "students": ["Alexandre.C","Alexandre.B","Benoît","Donatien","Hugo","Myriam","Youcef","Nicolas","Vlad","Quentin rmc","Quentin Kiou","Steven","Loïc","Julian","Maxence","Thomas","Amandine", "Tristan"]
    }


Notez que c’est très proche de la rédactions des objets. Mais on ne peut pas passer de fonctions en JSON 😪

Pour enregistrer une variable, un tableau ou un objet en JSON, on utilise `myJSON = JSON.stringify(myVar)`. À l’inverse, pour transformer un JSON en variable, tableau ou objet, on utilise `myVar = JSON.parse(myJSON)`

### Exercice 2 : liste des étudiants

Vous reprenez l’ancien exercice affichant la liste des étudiants (exo 5 de JS1) et vous refaites la même en affichant plus d’formations et en exploitant la donnée JSON fournie ci-dessus.

Amélioration: la même mais en _fetchant_ la liste des étudiants depuis  `https://pachyderme.net/students.json`

➡ On va recevoir du JSON: le premier `then` contiendra donc `response => response.json()`  
➡ C’est magique, la fonction du second `then` est notre JSON déjà parsé ! Rien à faire, c’est un objet JS valide

Jetez un œil du côté des `cards` de tailwind (https://tailwind-elements.com/docs/standard/components/cards/), y’a de quoi faire de jolis trucs

## LocalStorage

Alors là c’est facile comme tout.

Le soucis avec JavaScript, c’est que si on ferme le site web, on perd toutes les données.

On peut les enregistrer localement avec le `localStorage`. Bon, il existe d’autres manières d’enregistrer, mais on va se concentrer sur celle-là.

En `localStorage` on ne peut enregistrer que du texte. On va donc souvent utiliser `localStorage` avec du JSON !

Il y a 3 fonctions:
* Enregistrer une donnée: `localStorage.setItem("nomDEnregistrementLocal", JSON.stringify(myVar))`
* Lire une donnée: `myVar = JSON.parse(localStorage.getItem("nomDEnregistrementLocal"))`
* Supprimer une donnée: `localStorage.removeItem("nomDEnregistrementLocal")`

L’inspecteur a une section où on peut contrôler le contenu de son `localStorage` ce qui peut évidement s’avérer très utile.

### Exercice 3

Vous allez créer une page web avec un champ d’input pour enregistrer en `localStorage` un nom de promotion.

Vous allez créer un bouton pour charger le nom de promotion et un dernier pour supprimer le nom enregistré.

La solution est postée ici: <http://jsfiddle.net/Lajgynz8/>

## et plein d’autres API …

### Geolocalisation

sorti tout droit de <https://developer.mozilla.org/fr/docs/Using_geolocation> 

      var output = document.getElementById("out");

      function success(position) {
        var latitude  = position.coords.latitude;
        var longitude = position.coords.longitude;

        output.innerHTML = '<p>Latitude is ' + latitude + '° <br>Longitude is ' + longitude + '°</p>';

        var iframe = document.createElement("iframe");
        iframe.src = "https://www.bing.com/maps/embed?h=400&w=500&cp=" + latitude + "~" + longitude + "&lvl=12&typ=d&sty=r&src=SHELL&FORM=MBEDV8";

        output.appendChild(iframe);
      }

      function error() {
        output.innerHTML = "Unable to retrieve your location";
      }

      output.innerHTML = "<p>Locating…</p>";

      navigator.geolocation.getCurrentPosition(success, error);

### Vibreur

Voir <https://developer.mozilla.org/en-US/docs/Web/API/Navigator/vibrate>

    navigator.vibrate([500,100,250,100,500,500,100,250,100,500])

### Camera

⚠ Attention au Larsen. Vraiment

    // Prefer camera resolution nearest to 1280x720.
    var constraints = { audio: true, video: { width: 1280, height: 720 } }; 

    navigator.mediaDevices.getUserMedia(constraints)
    .then(function(mediaStream) {
      var video = document.querySelector('video');
      video.srcObject = mediaStream;
      video.onloadedmetadata = function(e) {
        video.play();
      };
    })
    .catch(function(err) { console.log(err.name + ": " + err.message); }); // always check for

## Exercices

Si vous faites tout ça, on sera bien. Mais vraiment bien. 
Genre la prochaine étape, ça sera après le cours de Daishi sur Symfony, la partie dédiée à la génération d’une API

### Extension de l’excercice 1: allons chercher plusieurs images

Préparez une liste d’image que l’utilisateur pourra aller chercher, soit via un bouton par image, soit via une liste déroulante

### Extension de l’exercice 2 : Gérer la liste des étudiants

Comme pour le cours précédent, je vous propose de prévoir des fonctions d’édition, ajout et suppression d’étudiants. 

### Extension de l’exercice 2 : Gérer la liste des étudiants

Comme pour le cours précédent, je vous propose de prévoir des fonctions d’édition, ajout et suppression d’étudiants. 

### Extension de l’exercice 3 : Charger les données au démarrage

Bah oui, à quoi ça sert de sauvegarder les données si on ne les charge jamais ?

Au lancement, la page contrôle s’il existe des données en localStorage et, le cas échéant, les charge

Indices en vrac:
* `window.localStorage("thisOneDoesntExsists")` renvoie la valeur `null` que vous pourrez tester dans un if ` === null`


### Fusion de l’extension de l’exercice 2 et de l’extension de l’exercice 3

Non-seulement on peut ajouter/modifier et supprimer des étudiants, mais en plus, vous enregistrez les données en localStorage et vous les chargez au lancement de la page
