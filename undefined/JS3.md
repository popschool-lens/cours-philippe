# Cours de JavaScript, travail avec les API

Philippe Pary, 2018

## Objectifs du cours

Ayé, on avance vers les choses pratiques. Aucune nouvelle motion, juste la mise en œuvre de ce qu’on a vu.

Prévoyez des dolipranes, il va falloir réfléchir beaucoup

➡ comprendre ce qu’est une `API`  
➡ comprendre la notion de CRUD  
➡ réussir à récupérer les données d’une `API` et les afficher  
➡ réussir à envoyer des modifications/suppression de données vers une `API`  

## Une API ?

Une `API` est une interface de programmation applicative. C’est une boîte noire qui suit certaines règles (`REST`, `SOAP`, `GraphQL`…) qui va servir à gérer les données et fournir des services (tirer un chiffre au hasard par exemple)

Les `API` reposent sur des **entry points** (ou un seul entry point pour `GraphQL``). Ce sont des adresses documentées, on n’a pas à les deviner par nous-même.

Les `API` reposent également sur les **verbes HTTP** qui sont des paramètres transmis à chaque requête HTTP:
* `GET`, lecture/accès. C’est la requête par défaut quand vous tapez une URL dans un navigateur web
* `POST`, envoi (sert pour créer ou compléter une ressource)
* `DELETE`, suppression
* `PUT`, modification (la différence avec `POST` est subtile)
* `PATCH`, correction (ahem… donc `POST`, `PUT` ET `PATCH`). Heureusement, on le croise peu

Vous utiliserez l’API créée avec Daishi (voir https://github.com/jibundeyare/symfo-p9)

### Premier exercice

À l’aide de l’interface graphique proposée par `api-platform`, réalisez chacun des actions CRUD sur chacune des entités

## Les explorateurs d’API

Les vrai.e.s, les dur.e.s, les tatoué.e.s utiliseront le logiciel `httpie` : https://httpie.org/

Les autres, les hipsters, les moules du bulbes utiliseront le logiciel `postman` : https://www.getpostman.com/

Les API modernes embarquent leur explorateur intégré d’API du genre `api-platform` : https://api-platform.com/

Ces logiciels vont permettront d’explorer l’API.

### Second exercice

Au travers du logiciel de votre choix, `httpie` pour mes chouchous, `postman` pour les hipster et l’interface `api-platform` pour la plèbe, visionnez la documentation de l’API évoquée plus haut. Vous devez être capable d’expliquer chaque endpoint : ses paramètres, ses valeurs de retour, sa méthode HTTP … 

## Utiliser les données

Vous l’avez repéré, les données transitent en `JSON`. Vous connaissez.

En mixant `fetch`, votre connaissance de `JSON` et un peu de tout le reste, on va se faire la plus belle application de CMS du monde.

### Troisième exercice

Afficher les articles d’une catégorie via une petite application JavaScript

➡ Récupérez les informations de la catégorie 1️⃣ via un `fetch` et affichez via un `console.log`
➡ Améliorez la fonction, affichez le nom de la catégorie dans un `h1`
➡ Récupérez la liste des articles via un `fetch` et affichez via un `console.log`  
➡ Améliorez la fonction, affichez la liste des articles sur la page web  
➡ Rendez-moi ça incroyablement beau 

### Quatrième exercice

Ajouter un champ pour créer une catégorie

➡ Vous ajoutez un champ de formulaire, quand on clique dessus, ça crée un article dans la catégorie

➡ pensez à faire des essais via `httpie` ou `postman` avant
➡ créez un formulaire avec les différents champs requis pour une catégorie
➡ mettez un bouton qui lance la fonction de création de la catégorie quand on clique dessus
➡ il va falloir récupérer la valeur des éléments du formulaire
➡ mettez ces valeurs sous la forme d’un beau `JSON`
➡ envoyez le tout via une requête `fetch`
➡ lisez la MDN et **n’hésitez pas à vous faire aider**. Dans la vie, on est jamais trop aidés !


### Cinquième exercice

Créer une page où on peut modifier ou supprimer une catégorie

➡ On va récupérer l’`id` de la catégorie à modifier via un paramètre `GET` (ie un paramètre dans l’URL). Voir https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams/URLSearchParams (on, il n’y a pas de VF, on a atteint le bout de ce qui est documenté en français)  
➡ **sur la page des catégories** mettez un lien pour chaque catégorie
➡ commencez par récupérer les données de la catégorie
➡ reproduisez le formulaire de création de catégorie
➡ dans les champs du formulaire, mettez les informations récupérées
➡ Faites vous aider pour la rédaction du `fetch` mais cette fois-ci ça devrait aller un peu mieux …


### Sixième exercice

Création d’article

➡ sur la page d’une catégorie, on ajoute la liste des articles liés à cette catégorie
➡ en bas de cette page, on met un formulaire pour créer un article
➡ sur chaque article on met un lien vers la page d’édition / suppression
➡ sur la page d’édition / suppression d’un article, on ajout le formulaire de modification de l’article
➡ … et un bouton de suppression 

### Et pour se marrer

➡ listez les tags en mettant les articles en rapport
➡ listez les utilisateurs en mettant les articles en rapport
➡ quand l’app est fonctionnel, éteignez votre ordinateur
➡ trouvez des camarades et réfléchissez à une manière ergonomique de présenter tout ça (ie faire des maquettes)
➡ lancez la création d’une application fonctionnelle et intéressante
➡ avec une méthode agile (on aura un cours là dessus)
➡ en utilisant le bug tracker de github
➡ en travaillant sur des branches
➡ (à votre guise) vous pouvez utiliser Vue directement ou refaire l’app en Vue par la suite
