# Cours de CSS pour le responsive Design

©Philippe Pary pour PopSchool (http://pop.eu.com) 2018

# Responsive design ?

Aujourd’hui plus des deux tiers des visites sur Internet se font depuis un GSM ou une tablette. Ce sont des écrans de petite taille.

Historiquement le web a été conçu pour des écrans de grandes tailles d’ordinateurs fixes ou portables. Il y a donc eu des ajouts à CSS pour permettre la gestion de tailles d’écrans très différentes, allant de la télé d’un mètre de diagonale à la montre connectée et son écran de 2cm …

Plusieurs méthodes sont possibles pour réaliser un site responsive:

1. Penser *mobile first*: un site conçu pour les écrans les plus petits sera adapté pour les écrans les plus grands. Au pire, il fait un peu vide. L’inverse ne marche pas du tout.
2. User et abuser de l’attribut `max-width` et `max-height` qui vous permettent d’éviter qu’un élément ne déborde de l’écran
3. Savoir utiliser au moins de manière bourine le meta `viewport` de HTML
4. Site dédié ? Site commun ?
5. Les media queries !
6. Design fluid ? adaptative ? responsive ?
7. Layout `flex`
8. Layout `grid`
9. Balise `<picture>`

## Mobile first

*Qui peut le moins, peut le plus*

Si vous commencez par développer par le mobile, le site marchera pour les écrans les plus grand. Il paraîtra certainement très épuré mais ça marche.

À l’inverse, un site trop fouilli prévu pour grand écran ne marchera jamais sur un plus petit écran.

## `max-width`, `max-height`, `overflow`

* <https://developer.mozilla.org/fr/docs/Web/CSS/max-width>
* <https://developer.mozilla.org/fr/docs/Web/CSS/max-height>
* <https://developer.mozilla.org/fr/docs/Web/CSS/overflow>

*Ma banière est jolie mais ne devrait jamais recouvrir plus de 25% de la hauteur de la page*

Pas de soucis ! On lui attribue un `max-height`: 25%;

*Oui, mais elle déborde sur les petits téléphones, même à 25%*

Rhô, on lui met un `max-width: 100%;`

*Bah non, elle devient vraiment trop petite verticalement, c’est presque inutile*

Oh ? Retirons le `max-width` et mettons en place un `overflow-x: hidden;` qui cachera le contenu qui déborde sur le côté.


## Viewport

* <https://developer.mozilla.org/fr/docs/Mozilla/Mobile/Balise_meta_viewport>
* <https://www.alsacreations.com/article/lire/1490-comprendre-le-viewport-dans-le-web-mobile.html>

     &lt;meta name="viewport" content="width=device-width, initial-scale=1.0"&gt;

Les sites webs étant initialement conçus pour les écrans d’ordinateurs fixes ou portables, les GSM et tablettes ont eu à merdouiller pour afficher les sites webs existants: dézoomer tout la page et laisser l’utilisateur zoomer, dans un premier temps, puis utiliser quelques hacks pour tenter de faire une page accessible sur mobile.

Ces hacks s’accompagnent mal d’un design moderne, directement responsive. Le `meta-viewport` indique au navigateur web de votre GSM/tablette d’afficher la page à ses réelles dimensions, sans tenter ses bidouilles.


## Sité dédié ? site commun ?

Une solution carrément bourrine est, plutôt que de se prendre le chou avec du responsive design et des media-queries à gogo, de créer des versions du site web pour tous les types d’écrans.

✅ Ça présente l’avantage de ne pas charger du contenu inutile car caché par un `display:none` ni de devoir charger des fichiers CSS faisant 1500lignes là où 500 suffiraient !\\
❌ Ça a pour inconvénient que votre SEO est mauvais : google va voir deux sites webs au lieu d’un seul !\\
💡 Vous pouvez jouer avec une balise `link` en donnant la valeur `canonical` à son attribut `rel`. Exemple: `<link rel="canonical" href="http://www.monsupersite.com/masuperpage.html`>` indiquera aux moteurs de recherche que votre page est un doublon de `http://www.monsupersite.com/masuperpage.html`. Vous évitez ainsi les doublons de référencement : hourra !\\
💡 Pensez à afficher un lien pour passer d’une version à l’autre du site web.\\

On peut tenter de détecter par un petit bout de JavaScript si l’utilisateur est sur un GSM/Tablette, ou du moins sur un petit écran et rediriger vers le site adapté…

    var isMobile = function() {
        console.log("Navigator: " + navigator.userAgent);
        return /(iphone|ipod|ipad|android|blackberry|windows ce|palm|symbian)/i.test(navigator.userAgent);
    };
    if(isMobile() || window.screen.width < 1000px) {
        window.location = "<URL small>";
    }
    else {
        window.location = "<URL big>";
    }


## Media-queries

* <https://developer.mozilla.org/fr/docs/Web/CSS/Requ%C3%AAtes_m%C3%A9dia/Utiliser_les_Media_queries>
* <https://www.alsacreations.com/article/lire/930-css3-media-queries.html>

Les media queries sont un moyen, en CSS, de définir des styles qui ne s’appliquent que sous certaines conditions. On les reconnaît à leur syntaxe `@media` dans les fichiers CSS.

Dans l’exemple suivant, la classe `tab_menu` par défaut fait 15em de large et floate sur la gauche. Si l’écran est d’une largeur inférieure à 1024px, la largeur est redéfinie à 5em et l’éventuel contenu qui déborderait est masqué.

    .tab_menu {
        width: 15em;
        float: left;
    }
    @media (min-width: 1024px) {
        .tab_menu {
            width: 5em;
            overflow: hidden;
        }
    }

On peut utiliser plusieurs expressions: `@media (min-width: 740px) and (max-width: 1024px)`

Une expression, c’est un opérateur (`min`, `max`) et une caractéristique (`width`, `aspect-ratio`) donnant une expression du type `min-aspect-ratio`

Pour les caractéristiques, on utilise surtout `width` et `width`. Voici quand même une liste de caractéristiques notables:

* `orientation`: `landscape`, `portrait`
* `aspect-ratio`: `1/1`, `4/3`, `16/9` …
* `width` et `height`

On peut aussi utiliser les *media types* pour définir des règles spécifiques pour l’impression notament.

`@media print { }`

* screen: écran, normal quoi …
* print: imprimante
* tv: télévision
* projection: vidéo-projecteur
* braille: claviers brailles
* speech: synthèse vocale
* tty: ligne de commande
* all: tout type de media

**ATTENTION**

Les media queries sont comme tout le CSS: la dernière propriété s’applique. Même si elle n’est pas contrainte par un `@media`

## Fluid design, adaptative, responsive

* <https://www.alsacreations.com/article/lire/1615-cest-quoi-le-responsive-web-design.html>

On a 3 grandes catégories de designs pour gérer les petits écrans. C’est une vision qui pour vous, première génération de dev en mobile first, devrait vous paraître surannée. 

### Le fluid design

* <https://developer.mozilla.org/fr/docs/Web/CSS/length>

On n’utilise pas de pixels comme unités: tout est en pourcentages, en `em` (taille de police), `mm` (distance), `vh` et `vw` (*height* et *width* en pourcentage du viewport. Comme le % mais en mieux quoi !)

Coder fluide c’est avoir fait 80% du boulot. Et ça prend 20% du temps.

### L’adaptative design

On utilise les *media queries* pour adapter la largeur des colonnes, voir au besoin créer des retours à la ligne. Ça prend vraiment un max de temps, aussi le fait-on rarement à la main et on s’appuie sur un framework CSS comme *Bootstrap* (<http://getbootstrap.com>) ou *Materialize* (<http://materializecss.com/>)

### Le responsive design

Là on repense carrément tout d’un media à l’autre. Ça mélange le fluid et l’adaptative. C’est carrément chiant à gérer, ça bloate parfois, mais l’expérience utilisateur passe de bien à génial ! *pouce levé*

## La propriété CSS `flexbox`

* <http://flexboxfroggy.com/>
* <https://developer.mozilla.org/fr/docs/Web/CSS/Disposition_des_bo%C3%AEtes_flexibles_CSS/Utilisation_des_flexbox_en_CSS>

Cette propriété  est assez récente, les anciens navigateurs ne la supportent pas, ce qui peut poser des soucis si votre public est bloqué sur des anciennes versions des navigateurs webs <http://caniuse.com/#feat=flexbox>

    <div class="flexbox">
        <div class="flexbox-element"></div>
        <div class="flexbox-element"></div>
        <div class="flexbox-element"></div>
    </div>

    .flexbox {
        display: flex;
        direction: row;
        height:100%;
        align-items: center;
    }

    .flexbox-element {
        flex: 1 1 auto;
        width: 30px; height: 30px;
        transition: width 0.5s ease-out;
    }
    .flexbox-element:hover {
        width: 1000px;
    }
    .flexbox-element:nth-child(1) { background: red; }
    .flexbox-element:nth-child(2) { background: green; }
    .flexbox-element:nth-child(3) { background: blue; }

## La propriété CSS `grid`

* <http://cssgridgarden.com/>
* <https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Grid_Layout>

Cette propriété  est assez récente, gérée que par les navigateurs les plus récents. <http://caniuse.com/#feat=grid>

    <div class="grid">
        <div class="grid-element"></div>
        <div class="grid-element"></div>
        <div class="grid-element"></div>
    </div>

    .grid {
        display: grid;
        grid-gap: 5px;
        grid-auto-rows: minmax(100px, auto);
        grid-template-columns: repeat(3,1fr);
    }
    .grid-element {
        grid-column: 1;
        grid-row: 1;
    }
    .grid-element:nth-child(1) {
        background: red;
        grid-column: 1;
        grid-row: 3;
    }
    .grid-element:nth-child(2) {
        background: green;
        grid-column: 2 / 4;
    }
    .grid-element:nth-child(3) {
        background: blue;
        grid-column: 4;
        grid-row: 1 / 4;
    }

## La balise `<picture>`
* <https://developer.mozilla.org/fr/docs/Web/HTML/Element/picture>

Cette balise est très récente, toujours le même soucis avec les anciens navigateurs <http://caniuse.com/#feat=picture>

… sauf que cette balise a été bien pensée : dans le pire des cas, le navigateur affichera l’image par défaut sans tenir compte de votre *media-query*.

La balise `<picture>` vous permet de définir une image et, à côté, de donner des liens vers des versions adaptées de l’image en fonction d’une media-query.

Ainsi vous pouvez tout à fait faire 3 versions de votre bannière, de sorte à avoir des bannières qui s’affichent parfaitement sur tous les types d’écrans

    <picture>
        <source srcset="banner-wide.png" media="(min-width: 1280px)">
        <source srcset="banner-large.png" media="(min-width: 1024px) and (max-width: 1280px)">
        <source srcset="banner-normal.png" media="(min-width: 740px) and (max-width: 1024px)">
        <source srcset="banner-tiny.png" media="(max-width: 740px)">
        <img src="banner-default.png" alt="Bannière du site web">
    </picture>
