# Professionnalisation

Philippe Pary

# Introduction

Cours visant à présenter l’organisation du monde du travail. Il est divisée en deux grandes parties:

1. Salariat
2. Entrepreneuriat

## Salariat

Être salarié, c’est signer un contrat de subordination: vous allez recevoir et accepter les ordres de quelqu’un en échange d’un salaire. Évidemment, tout ça est très encadré

### Postuler

Étape 0: postuler. En masse, y’a que ça qui marche. Vous pouvez soigner les candidatures particulières (entreprise motivante, lieu de travail à proximité etc.) mais ce qui marche c’est la masse

* Tenez un tableur avec la liste des entreprises où vous avez postulé et avec la date. Au bout de 15 jours, relancez, notez le
* Préparez un template de mail avec des sections à personnaliser en fonction de l’entreprise où vous postulez
* Squattez les événements informatiques et les espaces de coworking pour être informés rapidement des opportunités
* Vous êtes débutant: vous avez certes une formation et des envies, mais vous êtes ouverts à toute proposition
* Le marché de l’emploi est en notre faveur
  * mais ça n’empêchera pas certains employeurs de mal se comporter, hein
* last but not least: on a le droit de mentir sur son CV
  * mais évitez de trop en faire, l’employeur a le droit de vérifier ce que vous racontez !
  * Vous avez tellement le droit que vous pouvez postuler sous pseudonyme !

### Entretien de recrutement

Vous en avez passé au moins un pour entrer à PopSchool et vous l’avez réussi.

* Soyez à l’heure
* Soyez lavés du jour
* Ayez des fringues propres, mettre une chemise en entretien est une bonne idée, même si vous serez en t-shirt par la suite
* Renseignez-vous sur la société un max, préparez des questions, même bidons, pour montrer que vous vous êtes intéressés. Sachez au minimum l’activité et le nom du patron …
* Posez des questions sur les conditions de travail (horaires, machine à café, souplesse pour les parents, évolution salariale etc.) pour montrer que vous y vous voyez déjà (c’est comme le titre pro: faut dire des choses évidentes !)
* Si vous avez des contraintes prévisibles (15 jours de congés dans les prochains mois), évoquez le
* Votre prétention salariale est de 25000€ bruts annuels (ça fait autour de 1500€/net par mois), ce sont les meilleurs salaires constatés en sortie de PopSchool. C’est votre prétention, vous pouvez cependant accepter à moins hein … (on en parle plus loin)

Vous pourrez avoir un entretien technique ou un test technique en ligne

* Terminez le, vous êtes débutant et c’est normal que vous ne compreniez pas la moitié
* Si vous rencontrez un problème, dites le. C’est comme à PopSchool ! Si l’écran est cassé, si le clavier foire, vous le dites rapidement
* Assumez votre niveau, vous êtes en face d’un technicien à qui on la fait pas

### Accord

Hourra, vous faites l’affaire ! Vous allez recevoir un mail ou être à nouveau reçu pour un dernier entretien.

Si l’embauche est rapide (*« tu commences lundi »*)

* Si vous avez une contrainte, faites en part (*« j’ai un rendez-vous chez l’ophtalmo lundi … ça ne vous dérange pas si je pars à midi ? »*)
* Transmettez votre attestation de carte vitale et une copie d’une pièce d’identité. Vous pouvez exiger que votre véritable identité soit tenue secrète
* Transmettez votre RIB
* Un contrat de travail se paraphe (on met ses initiales en bas de chaque page) en plus d’être signé à la fin. Vous devez en avoir un exemplaire
  * Un contrat de travail est facultatif, si vous n’en avez pas, demandez simplement le justificatif de la DPAE (déclaration d’embauche auprès de l’État)
  * Si y’a ni contrat de travail ni DPAE, ne commencez pas le travail

Si l’embauche est plus lointaine (*« la mission commence dans 4 mois, donc on t’embauchera à ce moment là »*)

* Demandez une promesse d’embauche
  * Contient le jour précis du début du contrat
  * Contient le salaire prév
  * Engage votre employeur
  * Ne vous engage pas: vous êtes libres de rompre une promesse d’embauche pour travailler ailleurs
    * Par contre vous devez immédiatement prévenir l’employeur éconduit dès que vous avez commencé ailleurs
  * Ce document peut vous aider à trouver un appartement ou à avoir des autorisations de découvert auprès de la banque 
    * Il vous dispense de recherche de travail par Pole Emploi !

### La période d’essai

En CDI 2 mois reconductibles une fois, 3 mois si vous êtes cadre.

En CDD, une semaine _quelque soit la durée du CDD_ (max 2 ans, reconductible une seule fois)

C’est une période où l’entreprise vérifie si vous êtes bien celui ou celle que vous prétendiez être.

Toute période de travail faite dans l’entreprise (stage, mission freelance, CDD etc.) est déduite de cette période d’essai.

La reconduction est censée être motivée (il doit subsister un doute) mais elle est toujours reconduite … Sachez dans un coin de votre tête que c’est illégal: si on vous vire au 4ème mois, vous pouvez faire chanter votre employeur :)

La période d’essai, c’est licenciement libre. En gros, quand bien même vous n’avez commis aucune faute ni menti sur vos capacités, vous pouvez être virés

* Le premier jour est souvent consacré à l’installation: mettez en place votre ordinateur, allez vous présenter à vos collègues
* La première semaine est souvent assez creuse mais respectez les horaires ! Vous ne voudriez pas faire mauvaise impression en période d’essai …
* Imprégnez-vous de la culture d’entreprise: à qui dois-je dire bonjour le matin, comment marchent les pauses, comment est géré la cuisine collective etc.

Si vous faites une bonne impression la première semaine,c’est souvent gagné :)

La fin de la période d’essai ne signifie pas que vous pouvez arriver à 11H en caleçon. Il existe de nombreux moyens pour l’employeur de « prolonger » artificiellement la période d’essai. Tenez-vous à carreau encore un mois après la période d’essai …

### Quelques notions de droit du travail

Jetées en vrac …

Vos devoirs:

* Obéissance: vous devez obéir aux ordres de vos supérieurs. C’est à dire votre employeur (celui qui a signé votre contrat de travail ou toute personne lui ayant succédé dans la boîte) et tous ceux que votre employeur aura désigné comme vos supérieurs. 
  * Évidement, dans le cadre de votre contrat de travail et fiche de poste
  * Ne pas obéir est de l’insubordination, c’est une clause de licenciement immédiat pour faute grave
* Courtoisie: s’il est tout à fait autorisé de s’énerver de temps à autre, la courtoisie est un devoir. Le manque de courtoisie envers l’employeur sera qualifié en rupture de la relation de confiance et licenciement immédiat pour faute grave
  * Frapper son employeur est une faute grave sauf si vous pouvez apporter témoignage que vous étiez sous pression
* Fidélité: vous ne devez jamais nuire aux intérêt de l’entreprise, vous devez respecter ses secrets (surtout commerciaux)
  * Vous épancher au comptoir du bar sur les tarifs des contrats négociés par votre entreprise est une faute lourde
* Respect des affichages obligatoires et du règlement intérieur
  * Quelque part se trouve obligatoirement un coin de mur où sont affichés des informations obligatoires. L’entreprise peut s’en servir pour y publier des consignes
  * Le non-respect de ces consignes peut être qualifié de faute lourde, notamment les contraintes sur le harcèlement sexuel (mais fumer ou être ivre ne vous vaudra qu’un avertissement)
* Un avertissement trace un acte grave que vous avez commis et pour lequel vous pourriez être licencié pour faute grave s’il recommencait

Vos droits:

* Si vous n’êtes pas cadre, on ne peut pas vous reprocher une initiative malheureuse (ex: avoir passé commande auprès d’une fournisseur)
* On ne peut vous reprocher que des erreurs techniques uniquement sur les technologies que vous déclariez maitriser lors de votre embauche ou pour lesquelles vous avez été formés par l’entreprise
* Toute heure supplémentaire est due. Si vous restez tard, faites le remarquer et tracez le par écrit. Au pire, vous demanderez le décompte quand vous partirez …
  * On ne peut pas vous forcer à rester au delà des heures prévues dans votre contrat de travail, ce serait de la séquestration et dans ce cas, vous devez porter plainte auprès de la police (et votre employeur risquera de la prison)
  * D’une manière générale, on ne peut pas vous empêcher de bouger, même durant les heures de travail (mais partir au milieu d’une réunion pourra déclencher un avertissement)
* On ne peut pas vous licencier ni modifier votre contrat de travail quand vous êtes en arrêt ou en congés
* Vous avez le droit de vous syndiquer et de distribuer des documents syndicaux selon des règles bien définies
  * Un syndicat est une association de défense des droits des travailleurs
  * On distingue les syndicats de salariés (CGT, Sud, FO, CNT…) des syndicats patronaux (MEDEF, CGPME, CFDT…)
    * Les premiers veulent que les salariés travaillent le moins possible pour le meilleur salaire et dans les meilleures conditions de travail possible
    * Les seconds veulent l’inverse
* Vous pouvez contester un avertissement et notamment demander une formation pour palier à un éventuel manque de compétence ou de savoir être
* Votre salaire doit être payé à date régulière
  * Votre employeur a le droit d’avoir un mois de retard *de facto* (aucun tribunal ne condamnera pour un mois de retard isolé)
  * Au deuxième mois de retard vous pouvez vous considéré comme ayant subit un licenciement sans motif réel
  * Si votre salaire est régulièrement en retard dans les 6 derniers mois, vous pouvez également vous considéré licencié sans motif réel
  * Si l’entreprise est en difficulté financière, elle doit faire jouer son assurance de garantie des salaires (AGS), assurance obligatoire
* Sans refus exprimé sous 2 jours ouvrés, toute demande de congés est considérée comme acceptée

### La fin du contrat de travail

Le contrat de travail prend fin par :
* la démission
* la faute lourde ou grave
* la rupture amiable
* le licenciement économique
* le décès du salarié
  * Oui ça peut paraître con, mais ça veut dire que vos enfants n’hériteront pas de votre contrat de travail…
* Et les autres cas: jackpot

#### La démission

**VOUS NE DÉMISSIONNEREZ JAMAIS BORDEL DE MERDE**

**JAMAIS**

**JAMAIS NOM DE DIEU**

#### Les fautes

On distingue deux types de fautes pour le licenciement:

* La faute grave: une très grosse erreur technique, avoir tapé votre patron, commettre régulièrement la même erreur technique etc.
  * Contrairement à son nom, la faute grave n’est pas très grave hein :-)
  * Ne paniquez donc pas si on vous parle de faute grave, c’est pas grave en réalité …
  * Vos congés payés sont dus, votre salaire est du jusqu’à la date où vous signez les documents de sortie de la société
  * vous avez le droit au chômage, Pole Emploi ne vous posera aucune question
* La faute lourde: vous avez cherché à nuire à l’entreprise. Elle est souvent doublée d’une plainte au pénal
  * Là on ne rigole plus
  * La faute lourde… bah c’est grave. On vous accuse d’avoir cherché à nuire sciemment
  * Selon la gravité de la faute, l’entreprise peut ne plus rien vous devoir du tout (elle commence à se rembourser immédiatement)
  * PoleEmploi peut refuser de vous verser des indemnités chômage et vous sommer de vous expliquer

#### La rupture amiable

Vous et votre employeur tombez d’accord pour mettre fin au contrat. C’est la **rupture conventionnelle**

* Vous aurez une indemnité calculée automatiquement par la DIRECCTE (pour 1 an de travail à 1500€/net/mois comptez 1000€)
* Vous avez le droit au chômage, PoleEmploi ne vous posera aucune question

#### Le licenciement économique

La boîte n’arrive plus à payer votre salaire. Elle doit donner quelques documents à la DIRECCTE et peut procéder à votre licenciement économique

* Vous avez évidement le droit au chômage et évidemment, PoleEmploi vous accueillera sans poser de questions
* Vous avez le droit à des dispositifs de formation très avantageux à mettre en place avec PoleEmploi

#### Le décès

T’es mort, forcément t’as du mal à aller au travail le matin…

#### Autres cas

* Licenciement sans motif réel

Autrement appelé le licenciement sec. Vous ne serez jamais viré sur ce motif car il n’existe pas pour l’employeur. Il vous fera licencier pour faute lourde. 

Le licenciement sans motif réel est une requalification faite par le tribunal des prudhommes, quand ça arrive, ça coûte très très très cher à l’employeur …

Si vous contestez la faute, vous avez le droit à être défendu gratuitement par un représentant syndical, même si vous n’avez jamais été salarié ! L’inspection du travail la plus proche de chez vous vous fournira les coordonnées des représentants syndicaux

Le plus souvent vous arriverez à un *accord transactionnel* (à ne pas confondre avec la *rupture conventionnelle*), une somme d’argent que l’entreprise vous verse pour que vous renonciez à toute poursuite …

Vous avez 2 ans pour contester un licenciement pour faute lourde.

* Abandon de poste

Votre patron refuse de vous licencier ou de signer une rupture conventionnelle ? Ne démissionnez pas, cessez simplement d’aller travailler.

Votre patron devra notifier vos absences constatées sous quinzaine et vous mettre un avertissement. Si, sous 2 semaines calendaires vous n’êtes pas retourné au travail, il doit vous licencier pour faute lourde.

Vous perdez au maximum un mois de salaire, mais vous retournez à PoleEmploi avec allocations et tout …

Malin, hein ? Attention, votre ancien patron va vous haïr, ne le faites que si votre relation est déjà totalement pourrie

* Harcèlement & cie

Si vous estimez avoir été victime (geste déplacé, propos raciste etc.) vous pouvez immédiatement cesser de travailler. Vous devez aller porter plainte auprès de la police et entrer en contact avec un syndicat ou un avocat. Si vous assemblez un dossier probant, l’affaire ira au pénal, sinon vous écoperez d’un avertissement, d’un licenciement pour faute lourde ou, si vous avez ébruité l’affaire (presse & cie) d’un licenciement pour faute grave (tentative de nuire à l’entreprise)

Ouais hein ?

### Le tribunal des prud’hommes

Concrètement vous ne finirez jamais au tribunal, vous signerez une _transaction_ avant d’y arriver

Ce sont des tribunaux avec des jurys paritaires (moitié de syndicats patronaux, moitié de syndicats salariaux)
  * Nombre de plaintes de salariés sont clairement abusives
  * La plupart des situations réellement litigieuses sont donc tranchées en faveur des salariés
  * Une entreprise condamnée régulièrement attire l’attention de l’inspection du travail …
  * C’est long, très long: entre 3 et 5 ans
  * L’avocat y est facultatif. Un dossier sérieux et préparé par votre syndicat peut suffire
  * Si vous voulez faire peur, faire appel à un avocat coûte autour de 300-400€, l’employeur cédera trèèèèèès vite
    * Pour un gain typique de 2500€ (évidement, ça peut varier énormément selon les situations !)

Donc sauf si vous êtes réellement fautif (auquel cas vous faites le dos rond et vous fermez votre gueule), tenez tête à votre employeur: il ne veut pas aller au tribunal

Dans les affaires ont dit:
*« Mieux vaut un mauvais compromis qu’un bon procès »*

## Entrepreneuriat

On va parler ici du travail sans contrat de travail: création d’entreprise, freelance, indépendant etc.

N’oubliez jamais, l’objet social d’une entreprise est de dégager du profit pour ses actionnaires.

### Quelques questions à se poser

#### Pourquoi créer ?

1. Un projet qui tient à cœur
2. Une opportunité de se barrer avec la clientèle de son employeur
3. L’indépendance … à voir

#### Quand créer ?

Avant tout: lancer un projet et fonder une société sont deux choses différentes ! Vous pouvez lancer votre projet sans fonder de société

J’ai un projet ? Je le travaille ! Je code chez moi le soir, je rencontre des partenaires potentiel, je commence à démarcher des clients …  
Il existe des créateurs jeunes, vous pourriez vous lancer tout de suite après Pop sur un projet.  
Vous pouvez aussi accumuler de l’expérience en entreprise avant de vous lancer

On ne fonde une société que quand c’est nécessaire:
* Vous devez émettre une facture (voir la section sur les factures plus bas)
* Vous allez recevoir des financements (prêts, subvention etc.)
* Vous avez besoin d’une personnalité morale (pour vous protéger juridiquement, pour poser certains dossiers …)

#### Avec qui créer ?

On s’associe, entre autres, avec :

* Un associé (*so far, so good, what a teacher !*)
* Un financeur
* Un porte-nom
  * pour contourner un interdit bancaire
  * pour bénéficier d’un prestige
  * faire office de figure de proue

### Facturer

Le cycle de facturation est un point essentiel de la vie des entreprises. L’argent est le carburant des sociétés

#### Une facture, kézako ?

Une facture est un ordre de paiement. Théoriquement, toute facture reçue doit être enregistrée en comptabilité.

* Elle suit la réalisation d’une prestation
* détaille ce qui a été fait, et à quel prix
* détaille les taxes appliquées
* définit les modalités de paiement
* précise l’identité de l’émetteur
* … et celle du destinataire

#### Le cercle de la vie

Voici le cycle complet de facturation

1. Contact commercial (*lead*), discussion avancée avec un prospect pour lui vendre quelque chose (un site web)
2. Devis (*proposition commerciale*), document contenant des éléments concrets correspondant à une proposition ferme
3. Devis signé, validation par le client du devis
4. Bon de commande, validation de la réception du devis signé
5. Facture d’acompte, acompte versé par le client afin de lancer les travaux
6. Bon de production, notification du début de la réalisation
7. Bon de livraison, notification de la fin des travaux
8. Facture, ordre de paiement de la réalisation, parfois reçue avec un bordereau de paiement
9. Facture acquittée, confirmation du paiement

Voici le vrai cycle en informatique

1. Contact commercial (*lead*), discussion floue autour d’un projet avec un prospect hyper-enthousiaste qui ne comprend pas combien ça va lui coûter et vous qui ne comprenez pas les attentes démentielles du prospect
2. Devis (*proposition commerciale*), proposition rédigée à l’arrache en 5 minutes et qui est soit beaucoup trop chère pour le projet, soit totalement sous-estimée. La probablité que le prospect accepte le devis n’a rien à voir avec la surévaluation ou sous-évaluation …
3. Devis signé, un devis sur 10 finit signé. Si vous avez de la chance…
4. Facture d’acompte, elle sera souvent payée 3 mois après la réalisation du projet
5. Bon de production, personne ne fait ça sérieux
6. Bon de livraison, personne ne fait ça et pourtant c’est bien utile
7. Facture, ordre de paiement. Dans l’informatique on demande le paiement à reception en général. Concrètement vous serez payé à 9 mois
8. Facture acquittée, on émet ce genre de document une fois par an, quand un client a un comptable maniacodépressif

Notons également l’évolution pokémon de votre interlocuteur

1. Prospect (on ne lui a rien vendu)
2. Client (on produit pour lui, il a un contrat en cours, on a livré pour lui il y a moins d’un an)
3. Partenaire (on travaille avec lui régulièrement, il nous paie régulièrement)

#### Le devis

Document d’estimation d’un prix

* C’est un document contractuel: il vous engage
* Il décrit les prestations et leurs prix
* Il décrit les conditions de réalisation
  * conditions générales de vente
  * délais de réalisation (en fonction de votre carnet de commandes)
  * conditions de paiement
* Il a un délais de signature au-delà duquel il n’est plus valable

#### Devis signé

Acceptation par le client du devis

* Document contractuel: il engage le client
* Devis imprimé et signé ou tout document d’acceptation (mail, SMS etc.) **Favorisez les traces écrites**
* Déclenche la réalisation des travaux dans les conditions prévues (délais, acompte)

#### Le bon de commande

Validation de la réception du devis signé, il confirme les conditions de réalisations et leurs éventuels ajustements.

#### Le bon de production

Indique au client que les travaux commencent. N’existe quasiment pas en informatique, c’est antinomique avec une méthodo agile

#### Le bon de livraison

Document remis à la livraison (dans une méthodo agile, ça peut donc être toutes les semaines !)

Il est important que le client valide la réception de ce document; il reconnaît ainsi que vous avez bien réalisé la mission et vous pouvez donc facturer sans risque que la facture soit contestée.

#### La facture

Ordre de paiement

* Identifie le client et le prestataire
* Elle peut-être partielle dans le cas de travaux long (mission de deux ans par exemple)
* Détaille ce qui est à payer: quantité, prix unitaire etc. ex: 5H d’intégration graphique, réalisation de 2 thèmes enfant …
* Détaille les taxes (TVA)
* Fixe les conditions de règlement (à réception de la facture et par virement bancaire)
* Numérotée (mais vous pouvez commencer au numéro que vous souhaitez !)
* Doit être enregistrée dans la comptabilité du client (il peut cependant la contester, auquel cas il doit noter la contestation dans sa comptabilité)

#### La facture acquittée

Preuve de paiement transmise au client

* Remise au client **APRÈS** le paiement
* Document différent de la facture ! (même si souvent c’est juste la facture avec la mention « acquittée »)
* Il contient le numéro de la facture qui a été réglée
* C’est un document rare, il est le plus souvent demandée pour les factures réglées en liquides (pas de trace de paiement …)

### La forme sociale

SARL ? SAS ? Auto-entrepreneur ? Portage salarial ? 

Pour vendre des prestations, il faut un statut/forme sociale/etc. sinon vous faites du black. Il y a énormément de formes sociales, on va détailler ici celles qui sont les plus utilisées

#### Choix essentiel

C’est un choix très important, pourtant c’est l’œuf et la poule …

* la forme sociale va découler de votre organisation …
* … mais votre organisation découle de votre forme sociale

#### Auto-entrepreuneur

Simple à mettre en place, mais horrible sur le long terme

* on est seul, l’AE est une habilitation à vendre des prestations en tant que personne (vous êtes l’entreprise)
* « simple »
  * Pas de TVA
  * Comptabilité simplifiée
  * Peu de paperasse
  * Se crée en 3 minutes sur Internet (le monde entier nous l’envie !)
* « mais … »
  * maximum 32000€ de chiffre d’affaire (même pas de bénéfice: de chiffre d’affaire. On ne soustrait pas les dépenses)
  * protection sociale à chier

L’AE est pensé pour les personnes qui veulent exercer à côté de leur contrat en CDI

http://lautoentrepreneur.fr

Nota: le statut d’un freelance est 9 fois sur 10 un auto-entrepreneur

##### CAE

Coopérative d’activité et d’emploi, portage salarial

* on est « seul » : on signe un contrat de travail (CDD 2ans, 1H/mois) dans la CAE et on se démerde
* au fur et à mesure que vous vendez, on augmente le temps de travail et le salaire
* « protecteur »
  * Accompagnement juridique et fiscal
  * Plus sérieux (gestion de la TVA, on peut répondre à des appels d’offre etc.)
  * La protection sociale est normale
* « cher »
  * 10% de frais au début
  * vous êtes salariés: ~50% de ce que vous gagnez partira en cotisations sociales

À Lens, il y a CoopConnexion. On peut aussi évoquer Optéos qui fait beaucoup dans l’informatique

#### Association

Aisé à mettre en œuvre, limité pour la lucrativité

* Il faut être deux ou plus
* Avantages
  * Ça se fait facilement en ligne
  * C’est simple à mettre en place
  * Avec ou sans TVA, à la carte (sauf si vous avez un CA supérieur à 32000€)
  * Ça peut être transitoire (association de préfiguration)
* Inconvénients
  * Il faut toujours avoir 2 bénévoles pour vous encadrer si vous décidez de vous salarier
  * Impossibilité de bénéfice (les excédents sont intégrés dans la structure, même après transformation en SARL vous ne pourriez pas les retirer)

#### SARL

C’est le modèle standard, en grosse perte de vitesse

* Il faut être deux
* Les droits de vote sont liés aux parts de capital
* Mauvaise protection sociale des dirigeants
* Bénéfice possible (s’il y a de l’argent, vous pouvez le mettre dans votre poche)
* Comptabilité normale, TVA et tout le tralala
* Très lourd, connu mais lourd

#### SAS/SASU

Le nouveau modèle, bien plus adapté au XXIème siècle

* On peut être seul (SASU)
* Droit de vote libre (on peut faire une personne = une voix)
* Les dirigeants peuvent choisir une bonne protection sociale (coûteuse) ou une mauvaise protection sociale (pas chère mais rapport qualité/prix à chier)
* Bénéfice possible (s’il y a de l’argent, vous pouvez le mettre dans votre poche)
* Comptabilité normale, TVA et tout le tralala
* Très souple et moderne

### Comptabilité / Social

Il y a des obligations de tenir ses comptes quand on vend des choses. Pour éviter la fraude aux taxes et aux cotisations, évidemment.

#### Comptabilité simplifiée

* En auto-entrepreneur
  * déclaration mensuelle de chiffre d’affaire
  * frais dans la déclaration de revenus
  * comptabilité recette/dépense
* En association non-assujestie à la TVA
  * comptabilité recette/dépense à faire voter en AG
* Dans les deux cas :
  * « TVA non applicable - article 293 B du CGI »

#### Cas classiques

Ça coûte cher (~2000€/an) mais prenez un comptable

* Déclaration de TVA (réel, simplifié etc.)
* Contribution territoriale, impôt sur les sociétés, impôt sur les bénéfices …
* Commissaires (facultatifs, mais utiles)
   * aux comptes (~5000€/an) pour certifier les comptes
   * aux apports (~5000€) pour faire évaluer des apports en nature

#### Social

Là aussi, appuyez vous sur un comptable ou sur le TESE

* Déclaration obligatoire **avant** toute embauche : DPAE
* Convention collective
  * négociation nationale entre patronat et salariat, par branche
  * grilles des salaires minimums et dispositions diverses (arrêts maladies, congés etc.)
  * SYNTEC: informatique et conseil
* Caisses complémentaires et médecine du travail
  * Malakoff-Médéric pour Syntec, PST62 ou PST59
  * Inscription obligatoire, même si vous n’avez pas de salarié

### Juridique

Fini de jouer, bienvenu chez les grands

#### DON'T PANIC!!!

La loi dit ce que vous risquez, vous devez en tenir compte.

Après vous avisez.

Rappel utile: la loi, dans le droit des affaires, ne s’applique qu’en cas de conflit

#### Quelques notions utiles

* Pas de délais de rétractation proffessionnel
  * signé c’est signé
  * la promesse d’embauche vous engage vous et seulement vous
* Métiers dont la signature vous permet de vous retourner contre eux:
  * commissaires
  * notaires
  * huissiers
  * avocats
  * éditeur de logiciel de caisse
  * et des tas d’autres moins utiles dans les affaires (électricien, chirurgien etc.)

### Et en vrac …

#### Pour se faire aider ?

* PoleEmploi, premier financeur des créateurs d’entreprises (ACRE, ARCE, NACRE…)
* BGE, boutique de gestion espace
* Squatter au Louvre-Lens Vallée, Réseau entreprendre, finance solidaire …

#### ESS

Économie sociale et solidaire

sociale: la forme (démocratie d’entreprise, partage des bénéfices etc.), solidaire: le fond (faire des éoliennes, aider les SDF …)

Cassons les naïvetés: on peut faire social sans faire du solidaire et réciproquement ! On peut faire du social et solidaire et être une boîte de merde

Ceci dit, c’est en vogue, c’est vraiment intéressant. Contactez l’APÉS Hauts de France (acteurs pour une économie solidaire) ou la finance solidaire (PdC Actif, les Cigales)

NE CRÉEZ JAMAIS DE COOPÉRATIVE (je vous aurai prévenus)

#### CMA ou CCI

L’enregistrement d’une entreprise se fait auprès de la chambre des métiers (dépannage, maintenance) ou de la chambre de commerce (développement, création de sites web)

Document de création: M0 (sorti tout droit des années 50…), ou le P0 pour une SASU. C’est assez violent comme (« simple formalité administrative »)[https://www.youtube.com/watch?v=o2su7cbl-pY&feature=youtu.be&t=41s]

#### Constituer le capital

1. On se met d’accord
2. On pose les statuts (M0)
3. On va à la banque, on crée un compte temporaire (personne ne peut y toucher)
4. On pose l’argent sur le compte
5. On termine les formalités liées au M0
6. Le compte temporaire est transformé en compte normal, ouf !

Comptez 4 à 6 semaines, en attendant, vous devez avancer les frais de votre poche

#### Quelques chiffres-clefs

* 20 à 30% de marge en revente (matériel, prestation d’un sous-traitant)
* 50% d’imposition sur le HT
* Travail: 1/3 administratif, 1/3 commercial, 1/3 réalisation
* Seuils importants:
  * 3 ans: fin des fonds propre, début de l’imposition réelle
  * 7 ans: capacité à se renouveler (capital, humains)

