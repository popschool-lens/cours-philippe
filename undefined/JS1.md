# Cours de JavaScript Basique

Philippe Pary, 2018

## Objectifs de la journée

➡ savoir lier un fichier Javascript correctement  
➡ savoir utiliser l’objet console  
➡ savoir repérer et utiliser les variables, les fonctions et les structures de contrôle  
➡ savoir récupérer une balise HTML en tant que variable et savoir modifier cette balise HTML  
➡ comprendre les objets

C’est un objectif ambitieux pour la semaine !

Nous avançons par exercices. Je vous donne quelques consignes, il vous appartient de rechercher un peu sur Internet (notamment la MDN !) pour réaliser l’exercice.

Nous terminerons la semaine avec un savoir difficilement applicable (ou alors avec une grande imagination), c’est normal

## Premier exercice: usage de l’objet console

L’intitulé est simple: créer une page web qui va afficher dans la console de développement _Hello world_

Pour savoir si ça marche, vous devez donc avoir l’inspecteur ouvert !

Essayez, la solution est postée ici: http://jsfiddle.net/qumk7p1c/

➡ on voit ici le lien entre un script et une page  
➡ l’attribut `defer` indique d’éxecuter le JavaScript quand tout l’HTML est chargé  
➡ on utilise `console.log` pour écrire dans la console de l’inspecteur. Il existe d’autres fonctions à `console` tel `console.warn` ou `console.error`. Voir https: developer.mozilla.org/fr/docs/Web/API/console

## Second exercice: intervention sur une balise HTML

Afficher _Hello world_ sur la page web en l’écrivant à l’intérieur de la balise `body`

Essayez, la solution est postée ici: http://jsfiddle.net/6dxn30q9/

➡ on voit ici que la balise `body` est accessible via une variable dont le nom est `document.body`  
➡ on peut modifier le contenu de `document.body` en changeant la valeur de sa variable `document.body.innerHTML`

## Troisième exercice: créer de l’HTML dynamique

Modifiez votre code, affichez _Hello world_ dans une balise de titre (`h1` donc)

➡ Vous devez passer par la création d’une balise (`var helloWorldH1 = document.createElement("h1");`)
➡ Puis vous ajoutez votre texte (`helloWorldH1.innerHTML = "hello world");`)
➡ Puis vous ajoutez votre balise au document (`document.body.appendChild(helloWorldH1);`)

Essayez, la solution est postée ici : http://jsfiddle.net/1azb6uLx/

➡ `innerHTML` porte bien son nom: on modifie le contenu HTML de la balise. On peut y écrire de l’HTML mais c’est assez bourrin et pas trop recommandé.

## Quatrième exercice: créer de l’HTML dynamique dans une balise HTML, plus avancé

Modifiez votre code et ajouter une balise `div id="greetings"`

À l’aide de la fonction `document.querySelector`, vous affichez _Hello world_ à l’intérieur de cette nouvelle balise

Essayez, la solution est postée ici : http://jsfiddle.net/w72nLr6k/

➡ on peut manipuler des balises HTML en JavaScript  
➡ par delà le contenu, on peut modifier la classe CSS, gérer les balises HTML enfantes et parentes …

## Cinquième exercice: tableau et usage d’une boucle

Nous allons créer un tableau avec des noms:

    var students = ["Alexandre.C","Alexandre.B","Benoît","Donatien","Hugo","Myriam","Youcef","Nicolas","Vlad","Quentin rmc","Quentin Kiou","Steven","Loïc","Julian","Maxence","Thomas","Amandine", "Tristan"];

Afficher un à un tous les noms à l’aide d’une boucle dans la console

Essayez, ça sera plus compliqué cette fois-ci. La solution est postée ici : http://jsfiddle.net/na0f4sqc/1/

➡ un tableau est un type spécial de variable qui contient plusieurs variables  
➡ on peut utiliser une boucle `for` classique, comme en C, pour parcourir un tableau. C’est plus simple à rédiger  
➡ on peut utiliser la fonction `forEach` propre à chaque tableau pour le parcourir. C’est un peu plus complexe à rédiger, **mais c’est la bonne manière**

## Sixième exercice: ajouter à une balise

Créez une balise `ul id="studentsList"` et ajoutez y les étudiants sous forme `[li]`


Essayez, la solution est ici: http://jsfiddle.net/doaweqp6/1/

## Septième exercice: évènements et CSS

Cet exercice va être plus guidé… 

1. créez dans votre fichier JS une fonction appelée `loadStyle` qui a un paramètre appelé `event`
2. la fonction `loadStyle` contiendra le code suivant `event.target.className = "dynamicStyle";`
3. dans votre HTML, mettez une balise `main`
4. créez et ajoutez un fichier CSS qui contiendra la définition de `.dynamicStyle` avec comme propriété `background-color: red;` et de `main` avec une hauteur, une largeur et une bordure
5. ajoutez à votre fichier JS ce code: `document.querySelector("main").addEventListener("click", loadStyle)`

Essayez, la solution est ici: http://jsfiddle.net/ej8724fx/3/

➡ on peut ajouter des _listeners_ d’évènement: click, survol de la souris, valeur changée …  
➡ les _listeners_ passent un paramètre à la fonction, ce sont les données de l’événement. Dans cette variable, l’élément concerné est présent dans la variable `target`  
➡ on peut modifier le CSS d’un élément


## Conclusion

* On peut afficher des choses en console de développement, pour y voir clair
* On peut _attraper_ une balise et interagir avec elle
* On peut _écouter_ des événements sur des balises
* On vient de découvrir les objets. Ce sont des variables qui contiennent d’autres variables ET des fonctions
* JavaScript est un langage orienté objet, quasiment tout est objet
* On a vu les fonctions de _callback_ et les fonctions _anonymes_ via `forEach`

## Exercices

Voici quelques exercices pour pratiquer

Ceux d’entre vous qui en ont besoin, passez du temps sur le CSS pour rendre vos logiciels beaux. Dans tous les cas, à défaut d’être belles, j’attends les applications qu’elles soient fonctionnelles.

### Addition

Affichez le résultat de l’addition de deux nombres entrés par l’utilisateur dans des champs de type `input`

Indices en vrac:
* on récupère la valeur d’un champ `input` avec l’attribut `value` (exemple: `document.querySelector("#myInput").value`)

### Cumul, moyenne

On a un champ d’input. À chaque fois que l’utilisateur y entre une valeur (validée en appuyant sur un bouton par exemple), vous affichez le cumul de toutes les valeurs entrées depuis le début ainsi que la moyenne des valeurs entrées.

Indices en vrac:
* Créez un tableau pour stocker les valeurs (`var inputVals = [];`)
* Pour ajouter une valeur à un tableau : `inputVals.push(parseInt(document.querySelector("#myInput").value));` par exemple

### Damier

À l’aide d’un clic sur un bouton, générez visuellement un damier de 9x9

Le damier sera une succession de `div` de couleur sombre puis de couleur claire.

Si vous voulez aller plus loin, offrez la possibilité de générer un damier 11x11 voir carrément un damier de dimensions personnalisées

Indices en vrac:
* pensez à utiliser les pseudo-classes `:nth-child(even)` et `:nth-child(odd)` pour appliquer les couleurs sans vous faire suer

### Tri alphabétique

Affichez le nombre total d’élèves et les prénoms des élèves par ordre alphabétique décroissant (de Z à A) donc à partir du tableau qui avait été donné dans le cours

Indices en vrac:
* pfff, j’ai la flemme de vous trouver un indice

### Plus petit, plus grand

Faire un jeu où l’utilisateur doit devenir un nombre tiré au hasard entre 1 et 100

Indices en vrac:
* on récupère la valeur d’un champ `input` avec l’attribut `value` (exemple: `document.querySelector("#myInput").value`)
* pour générer un nombre entre 1 et 100: `Math.floor(Math.random() * 100) + 1`

### Horloge

Affichez l’heure dynamiquement sur une page. L’heure doit se mettre à jour à chaque seconde.

L’exercice n’est pas trop compliqué, il nécessite de savoir parcourir la documentation, il permet de découvrir les timers.

### Gestion d’élèves

À partir du tableau d’élèves donnés dans le cadre du cours, créez une petite application pour modifier, ajouter et, si vous êtes chauds, supprimer des élèves

Indices en vrac:
* au lieu d’afficher les élèves via `ul`/`[li]` faites le via des champs d’input
* n’oubliez pas de donner un `id` à votre champ d’input
* au choix : soit vous passez par un bouton pour valider les modifications, soit vous utilisez l’event `change` (https: developer.mozilla.org/en-US/docs/Web/Events/change)
* créez un champ d’input qui sera tout le temps vide et dédié à l’ajout d’élève
* pour la suppression, vous mettrez une croix à cliquer avec une demande de confirmation via `window.alert`
* `studentsList.splice(i, 1)` voir https: developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice (cet indice n’est vraiment pas sympa)

### Jeu de pendu

Réservé à ceux qui auraient terminé et fait valider par un formateur les exercices précédents, créez un jeu de pendu
