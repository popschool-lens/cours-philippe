# Cours de Webpack

Loïc Pennequin & Philippe Pary, 2018-2022


## Qu’est-ce que Webpack ?

![logo webpack](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Webpack.png/214px-Webpack.png)

https://webpack.js.org/

Webpack est _bundler_ JavaScript.

Outre des fonctionnalités de programmations indisponibles en JavaScript (c’est même son but premier en fait…), il va surtout nous servir de grand chef d’orchestre pour combiner des tas de fonctionnalités de webpack ou d’outils externes :

* babel: support des vieux navigateurs
* minification: réduire la taille des fichiers
* sass: préprocesseur CSS que certains d’entre-vous pourraient kiffer
* environnement de développeement: prise en compte en live de vos modifications

Et ça c’est pour ce qu’on fera dans le cadre de notre cours. Les possibilités sont presque infinies

Webpack s’appuie sur NodeJS, un environnement pour exécuter du JavaScript en dehors d’un navigateur (dans un terminal par exemple)

## Installer node

On va commencer par installer Node sur nos machines :

https://nodejs.org/en/download/

## Installer le boilerplate PopSchool

Un _boilerplate_ c’est un fait-tout (une casserole quoi) qu’on va sortir dès qu’on se lance dans un projet d’importance.

Je vous ai trouvé un boiler plate un peu ancien : https://github.com/taniarascia/webpack-boilerplate
N’hésitez pas à en chercher un, github grouille de boilerplates webpack pour javascript

Je vous invite à forker ce projet et à personnaliser ce boilerplate en fonction de vos goûts et de vos découvertes au travers du web :)

Un fois le projet cloné, vous avez une commande à lancer:

    $ npm install

Elle va installer tous les modules JavaScript (dont Webpack). On ne les a pas inclus dans le dépôt github car c’est plus léger sans ça,  et ça vous fait avoir les dernières mises à jour. Certains dév JS avancées peuvent aussi avoir certaines dépendances installées au niveau du système !

## Réfléchir

Allez jeter un œil aux fichiers. On repère:

* le dossier `config` avec les fichiers de configuration de webpack qui mette en œuvre les différents modules et filtres (SaSS, PostCSS, Babel, minification …)
* le dossier `dist` va contenir le site web à publier après avoir fait un `npm run build`
* le dossier `src` va contenir votre code
* le dossier `node_modules` qui contient les dépendances (webpack, babel et leurs propres dépendances). Ce dossier est rempli automatiquement par `npm` quand vous faites un `npm install` ou `npm require`
* le fichier `package.json`, qui contient la liste des dépendances du projet
* un fichier `.babelrc.json` qui contient les modules de babel. Babel permet de faire fonctionner votre logiciel sur les vieux navigateurs comme IE8, par exemple

## Agir

Modifiez un peu les fichiers HTML, CSS et JS

Pour connaître les commandes disponibles, ouvrez `package.json`, c’est là qu’elles sont listées
Vous pouvez consulter les fichiers de configuration de webpack. Ça peut paraître intimidant mais si vous vous concentrez et que vous regardez la doc de webpack, ça peut avoir un peu de sens. Ça en aura de plus en plus au fur et à mesure que vous utilisez webpack.

Lancez le serveur de développement:

    $ npm start

Modifiez des fichiers JS/CSS, voyez les changements live. Si vous modifiez l’HTML, rechargez la page (il y a des astuces pour ne pas avoir à le faire, je vous laisse chercher :))

Regardez les erreurs :

    $ npm run lint

C’est un module qui a été installé et qui vous analyse le code et vous renvoie des alertes et des erreurs. Utile

Corrigez les indentations & cie

    $ npm run prettify

Idem, c’est un module installé fort utile. Sa configuration est dans le fichier `.prettierrc.json`

Lancez la build pour la production:

    $ npm run build

Pas de navigateur qui se lance. Par contre le contenu de `dist` contient votre site web prêt à être publié !
