# Cours de bash, partie 4 : installer des logiciels

Philippe Pary, janvier 2017  
PopSchool Lens. CC-BY-SA

## Objectifs

Savoir gérer les logiciels du système d’exploitation:

* Comprendre le fonctionnement de Debian
* Trouver un logiciel
* Installer un logiciel
* Supprimer un logiciel
* Mettre à jour le système

## Fonctionnement des logiciels de Debian

### Versions de Debian

![Illustration simple](infographic_debian_history-fr-v08.png)

Debian récupère les logiciels et les publie en 3 étapes:

1. Unstable (aka *sid*): première publication. Pour tester, Still In Developpement (*Sid* est aussi le nom du méchant garçon dans Toy Story 1)
2. Testing: les logiciels sont sans bug majeur connu. On élargit la base d’utilisateurs pour vérifier
3. Stabe: les logiciels sont sans bug connu, ou presque. On peut l’utiliser pour un serveur

En mémoire de Pixar, qui a beaucoup aidé Debian à ses débuts, les versions de Debian portent des noms de personnages de Toy Story.

Debian vient de la contraction de feu *Ian* Murdock et *Deb*orah Flynn

### DPKG, APT

DPKG: Debian Package  
APT: Advanced Packaging Tool
Paquet: un logiciel quoi, on l’a déjà dit…

Debian utilise un système centralisé pour l’installation des logiciel **ALORS ARRÊTEZ DE CHOPPER DES MERDES SUR INTERNET!!!**  
(Mais si jamais vous cherchez sur le net, ce sont des fichiers *.deb* dont vous avez besoin)

Ça fonctionne comme pour l’AppStore ou le PlayStore (en fait c’est eux qui s’inspirent de ce modèle …)

En ligne de commande vous avez deux logiciels à connaître: **apt** et **aptitude**

* apt: apt est un logiciel récent permettant la gestion des logiciels. Il remplace les historiques apt-cache et apt-get
* aptitude: aptitude est un logiciel plus lourd qu’apt. Mais il peut vous résoudre des problème épineux de versions si vous veniez à vous emmeler les pinceaux (en ayant installé des merdes depuis Internet, par exemple)

Les deux utilisent les mêmes arguments. C’est pratique.

**Les deux marchent très bien avec la *tab completion*: appuyez sur la touche tab pour compléter les arguments ou voir la liste des arguments possibles !**

### Sources logiciels

Vos sources logicielles sont définies dans le dossier */etc/apt/sources.list.d* ainsi que dans le fichier */etc/apt/sources.list*

    deb http://ftp.fr.debian.org/debian/ testing main contrib non-free
    deb-src http://ftp.fr.debian.org/debian/ testing main contrib non-free
    
    deb http://security.debian.org/ testing/updates main contrib non-free
    deb-src http://security.debian.org/ testing/updates main contrib non-free

* *deb*, *deb-src*: logiciel ou code source
* URL: adresse du dépôt
* *testing*: version utilisée (*unstable*, *testing*, *main* ou un nom de version comme *jessie*)
* *main contrib non-free*: types de logiciels utilisés

Les types de logiciels sont:

* main: logiciels libres garantis en qualité par la communauté debian
* contrib: logiciels libres avec des dépendances en *non-free* (sert assez peu)
* non-free: logiciels non-libres (drivers, flash …)

Pour ajouter un type de logiciel, modifiez votre fichier */etc/apt/sources.list* pour ajouter ce qui manque. C’est souvent la raison pour laquelle vous ne trouverez pas un firmware donné ; la plupart ne sont pas libres

## Trouver un logiciel

### On connaît le logiciel qu’on cherche

Par exemple: on veut savoir la liste des paquets apache:

    # apt search apache

On peut également s’appuyer sur le site <http://packages.debian.org/>

Pour chercher un fichier précis <http://packages.debian.org/file:libstdc++.so>

### On ne sait pas vraiment ce qu’on cherche

On fait des recherches sur Internet et après on contrôle via un *apt search* ou via *<https://packages.debian.org>* :)

## Installer un logiciel

C’est facile comme tout:

    # apt install apache2

Si par hasard vous êtes allez chercher de la merde sur Internet:

    # dpkg -i google-chrome-stable_current_amd64.deb

Souvent dans ce cas, il faut compléter par un:

    # apt -f install

Si tout merde:

    # aptitude remove google-chrome

## Connaître les logiciels installés

On sort le logiciel *dpkg*

    # dpkg -i

Éventuellement avec un filtre

    # dpkg -i | grep apache

Sinon *aptitude* vous indique par un *i* en début de ligne si le logiciel est installé

    # aptitude search apache

## Supprimer un logiciel

### Garder la configuration

Des fois qu’on puisse réinstaller le logiciel plus tard et qu’on ne veut pas perdre la configuration, ou les bases de données dans le cas d’un SGDB:

    # apt remove apache2

### Tout virer

Histoire d’être bien carrés, tout est viré. Pas de pitié pour les croissants:

    # apt purge apache2

## Mettre à jour son système

### Manuellement

Ça se fait en deux temps.

1. On récupère la liste des mises à jour:

     # apt update

À ce moment là, on peut mettre à jour un logiciel en particulier sans tout mettre à jour

2. On procède à la mise à jour

     # apt dist-upgrade

Vous verrez peut-être *apt upgrade*. Concrètement *dist-upgrade* est plus complet. À votre niveau, privilégiez-le.

### Automatiquement

Via un logiciel de mise à jour comme **apt-cron**

<https://debian-administration.org/article/162/A_short_introduction_to_cron-apt>

     # apt install apt-cron

## Exercices

Installez les logiciels suivants:
* apache2
* openssh-server
* composer
* nmap

Vérifez si *mysql* est installé sur votre système

Avec l’aide de nmap, vérifiez les ports de votre machine et les versions des logiciels qui tournent dessus. Puis trouvez la liste des ordinteurs accessibles par SSH sur le réseau de l’école.

(vu qu’on se partage une connexion à nous tous, ça devrait prendre au moins la journée ! :))

