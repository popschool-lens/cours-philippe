# Ionic framework: création d’application *natives*

©Philippe Pary 2017, pour PopSchool
CC-BY-SA (latest)

## Objectif

Créer une application Ionic connectée à l’API de Daishi <http://tp-lens.popschool.fr>

La publier sur PlayStore ou sur AppStore.

## Choses à faire

1. Créer le projet Ionic

    $ ionic start api-daishi blank

2. Coder l’application

L’API de Daishi est simple, on peut partir sur une application monopage (liste des utilisateurs, détails qui s’affichent quand on clique sur un utilisateur)

Ou alors partir sur deux pages : la liste des utilisateurs, le détail d’un utilisateur.

Selon le choix, il faut créer les vues et les routes liées.

Le contrôleur contient les fonctions, par exemple celles qui demandent confirmation avant de supprimer un utilsateur ou transformer une date de naissance en signe du zodiaque.

Le service contient les appels à l’API.

Pensez à utiliser `ionic resources` pour créer une icône et un splashscreen personnalisés. Éventuellement, modifiez un peu le contenu de `config.xml` pour vraiment entrer dans les détails et obtenir une application qui fasse pro (on y trouve par exemple le texte qui s’affichera dans la liste des applications sur Android).

3. Tester l’application

Dans un monde meilleur, on mettrait en place des tests unitaire. Vous apprendrez ça sur le tas. Mais sachez qu’on devrait vraiment le faire (on le fera peut-être en nocturne)

Dans un monde parfait, on commencerait par rédiger les tests avant même de coder l’application.

Dans notre cas, vous devez faire tourner l’application sur plusieurs devices (GSM, tablettes) pour vérifier que tout fonctionne (ça doit vous prendre du temps pour tester méticuleusement) et surtout que ça s’affiche bien (quand on teste sur son GSM ou sa tablette, on a souvent une application qui ne s’affiche bien que sur son GSM ou sa tablette…)

4. Préparer la signature puis signer son application

Voir <http://ionicframework.com/docs/guide/publishing.html>

Les applications sont signées. C’est une sécurité pour éviter que des personnes malintentionnées créent une application qui aura le même nom que le votre.

Vous devez générer une clef. **CONSERVEZ LA PRÉCIEUSEMENT !!!**  
Si vous la perdez, vous ne pourrez plus publier de mise à jour de votre application, vos utilisateurs devront désinstaller puis réinstaller. La teuhon …  
Si quelqu’un en obtient une copie (parce que votre session n’était pas verrouillée, par exemple…), il pourra créer de fausses versions de votre logiciels. Versions qui pourront contenir du code malicieux.

    $ keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000

Le fichier à conserver jalousement est `my-release-key.keystore` (d’ailleurs, vous pouvez changer ce nom dans la commande ou simplement renommer le fichier)

On doit ensuite générer un apk spécial et le signer

    $ cordova build --release android
    $ jarsigner --verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore api-daishi-release-unsigned.apk api_daishi
    $ zipalign --v 4 api-daishi-release-unsigned.apk api-daishi.apk

On a enfin un fichier prêt à être publié


5. Publier sur Google Play ou sur l’AppStore

toujours voir: <http://ionicframework.com/docs/guide/publishing.html> 

Préparez des screenshots, une description, des mots-clefs. Pensez à l’anglais, pensez à faire des screenshots pour GSM, pour tablette etc.

Par le grand clicodrôme, publiez. Faites installer votre application à votre maman qui sera fière de vous.

