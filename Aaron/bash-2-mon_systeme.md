# Cours de bash, partie 3 : découvrir mon système

Philippe Pary, décembre 2016  
PopSchool Lens. CC-BY-SA

## Objectifs

Découvrir votre système d’exploitation:
* organisation du système
* introduction au hardware
* gestion des processus
* gestion des droits utilisateurs (ACL)

Ce cours sera complété d’un second cours en janvier dédié à l’administration système

Lexique:
* software: logiciel
* hardware: matériel
* middleware: pilotes, drivers, *intergiciel*
* OS: operating system, système d’exploitation. Permet aux logiciels de se lancer sans avoir à gérer eux-même le matériel
* ACL: access control list, droits d’accès
* log: journal. Fichier dans lequel sont enregistrés des évènements (accès à une page web, envoi d’un mail, diagnostic de démarrage de l’ordinateur …)
* charset: encodage. Manière dont sont enregistrées les caractères (quelle chaîne de 0 et de 1 de quelle longueur pour enregitrer *é* ?
* bit (b), digit: un 0 ou un 1
* byte (B), octet: 8 digits
* Ko=1024o; Mo=1024Ko, 1Go=1024Mo, 1To=1024Go etc.
* 1GB=1Go. 1Gb=128Mo. Vos connexions internet sont annoncées en bits (digits), évidement.
* binaire: synonyme de logiciel. Un fichier binaire n’est cependant pas forcément un logiciel, ça peut être une image, du son, une vidéo …
* processus: logiciels en cours d’éxecution
* root: superutilisateur du système. Son ID est 1. Il a tous les droits. *il y a deux types d’administrateurs systèmes : ceux qui ont déjà fait une grosse connerie en root et ceux qui vont en faire une*

## Mon système

Déjà ce qui est bien, c’est de savoir où on est. Ce n’est pas toujours évident (serveur d’un client accédé en SSH par exemple)

    $ lsb_release -a
    $ uname -a
    $ uname -r
## Organisation du système

La philosophie UNIX des systèmes d’exploitation dit que tout est fichier. Même les disques durs, même les cartes réseaux, même les processus.

L’arborescence comporte à la racine plusieurs dossiers dont certains sont remarquables. D’autre moins. Ce qui importe est en gras.

* /bin : binaires usuels du système (on trouve les autres dans /usr/bin)
* /boot : fichiers liés au chargement du système d’exploitation (boot phase = phase de démarrage)
* /dev : fichiers du matériel (on s’y attarde dans la section sur le hardware)
* **/etc** : fichiers de configuration du système. La plupart des fichiers ont une déclinaison par utilisateur dans son HOME
	* /etc/init.d : fichiers des services disponibles (exemple: serveur web, serveur de base de données)
	* /etc/crontab : tâches planifiées au niveau du système
* **/home** : dossier où sont rangés les répertoires utilisateurs
* initrd.img : l’amorçage du kernel
* /lib : librairies usuelles. Les librairies sont du code partagés entre les logiciels (pour éviter qu’un simple ls prenne plusieurs dizaines de Mo de mémoire vive)
* /lost+found : en cas de crash, les fichiers orphelins sont déplacés ici
* /media : emplacement d’accès aux clefs USB, GSM & cie
* /mnt : emplacement prévu pour monter à la main un matériel de stockage (disque dur, clef USB, partage réseau etc.). Utilisé dans le dépanage ou le développement.
* /opt : tout ce qui est optionel. Vous pourrez y ranger votre propre bordel, tout ce qui n’est pas important pour le système, vous n’en aurez probablement jamais l’utilité
* /proc : répertoire des processus en cours d’éxecution
* /root : HOME du super-utilisateur
* /sbin : binaires réservé au superutilisateur
* /sys : dossier du système d’exploitation
* /tmp : fichier où sont enregistrés les fichiers temporaires (document en cours d’édition, fichier non-téléchargé et juste affiché …). Si un logiciel crashe, regardez par là pour récupérer vos données
* /usr : fichiers propres aux utilisateurs
	* /usr/share/doc : documentation de vos logiciels (exemples de configuration, documentation d’usage etc.)
* **/var** : fichiers variables du système
	* **/var/lib** : bases de données notamment
	* **/var/log** : journaux système (syslog, mail.log, auth.log)
	* **/var/www/html** : répertoire par défaut du serveur web
* vmlinuz : votre système d’exploitation

Ouch hein ?  
Il n’en est que 3 importants pour vous : /etc, /home /var. Le reste vous vous en foutez un peu.

## Hardware

Nous sommes des développeurs web, donc des développeurs logiciels. Dans un monde idéal on se foutrait du matériel.  
Dans un monde idéal, les processeurs ont une vitesse infinie, la RAM est illimité et instantannée, les disques durs également. Évidement ce n’est pas le cas

**Il y a ordinateur du moment où on a un processeur et de la mémoire vive**

(vos GSM sont des ordinateurs)

### Processeur

Le processeur fait des opérations (calcul, enregistrement de données dans des registres électroniques etc.)

On retrouve le terme CPU (central processing unit). Il existe des processeurs dédiés pour les cartes graphiques, GPU (graphical processing unit) mais on s’en fout un peu dans notre cas : le web ne sollicite pas trop les GPU.

Il a deux caractéristiques importantes : sa vitesse de calcul (exprimée en Hz, hertz) et la taille de ses caches (exprimée en octets)

Le nombre de cœurs est également à prendre en compte : un processeur mono-cœur très puissant peut être moins intéressant qu’un processeur octo-core aux caractéristiques moins évoluées. Cependant le marketing se charge très bien de vous enfumer sur ces chiffres bruts, lisez la presse hardware pour y voir clair.

On trouve les informations sur ses processeurs via la commande :

    $ cat /proc/cpuinfo

Il y a un bloc d’informations par processeur. La ligne qui vous permet de comparer la taille de vos engins est *bogomips* (bogus micro-instructions per second). C’est l’estimation, totalement à l’arrache, de la capacité du processeur.

Le cache est une mémoire vive de petite taille (2Mo/4Mo) pour accélérer son fonctionnement. L’accès au cache est des milliers de fois plus rapide que l’accès à la mémoire vive.

### Mémoire vive

La mémoire vive stocke les informations sur les processus en cours (code, données etc.). Elle est effacée à chaque redémarrage (d’où son nom). Sans mémoire vive, pas de processeur utile.

On trouve des informations sur la mémoire vive via ces commandes :

    $ cat /proc/meminfo
    $ free

La mémoire vive a plusieurs états :
* used : total de ce qui est utilisé
* free : total de ce qui est disponible
* shared : sous-section de used. Zone partagée entre plusieurs processus
* buff/cached : sous-section de used. mémoire pré-réservée par le système pour les processus en cours
* available: sous-section de buff/caches. mémoire qui peut être utilisée malgré sa pré-réservation

Le SWAP. Quand la mémoire vive est saturée, on utilise le disque dur comme relais. C’est le début de la fin : le disque dur est de l’ordre de mille fois plus lent que la mémoire vive. En français quand on dit *ça rame* c’est souvent que le SWAP est utilisé.

### Disque dur

Le disque dur est un périphérique de stockage de masse (par opposition à la cache ou à la mémoire vive). Il est non-volatile : ses données restent enregistrées même s’il n’est plus alimenté électriquement.

Son nom vient en opposition aux disques souples (floppy drives), disquettes réellement souples utilisées au début des années 80.

Il est lent comparé au cache et à la RAM. Même un SSD est lent comparé à ça. Mais il peut contenir énormément de données.

Un disque dur est divisé en partitions. Les partitions sont des divisons logiques d’un disque dur (penser à C: et D: sur un même disque dur sous Windows). Elles existent essentiellement pour des raisons historiques, aujourd’hui on a comme utilité des partitions :
* faire coexister plusieurs systèmes d’exploitation sur un même ordinateur (dual boot)
* conserver les données d’une réinstallation sur l’autre (/home séparé par exemple)
* séparer une zone du disque dur plus suceptible d’erreurs que d’autres (/var séparé pour limiter les erreurs liées aux écritures fréquentes)

Les disques durs sont des fichiers /dev/sd[a-z] et les partitions d’autres fichiers /dev/sd[a-z][0-9]

    $ ls -l /dev/sd*

Quelques commandes pour avoir des informations:

    $ df -h
    $ du -sh <dossier>
    $ cat /proc/diskstats

### Cartes réseaux

Pas grand chose à dire. Bluetooh, wifi, ethernet, USB (c’est paticulier, évidement)

Quelques commandes:

    $ netstat -tanpu
    $ ping google.com
    $ telnet google.com 80 (puis GET /)

Sur netstat, il y aura des précisions dans le cours de réseau.

### Carte graphique

Pour votre culture essentiellement:

* xserver-xorg : gestionnaire d’affichage graphique
* display-manager (dm) : gère les droits d’accès depuis l’interface graphique
* environnement graphique : gnome, kde, unity, xfce etc.

### Human Input Device

Les HID sont tous les périphériques avec lesquels les humains peuvent interagir avec le système :

* Clavier, keyboard
* Souris, mice
* Écran tactile, touchscreen
* Lecteur code-barres, barcode reader
* Lecteur de carte magnétique, magcard reader
etc.

Quand on lance cette commande et qu’on bouge sa souris, ça bouge. C’est magique. Mais faut être root …
    # cat /dev/input/mice | od -t x1 -w3

## Processus

Un processus peut se résumer à un logiciel qui tourne sur votre système. La réalité est plus complexe hein (threads, routines etc.)

Chaque processus a ses fichiers dans /proc (philosophie Unix). Chaque processus est identifié par un **PID**, son matricule.

Voir tous les processus du système:

    $ ps aux

N’afficher que gedit:

    $ ps aux | grep gedit

Utilitaire pour avoir une idée de l’état de votre système:

    $ top

(vous quittez top avec la touche *q*)

Commande pour mettre fin à un logiciel un peu pénible:

    $ killall -9 firefox-esr

(mettra fin à Firefox)

Uptime est le temps depuis lequel la machine est allumée. C’est assez anecdotique comme information

    $ uptime

La charge est le nombre de logiciel en attente d’éxecution au niveau du processeur. C’est la taille de la file d’attente à DisneyLand en somme … Elle s’exprime en 3 chiffres : sur la dernière minutes, sur les 5 dernières minutes, dans le dernier quart d’heure. Ça permet de savoir la tendance.  
On retrouve ces données dans top et dans uptime

    $ uptime
    $ top

## Gestion des droits

Il y a des droits sur votre système pour chaque fichier et dossier.

Ces droits sont répartis entre l’utilisateur (u) qui possède les fichiers, le groupe (g) et les autres (o pour others)

Vous aurez souvent des problèmes de permissions. C’est ici que ça se joue

L’utilisateur peut modifier les droits d’un fichier et modifier le groupe d’un fichier ou dossier. Seul le superutilisateur peut modifier l’utilisateur.

Les droits sont de 3 types (plus des subtilités dont on se fout à notre niveau) :
* r : read, droit de lire le fichier, lister les fichiers du dossier
* w : write, droit d’écrire dans le fichier, dans le dossier
* x : eXecute, droit de lancer le fichier comme un logiciel, droit de lire le contenu des fichiers d’un dossier

Ces droits sont notés en une suite de 3 digits:
* 000 : aucun droit
* 111 : tous les droits

3 digits peuvent se noter de 0 à 7
* 0: aucun droit
* 7 : tous les droits

On note les droits d’un dossier ou fichier en mettant 3 chiffres compris entre 0 et 7 l’un à la suite de l’autre :
* 000 : personne n’a aucun droit
* 777 : tout le monde a tous les droits
* 700 : seul l’utilisateur a tous les droits, les autres n’ont rien
* 750 : tous les droits à l’utilisateur, lecture et éxecution pour le groupe, rien pour les autres

Commmandes:

    ls -l
    chmod 755 <monfichier>
    chmod -R 755 <mondossier>
    chgrp www-data <monfichier>
    chgrp -R www-data <monfichier>

## Exercices

### Connaître son environnement
1. Quel est le nom de votre système d’exploitation ?
2. Quelle est la version de votre noyau ?
3. De quelle quantité de mémoire vive disposez vous ? Votre ordinateur utilise-t-il du swap ?
4. Combien d’espace vous reste-t-il sur votre disque dur ?
5. Le port 80 de votre machine est-il ouvert ?

### Gérer les processus
1. Quelle est la charge de votre machine sur les 5 dernières minutes ?
2. Quel est le processus qui vous prend le plus de mémoire vive ?
3. Coupez firefox depuis la ligne de commande
4. Quel est le PID de votre terminal ?

### Gérer les droits
1. Créez un dossier public_html dans votre répertoire utilisateur
2. Affichez ses droits
3. Modifiez le groupe de ce dossier pour www-data
4. Accordez tous les droits à www-data sur ce dossier
5. Vérifiez les modifications. Laissez ce dossier là, il sera utile par la suite
6. Allez dans le dossier /root
7. Affichez les droits du dossier (ls -al)
8. Tentez de créer un fichier
