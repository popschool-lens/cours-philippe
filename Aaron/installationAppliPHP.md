# Installation et personnalisation d’une application PHP

Philippe Pary pour PopSchool Lens http://pop.eu.com  
CC-BY-SA

## Objectifs du cours

* Réaliser qu’il existe de nombreuses solutions libres et gratuites pour de nombreux besoins, et donc ne pas ré-inventer la poudre
* Savoir chercher et trouver des logiciels webs
* Appréhender Wordpress, un des logiciels phares du web ([27% du web](https://w3techs.com/technologies/details/cm-wordpress/all/all))
* Monter en compétence sur la personnalisation des ces solutions

## Quelques généralités

Il existe tout un tas de logiciels libres pour les usages courants :
* CMS (Wordpress, Joomla …) : *content management system* site web de publication de contenu (blog, journal etc.)
* e-Commerce (Prestashop, Magento …) : site de vente en ligne, avec des modules de paiement (banque, paypal etc.)
* Gestion documentaire (MediaWiki, Dokuwiki)
* Partage de fichiers (Owncloud, NextCloud)
* logiciels d’entreprise comme des CRM (*customer relationship management* CiviCRM, vTiger) ou des ERP (*entreprise ressources planner* dolibarr)
* Gestion de base de données : PHPMyAdmin, vous connaissez ? :)
* etc.

Une part importante de ces logiciels utilise PHP et MySQL, mais on retrouve des applis dans tous les langages (Ruby, Python…), avec tous les framework (rails, Symfony, Django …) et tous les systèmes de gestion de base de données (PostgreSQL, MongoDB …)

L’installation est généralement triviale : quand vous voyez passer un nouveau nom, prenez 30 minutes pour installer et tester un peu le logiciel.

Sources pour trouver des logiciels:
* Framasoft: <https://framasoft.org/rubrique2.html>
* Wikipedia contient de nombreuses listes (<https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Logiciel_libre_sous_licence_AGPL>, <https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Syst%C3%A8me_de_gestion_de_contenu>…)
* Une veille efficace vous fera connaître les noms principaux en quelques mois !

## Découvrir des logiciels (30min)

* Je veux réaliser une galerie photo pour présenter mes créations artistiques
* Je veux partager des documents au sein de mon entreprise de BTP avec des droits d’accès
* Je souhaite créer un catalogue pour présenter mon offre de traiteur
* Je veux pouvoir émettre des factures en lignes
* Je veux pouvoir tenir ma comptabilité d’association

## Installez un logiciel (1H)

Installez Wordpress sur votre machine

*NB: la plupart des archives sont au format tarball (.tgz ou .tar.gz). Vous pouvez les décompresser normalement en interface graphique ou via *tar xzfv monFichier.tgz* en ligne de commande*

## Découvrir le logiciel (1H)

Découvrez le logiciel et ses options : ajoutez du contenu, activez des modules, regardez des options etc.

## Personnaliser le logiciel (2H)

Quasiment tous les logiciels proposés sont MVC et proposent des templates. Prenez un template et modifiez le (quitte à juste le rendre moche)

0. (pushover) Trouver le dossier des templates
1. (easy) Modifier le template comme un barbare (directement les fichiers du template)
2. (normal)Copier un template et créer le sien (clean)
3. (hard) Créer un fichier de template qui peut être installé par un camarade ou sur une nouvelle installation du logiciel
4. (godmode) Créer from scratch son fichier template ou personnaliser un template existant au point de pouvoir expliquer chaque fichier

## Débrief (30 minutes)
