# Autopsie des serveurs Apache et MySQL

Philippe Pary pour PopSchool Lens http://pop.eu.com  
CC-BY-SA

## Objectifs du cours

Donner des repères sur le fonctionnement d’un serveur web, en l’occurence Apache, et d’un système de gestion de base de données (SGDB), en l’occurence MySQL.

Ceci doit permettre à l’élève d’avoir au moins une vague notion de comment ce merdier finit par tomber en marche pour deux raisons:
* savoir installer un environnement de développement
* avoir des idées de ce qui peut bugguer côté système
* comprendre les systèmes de log

## Partie1: Apache

Apache: a patchy web server … on peut y préférer [nginx](https://www.nginx.com/)

Apache est un passe-plats: il prend des fichiers de base (HTML, PNG, CSS, JS …) et les transfère à l’utilisateur.

Selon sa configuration, Apache peut faire appel à des modules ou des logiciels externes. Par exemple, un fichier .PHP sera transmis au logiciel PHP.

Le logiciel PHP, éxecutera le code source présent dans le fichier pour générer un fichier HTML. Au besoin, il se connectera à la base de données (MySQL, typiquement)

Le fichier HTML généré sera enregistré de façon temporaire, transmis à Apache qui se chargera de le transmettre au visiteur.

On en déduit que:
* Apache ne fait pas grand chose, il prend des fichiers et les transfère
* Le code PHP n’est jamais vu par le visiteur
* Le code PHP est transformé par un logiciel autre que Apache (le logiciel PHP)
* Apache ne se connecte pas à la base de données, c’est PHP qui le fait

### Configuration d’Apache

Sur un système Debian …

#### Utilisateur web

Par défaut, Apache tourne avec l’utilisateur appelé **www-data** et le groupe **www-data**

Vous allez rencontrer vos premiers problèmes de droits. Ressortez le vieux cours de Bash :)

#### Configuration d’Apache et de PHP

La configuration d’Apache tient sur plusieurs fichiers et dossiers

* /etc/apache2/apache2.conf : configuration principale, qui s’applique à tous les sites web sauf mention contraire
* /etc/apache2/\*-available: sites, modules ou configurations disponibles, mais pas forcément activées
* /etc/apache2/\*-enabled: sites, modules ou configurations activées
* fichiers **.htaccess** présents parmi les fichiers d’un site web, ils contiennent de la configuration qui s’appliquera au dossier et aux sous-dossiers

Les commandes pour activer/désactiver les sites, modules et configurations sont :

    # a2ensite <monsite>
    # a2enmod <monmod>
    # a2enconf <maconf>
    # a2dissite <monsite>
    # a2dismod <monmod>
    # a2disconf <maconf>

La configuration de PHP tient sur un fichier: **/etc/php7.0/apache2/php.ini**  
Cette configuration peut être modifiée via des fichiers .htaccess (php\_flag ou php\_value)

Il n’y a rien de magique: toute la configuration se trouve dans ces fichiers. Pour être précis, en lisant à partir du fichier apache2.conf, on pourrait retrouver tout le reste !

Dossier web par défaut: **/var/www/html/**

On peut ajouter des dossiers webs pour chaque site web (via dossier sites-available)

On peut activer le module *userdir* pour que le dossier *public_html* de chaque utilisateur crée automatiquement un dossier web. **Attention**, par défaut PHP est désactivé des userdirs, il faut l’activer en modifiant le fichier /etc/apache2/mods-available/php7.0.conf

    http://localhost/~philippe/

##### Notion de virtualhost et de site web

### Les logs

Les logs contiennent énormément d’informations: qui s’est connecté, à quelle heure, quelle était la requête, quel a été le résultat …

Selon la configuration de PHP, les logs peuvent même contenir les messages d’erreur PHP, ce qui les rend incontournable pour débugguer.

Par défaut, les logs sont dans **/var/log/apache2/**

La configuration d’un site web peut changer l’adresse du log, consultez la configuration du site web pour y voir clair (dans /etc/apache2/sites-enabled/)

**Les logs doivent être un réflexe en cas de problème !!!**

Quelques commandes utiles pour suivre des logs:

    tail -f /var/log/apache2/error.log
    tail -f /var/log/apache2/access.log|grep categories.html
    multitail /var/log/apache2/error.log /var/log/apache2/access.log

### Exercices

1. Modifiez le fichier `/var/www/html/index.html`, affichez le via <http://localhost>
2. Trouvez votre adresse IP, faites afficher votre page par défaut à votre voisin (par exemple <http://192.168.1.45>)
3. Publiez votre CV en copiant le dossier dans `/var/www/html/` et affichez le (par exemple <http://192.168.1.45/cv/>), affichez le également depuis votre GSM
4. Lisez le log d’accès (`/var/log/apache2/access.log`) et regardez qui a visité votre serveur web
5. Supprimez le dossier de votre CV
6. Faites un git clone du projet de votre CV dans `/var/www/html`
7. Faites des modifications à votre CV depuis votre répertoire utilisateur, commitez et pusher
8. Faites un pull dans votre dossier `/var/www/html/cv/`
