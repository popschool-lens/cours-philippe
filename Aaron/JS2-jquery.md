# JavaScript: introduction à jQuery

Philippe Pary 2017, dans le cadre de PopSchool.  
CC-BY-SA

## Exercice 1

À l’aide d’un bouton, changez la couleur de fond de la page à l’aide de la fonction [click](http://api.jquery.com/click/) qui vient modifier la couleur de fond de la page via [css](http://api.jquery.com/css/)

## Exercice 2

Amélioration de l’exercice précédent :  
Créez un champ *&lt;input&gt;*, exploitez le résultat via un bouton, *click*, ou via la fonction *[change](http://api.jquery.com/change/)* du champ de formulaire. 

Affichez le résultat dans une *&lt;div&gt;* à part que vous sélectionnerez via son attribut *id* et le sélecteur *$()*/*jquery()* et la fonction *[text](http://api.jquery.com/text/)* de l’élément sélectionné.

Et maintenant, faites apparaître puis disparaître la div avec la fonction *[slide](http://api.jquery.com/slideToggle/)*

## Présentations

jQuery est *ancien*: 2006.

C’est une *bibliothèque*, pas un framework.

Il visait à simplifier la vie des développeurs webs à une époque où existait encore le légendaire et terrible Internet Explorer 6 et où les navigateurs ne se mettaient pas à jour automatiquement. Il assure que le comportement des fonctions, des sélecteurs etc. est homogène : en utilisant jQuery, tous les navigateurs se comportent de la même façon.

Il tombe aujourd’hui en désuètude car vanillaJS est bien adopté par tous les navigateurs. Les comportements sont homogènes. Et vanillaJS va 100 fois plus vite, ce qui est important pour les grosses opérations.

Ceci dit, jQuery garde l’avantage d’une documentation claire et d’une plus grande facilité de prise en main.

## Usage

### Sélecteur

jQuery c’est avant tout un sélecteur que vous croiserez trop souvent comme étant **$**.

Je vous invite à favoriser l’usage de **jQuery** au lieu de **$** : le symbole **$** est utilisé par d’autres bibliothèques et fonctions JS et vous pourriez avoir des conflits extrêmements épineux à résoudre.

Attention à la casse ! **jQuery** : *j* en minuscule, *Q* en majuscule. Du camelCase est une convention d’écriture en JS.

### jQuery(document).ready()

    jQuery(document).ready(function() {
        alert("Hello world !");
    });

*jQuery(document).ready()* est central pour jQuery. Ça garantit que le code ne s’exécutera que quand la page sera intégralement chargée: tout le code HTML est là, tout le code JS est là etc.

Ça évite les soucis que certains ont eu de tenter des interactions avec des élèments pas encore chargés …

Cool, non ?

## Exercice A

Refaites le jet de dés, mais en jQuery

## Exercice B

Refaites le jeu de pile ou face, mais en jQuery

## Exercice C

Refaites le jeu de pierre-feuille-ciseaux en jQuery

## Exercice D

Refaites le jeu de pendu en jQuery. Pour ceux qui trouvent ça trop facile: faites le jeu mais en prenant un mot au hasard sur Wikipedia avec la foncntion *[ajax](http://api.jquery.com/jQuery.ajax/)* et de l’*[API de wikipédia](https://fr.wikipedia.org/w/api.php)*

