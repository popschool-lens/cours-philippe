# Introduction aux apprentissages dispensés à PopSchool Lens

Par Philippe Pary le 6 novembre 2020
philippe@pop.eu.com

Pour le salon numériquelles 2002

Licence CC-BY-SA https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.txt

## À quoi forme-t-on à PopSchool ?

**Au développement web**

Deux grands aspects:

* le développement _front_ : produire de jolies interfaces pour les utilisateurs. Le front c’est toute la partie visible d’un site web
* le développement _back_ : créer la machinerie qui permet à un site web d’être dynamique (lister des produits, enregistrer des ventes, gérer les commentaires…)

### Statistiques de sortie à Pop School Lens

Les chiffres datent de fin 2019, mais restent d'acutalité

<table>
    <tr>
        <th></th>
        <th>Métier</th>
        <th>Nb élèves</th>
    </tr>
    <tr>
        <td>SSII / ESN</td>
        <td>Dev web en équipe (3-5 personnes) sur des projets plus complexes</td>
        <th>14</th>
    </tr>
    <tr>
        <td>Indépendant</td>
        <td>Dev web freelance</td>
        <th>11</th>
    </tr>
    <tr>
        <td>Web Agency</td>
        <td>Dev web, souvent seul ou en binome, en relation avec une équipe (graphiste, chef de projet etc.)</td>
        <th>7</th>
    </tr>
    <tr>
        <td>PME</td>
        <td>Informaticien *homme à tout faire*</td>
        <th>5</th>
    </tr>
    <tr>
        <td>Reprise d’études</td>
        <td>De plus en plus fréquent</td>
        <th>5</th>
    </tr>
    <tr>
        <td>Collectivité</td>
        <td>Secteur public</td>
        <th>1</th>
    </tr>
</table>

## À quoi passe-t-on son temps ?

Estimation purement subjective …

![Graphique très explicite: Coder 40%, Réunion 25%, Veille technologique 15%, Indeterminé 10%](images/ChartGo.png)

## Quelques stats sur le métier

* Très peu de femmes : 4 à 6% de femmes dans les équipes techniques …
    * C’est un phénomène issu des années 80
    * C’est une construction culturelle (les séries et les films présentent extrêmement peu de femmes informaticiennes !)
    * Mais ça progresse : il y a 40% de femmes dans la promotion actuelle de PopSchool Lens !
* Salaires élevés : le salaire moyen de sortie de Pop School Lens est de 1740€ bruts mensuels
* Métier *souple* : pas vraiment de dress code (on évite simplement de venir jogging…) sauf dans de rares cas. L’ambiance *start up* généralisée
* Carrières *grenouille* : taux de turn over autour de 30%, on progresse peu par promotion ; essentiellement en changeant de société

## Culture code …

Le développement dispose d’une culture forte qui se transmet dans les écoles, les entreprises et les nombreux évènements (conférences, associations, projets contributifs …)

Culture qui évolue avec la généralisation du métier, évidement

# Tout ce que vous avez toujours voulu savoir sur le code, mais n’osiez pas demander

Le code, c’est du texte. Du texte dit _plat_, sans accent, sans guillemets. Juste du texte.  
Le développement web, c’est du texte, juste du texte (`HTML`, `CSS`, `JS`), qui est éxecuté par un logiciel, le navigateur web.  
Des logiciels, des serveurs webs, préparent ce texte (partie back) et l’envoient au visiteur (partie front) au travers du protocole HTTP.  
Et pour savoir quel texte-code on doit rédiger, on doit aller lire du texte-documentation

![test d’attention](images/attention.png)

## navigateur web
`Firefox`, `Safari`, `Chrome` … c’est le logiciel avec lequel vous consultez une page web

## page web
`HTML` est le langage pour structurer les *pages web*.

`CSS` est le langage pour faire le design de ces *pages web*

## serveur web
Le logiciel `Apache` est un *serveur web*. Il transmet les pages au visiteur, il est le pendant du navigateur web

## langage de programmation
`PHP` ou `Ruby` sont des langages de programmation back, ils permettent de générer des pages web à la volée, à partir d’une base de données ou de formulaires remplis par l’utilisateur.  
`JavaScript` est un langage de programmation front, il permet de rendre les pages web dynamiques, par exemple en chargeant tout seul des nouveautés (cf facebook, tweetdeck etc.)

## frameworks
`Symfony` ou `rails` sont des *framework* back, tout un tas de code pré-mâché pour faciliter la vie des développeurs.  
`jQuery` ou `BootStrap` sont des *framework* front, tout un tas de code pré-mâché pour faciliter la vie des développeurs.  

## base de données
`MySQL`, `PostgreSQL`, c’est là que sont enregistrées toutes les informations

## Et tout ensemble
Si un site web est un restaurant …

* Le navigateur web est le client
* La page web est le plat
* Le serveur web est le serveur
* `PHP` et `Ruby` sont les cuisiniers, les framework sont les commis de cuisine
* JavaScript est l’assaisonnement disponible à votre table (sel, poivre, moutarde…)
* Les bases de données sont les frigos

## HTML simple

Voici un exemple de page web simple, sans mise en page.

1. On ouvre son éditeur de texte simple (logiciels: Visual Studio Code, Atom, Notepad++)
2. On rédige son code (voir l’exemple plus bas)
3. On enregistre le fichier comme étant au format `.html` (la pratique utilise le nom `index.html` pour la première page)
4. On peut l’ouvrir avec son navigateur web (Firefox, Safari, Chrome…)


Voici un exemple de code `HTML` à copier dans un ficher et à ouvrir:

    <!doctype html>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Mon site web</title>
    </head>
    <body>
        <h1>Mon site web est chouette <small>vous ne trouvez pas ?</small></h1>
        <div>Hello world!</div>
    </body>
    </html>

## HTML + CSS

Voici un exemple de page avec mise en page sommaire

    <!doctype html>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Le site web de Céline Verhaegue</title>
        <style>
            body {
                font-family: Helvetica, sans;
            }
            h1 small {
                color: grey;
                margin: 0 5px;
                }
        </style>
    </head>
    <body>
        <h1>Mon site web est chouette <small>vous ne trouvez pas ?</small></h1>
        <div>Hello world! Bienvenue chez <strong>Céline</strong></div>
    </body>
    </html>

## HTML + BootStrap

Les frameworks, c’est magique


    <!doctype html>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Le site web de Céline Verhaegue</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <h1>Mon site web est chouette <small>vous ne trouvez pas ?</small></h1>
        <div>Hello world! Bienvenue chez <strong>Céline</strong></div>
    </body>
    </html>

### Et pour aller plus loin ?

* `<img src="http://popschool.eu.com/wp-content/themes/popschool/img/logos/popschool.png" alt="logo de PopSchool">`
* `<a href="http://www.popschool.eu.com">Lien vers le site de PopSchool</a>`
* les documentations : https://developer.mozilla.org/fr/ et https://getbootstrap.com/docs/

Et on va pratiquer un peu tous ensemble …

# Et on fait quoi à POP ?

## Temps consacré …

Ceci est estimatif

<table>
    <tr>
        <th>Technos</th><th>Meta-techno</th><th>Misc</th>
    </tr>
    <tr>
        <td>Back: 150H</td>
        <td>Gestion de projet: 40H</td>
        <td>Veille technologique: 80H</td>
    </tr>
    <tr>
        <td>Front: 150H</td>
        <td>Gestion ordinateur et serveur: 20H</td>
        <td>Création entreprise et salariat: 20H</td>
    </tr>
    <tr>
        <td>Dev mobile: 100H</td>
        <td>Sécurité: 20H</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>Droit auteur, vie privée: 10H</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>IDE (outil de développement): 10H</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>400H</th>
        <th>100H</th>
        <th>100H</th>
    </tr>
</table>

## Listing des technos

* `HTML5` (langage front)
* `CSS3` (langage front)
    * Bootstrap (framework)
* `PHP` (langage back)
    * `Symfony` (framework)
* `JavaScript` (langage front)
    * `Vue JS` (framework)
* `Gnu/Linux` (système d’exploitation)
    * `VirtualBox` (virtualisation)
* `Visuel Studio Code` (éditeur de texte pour le code)
* `Git` (gestion de versions)
* `Slack`/`Trello`/… (gestion de projet)
* Console de développement des navigateurs web
    * CTRL+option+J dans `Safari`
    * CTRL+MAJ+J dans `Firefox` et `Chrome`
* De la culture générale (histoire de l’informatique, droit, notions en réseaux de télécommunication…)

# Parti pris pédagogique

## Rappel

PopSchool est une école d’informatique sans pré-recquis de diplôme.  
**Objectif:** Amener des jeunes vers l’emploi informatique en moins de 6 mois

## Choix

Ce choix peut changer en fonction des évolutions des demandes des entreprises avec qui nous sommes toujours en contact.

Nous nous concentrons sur le trio de base du web : `HTML` / `CSS` / `JavaScript` car ces technos sont stables dans le temps Nous formons également à `PHP` en langage _back_ car c’est le langage le plus populaire.

Nous favorisons également les _soft skills_ (savoir être) car elles importent autant si ce n’est plus que le code ! Par exemple nous rappelons qu’être méticuleux est plus important que de travailler rapidement. Quand on fait un logiciel qui va tourner pendant 10 ans, on n’est pas à 10 heures près !

# Conclusion

L’informatique est un métier essentiellement masculin et il n’y a aucune raison pour ça. D’ailleurs, à force de volonté, on réussit à féminiser le métier !

Vous venez de voir l’A.B.C de la programmation web, si ça vous a plus, rejoignez-nous !


contact pour plus de renseignements: Baptiste Dufour, responsable communication. baptiste@pop.eu.com 
