# Droits d’auteur, respect de la vie privée : mentions légales

Philippe Pary, 2017© CC-BY-SA
Pour PopSchool Lens. http://pop.eu.com

Pour tous les sites webs professionnels, des mentions légales sont obligatoires. Pour les sites personnels, le droit à l’anonymat existe. Il faut dans ce cas uniquement indiquer les coordonnées de l’hébergeur.

## Qui ?

On doit clairement identifier à qui appartient le site:

* pour un entrepreneur individuel : nom, prénom, domicile
* pour une société ou une association : raison sociale, forme juridique, adresse de l'établissement ou du siège social (et non pas une simple boîte postale), montant du capital social, adresse de courrier électronique et numéro de téléphone
* pour une activité commerciale : numéro d'inscription au registre du commerce et des sociétés (RCS)
* pour une activité artisanale : numéro d'immatriculation au répertoire des métiers (RM)

## Quelle personne contacter

Si ce n’est pas une entreprise individuelle, le site doit afficher le nom et les coordonnées d’un responsable de la publication.

## Hébergeur

On doit afficher les coordonnées de l'hébergeur du site : nom, dénomination ou raison sociale, adresse et numéro de téléphone

## Cas spéciaux

* pour une profession réglementée : référence aux règles professionnelles applicables et au titre professionnel, nom et adresse de l'autorité ayant délivré l'autorisation d'exercer quand celle-ci est nécessaire
* pour un site marchand, conditions générales de vente (CGV) : prix (exprimé en euros et TTC), frais et date de livraison, modalité de paiement, service après-vente, droit de rétractation, durée de l'offre, coût de la technique de communication à distance, no individuel d’identification fiscale, numéro de TVA intracommunautaire
* numéro de déclaration simplifiée Cnil, dans le cas de collecte de données sur les clients
