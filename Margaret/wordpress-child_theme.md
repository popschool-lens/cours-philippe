# Création d’un child theme wordpress

Philippe Pary 2019

## C’est quoi un child theme ?

Un child theme est un _thème enfant_ (quel talent, quel prof !)

C’est un thème wordpress qui va s’appuyer sur un thème **installé** sur votre instance de Wordpress et venir le corriger.

C’est la manière la plus rapide pour développer un thème personnalisé.

## Avantages et inconvénients du child theme

    ------------------------------------------------------
    |            ❤           |            💔             |
    ------------------------------------------------------
    | Rapide à créer          | Customisation limitée    |
    | Mises à jours fournies  | Failles de sécurité      |
    |  par le thème parent    |  héritées du thème parent|
    |                         | Nécessite d’avoir les    |
    |                         |  deux thèmes installés   |
    ------------------------------------------------------

## Comment qu’on créée un child theme ?

📚 RTFM ! <https://developer.wordpress.org/themes/advanced-topics/child-themes/>

1. Créer le dossier pour le thème enfant. Dans `wp-content/themes/` vous créez un dossier du nom que vous voulez
2. Trouvez un thème qui semble proche de ce que vous souhaitez réaliser en terme de fonctionnalités, de structure et d’apparence
3. Créer un fichier appelé `stylesheet.css` dont voici un exemple de contenu (seules les deux premières lignes sont obligatoires)

    /*  
       Theme Name:   Twenty Nineteen Child  
       Template:     twentynineteen  
       Description:  Twenty Nineteen Child Theme  
       Author:       Philippe Pary  
       Author URI:   http://pary.fr  
       Version:      1.0.0  
       License:      GNU General Public License v3 or later  
       License URI:  http://www.gnu.org/licenses/gpl-3.0.html  
       Text Domain:  twenty-nineteen-child  
     */

4. Créez un fichier `functions.php` et mettez-y ce contenu :

    &lt;?php  
    add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );  
    function my_theme_enqueue_styles() {  
        $parent_style = 'parent-style'; // This is 'twentynineteen-style' for the Twenty nineteen theme.  
        wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );  
        wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ));  
    }  
    ?&gt;

5. Pouf, tralala 🎶

### 🖌 Personnaliser le CSS

C’est le plus facile: mettez vos surcharges de classes dans le fichier `style.css`

⚠ Si le thème parent a plusieurs feuilles de style, vous devez les charger une à une (mais c’est rare)

➡ Si vous souhaitez que votre child theme ait plusieurs feuille de style, c’est possible mais vous devez les charger une à une (mais c’est à éviter)

### 📐 Personnaliser la structure

Vous pouvez modifier la structure HTML en copiant les fichiers `.php` contenus dans le dossier parent et en venant modifier leur contenu.

Rien n’est à faire, le remplacement sera automatique.

➡ Si vous modifiez beaucoup la structure HTML, envisagez de faire un thème from scratch !


### 🔧 Pratiquer un peu …

En partant du thème `twentyseventeen`

* Modifiez la couleur du titre en manipulant le CSS
* Modifiez l’image de la section haute
* Modifiez l’affichage du pied de page, ajoutez y des mentions légales _hardcodées_
* Publiez le tout sur framagit/github (👉 évidement vous ne versionnez que le dossier de votre thème enfant !)
