# TP de VueJS : application de gestion des élèves

Philippe Pary, 2019

## Objectif

(re)-créer en pas à pas une application de gestion des élèves

Nous sommes censés avoir les éléments suivants:
* Une API que vous connaissez bien
* Du code JavaScript qui requête cette base de données
* Une organisation fonctionnelle et algorythmique en tête

## Installer VueJS

On commence par installer VueJS

    # npm install -g vue-cli

Vérifiez que ça fonctionne avec la commande suivante, appelez-moi au besoin

    $ vue

## bootstrap du projet

On va créer un nouveau projet Vue

    $ mkdir vuejs-tp
    $ cd vuejs-tp
    $ vue init webpack
    $ npm install
    $ npm run dev

## Tutoriel autonome obligatoire

Ah ah !

Vous allez tous me suivre le tutoriel suivant: https://www.pierrefay.fr/formation-vuejs.html

## L’application students

### Design de la page

Comme dans WebPack, le fichier `index.html` va être à peine touché.

On lui ajoute le code HTML et CSS nécessaire pour appliquer le style global, ainsi que l’en-tête et le pied de page mais pas plus. Tout le reste est géré par cette discrète balise qui vous a peut-être échappé : `<div id="app"></div>` (si vous la virez, plus rien ne marchera !)

## Le composant de promotion

On va créer un composant promotion, ça revient donc à créer un fichier dans le dossier `src/components`.
Ce composant va servir à afficher une promotion, pour le moment laissez le afficher un texte statique.


Dans le fichier `src/App.vue` ajoutez évidemment ceci (si ce n’est pas évident pour vous, c’est que vous n’avez pas fait le tutoriel de Pierre Fay) :

Dans la section haute du fichier:

    <promotion v-for"promotion in promotions></promotion>

Dans la section `<script>`:

    import Promotion from './components/Promotion.vue`;

Toujours dans Dans la section `<script>` du fichier `src/App.vue` … on va récupérer les promotions via un `fetch`

### Fetcher avec Vue …

Je vous fais gratos ce que Pierre Fay tente de vous faire payer … _Software is like sex, it's better when it's free_ Linus Torvalds

La section des données doit déclarer l’existence d’un tableau de promotions :

    data {
        promotions: []
    }

À ce stade, rien ne s’affiche et c’est normal: on a un tableau vide de promotions. Le `v-for` marche donc … 0 fois !

On va créer une fonction `getPromotions()` dans une section `methods` dans `<script>`. `methods` est une section comme `data` :

    methods {
        getPromotions() {
            // ici vous mettez le fetch qui remplira le tableau promotions
            // évidement vous DEVEZ copier/coller le code du TP JS
            // refaire le truc de 0 serait une perte de temps inutile
        }
    }

`methods` permet de déclarer des fonctions mais pas de les appeler. Le fonction pourrait être appelée via un `v-on:click=getPromotions` sur un bouton.  
Pourquoi pas, mais on peut aussi la lancer automatiquement dès que le composant est prêt et monté dans le `DOM` en ajoutant une nouvelle section `mounted`

    mounted {
        this.getPromotions()
    }

Maintenant, après un temps de chargement, vous devez voir apparaître le texte statique du composant Promotion autant de fois qu’il y a de promotions dans l’API.

### Passer des propriétés

C’est mignon ce qu’on a fait, mais on affiche du texte statiques. Les propriétés des promotions ont pourtant été récupérées par le `fetch`. On va voir comment les transmettre au composant `Promotion`

Ce sont des `props`

### Dans le composant Promotion…

Dans le composant `Promotion` on ajoute une section `props` qui décrit la structure de l’objet. Seules les `props` définies seront prises en compte !

    props: {
        id: Number,
        name: String,
        '@id': String,
        '@type': String,
        students: Array,
        startDate: String,
        endDate: String
    },

Et pour les passer, on va modifier le fichier `src/App.vue` et surtout l’appel au composant `Promotion`.

On doit lui donner une clef de liaison (`v-bind:key`) qui doit être unique pour que VueJS puisse faire son travail. Et évidement, on doit lui transmettre la promotion (`v-bind`)

    <promotion v-for="promotion in promotions" v-bind:key="promotion.id" v-bind="promotion"></promotion>

## Afficher les élèves

On a une belle liste de promotions, cherchons à afficher les élèves !

C’est la même chose… du coup ça sera bien moins guidé :)

## Le composant promotion

* afficher la liste des élèves en filtrant avec un `v-if` (exemple `<student v-for="student in students" v-if="student.promotion_id==this.id">…</student>`
* créer une fonction `getStudents()`
* la lancer au montage du composant

### Le composant student

* on crée le composant `Student`
* on transfère les informations via des `props` dans `Student` et via un `v-bind`, exemple `<student v-for="student in students" v-if="student.id==this.id" v-bind:key="student.id" v-bind="student"></student>`

## Fonctions de mise à jour et de suppression

Je vais également peu guider, les notions sont vues.

Vous devez créer des fonctions _update_ et _delete_ dans les sections `methods` de vos composants `Promotion` et `Student`.

Vous devez copier/coller le code de votre TP VanillaJS et adapter les noms de variables pour VueJS

Vous pouvez créer des éléments HTML (`button`, `div`…) avec un lien `v-on:click=update` (`v-on:click=myFunctionName`) et la fonction se lancera. C’est un événement comme un autre, la fonction reçoit donc un paramètre unique, `event`.
