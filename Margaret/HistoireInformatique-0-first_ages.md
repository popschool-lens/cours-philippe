# Culture générale informatique

Cours distillé au long cours, Philippe Pary. CC-BY-SA (most recent)

## Objectifs

Ce cours doit introduire aux enjeux du droit d’auteur et du respect de la vie privée. Il présente donc l’informatique, et particulièrement son histoire, en mettant en avant les tensions qui existent autour de la notion de droit d’auteur et du droit à l’intimité et à la vie privée.

Il suit une trame essentiellement historique. La lecture de [Hackers: heroes of the computer revolution](https://www.leslibraires.fr/livre/2042477-l-ethique-des-hackers-steven-levy-globe) de Steven Levy est chaudement recommandée.

## Partie 1 : la pré-histoire

### Dark ages

L’informatique ne commence qu’avec l’apparition des machines programmables capables de traîter des données. Cependant, cette évolution technique a été rendue possible par plusieurs choses évoquée en vrac ici.

* [Pascaline](https://fr.wikipedia.org/wiki/Pascaline) : inventée par Blaise Pascal, première calculette (1642) dans le contexte de la renaissance européeenne
* [Programmation](https://fr.wikipedia.org/wiki/Ada_Lovelace) : Ada Lovelace pose les premiers algorithmes (1840) (fille de Lord Byron) dans un contexte de révolution industrielle
* [Machine de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing) : notion d’ordinateur, par Alan Turing (1936) dans l'entre deux guerres

On notera également l’existence des tisseuses programmables au XIXe siècle (machine de Jacquard, 1805, révolution industrielle française)

!["Et les français inventèrent la mode"](images/metier-jacquard.jpg)

L’objectif est ancien : créer un outil générique qui, sans grand surcoût, puisse être modifié. L’outil est le hardware (matériel), la modification est le software (logiciel). On en parle dès l'antiquité !

On oppose donc le [matériel](https://fr.wiktionary.org/wiki/mat%C3%A9riel) (latin *materialis*, *formé de matière*) au [logiciel](https://fr.wikipedia.org/wiki/Logiciel) (grec *logiké* [λογική], *science du raisonnement*)

### Aube

L’informatique voit ses balbuciemments au début du XXème siècle (IBM est fondé en 1911) jusqu’à la seconde guerre mondiale (déchiffrement d’*Enigma* par Alan Turing, contribution d’IBM à l’holocauste)

Les années 40 et 50 voient l’apparition de l’informatique moderne. Dans un contexte de guerre froide, de boom démographique et de forte croissance économique. 

!["Vous voyez le problème ?"](images/cccp-rocket.jpg)

Les machines sont excessivement coûteuses (plusieurs dizaines de millions d’euro) pour des résultats faible (la moindre montre a plus de puissance que toutes les machines de l’époque réunies) nécessitant un personnel hautement qualifié.

La programmation se fait en assembleur (programmation bas niveau), les premiers langages (LISP, FORTRAN) sont à destination d’un public averti et mathématicien.

L’informatique sert alors essentiellement à l’armée (balistique. Vous croyez que ça sert à quoi de savoir envoyer des fusées dans l’espace ?) ou aux grandes données économique (assurance, banque, collecte des impôts, statistiques nationales)

Vu le coût d’une machine, le logiciel n’a aucune importance. La question des droits d’auteur sur les logiciels ne se posait pas : le code source était distribué avec les ordinateurs, aucun contrôle n’était effectué sur la diffusion du logiciel. L’échange était la norme.  
La question des données personnelles ne se posait pas : l’échange était essentiellement technique et scientifique. Le peu de données personnelles éventuellement présentes étant constitué des premiers jeux ([spacewar!](https://fr.wikipedia.org/wiki/Spacewar!) 1962) ou des poésies (PCC, Californie)

!["spacewar!](images/spacewars.jpg)

Dans le même temps, les réseaux téléphoniques nationaux et internationaux terminent leur déploiement (réseau RTC, non-numérique). Le chantier, entamé dans l'entre deux guerres est énorme. Il faut imaginer des milliers de personnes tirant des câbles partout. Nous avons en ce moment le même chantier avec la fibre optique (mais c'est plus facile : nos pays sont encore plus développés que dans les années 50)
Les hackers se saisissent du sujet des télécommunications.  

Les financements de l'informatique sont nombreux et généreux. Dans les universités américaines nait une culture informatique (mouvement hacker au MIT)

L’informatique dispose alors d’une image exécrable auprès du public contestataire, elle est vue comme un outil militaire et totalitaire. Ceci entraînera la destruction d’ordinateurs lors des manifestations contre la guerre du Viet-Nam au sein des Universités. L’informatique était une cible en tant que tel pour les groupes les plus radicaux, comme le Weather Underground.  

![Le weather underground](images/weathermen.jpg)

Ces destruction d’un matériel très coûteux entraînent un premier mouvement de protection du matériel informatique (serrures, vigiles …) qui sera combattu par le mouvement hacker naissant.

On notera égalemnt l’aversion du grand public pour ces outils et la crainte très vive du recoupement de fichiers, entraînant notamment la loi *informatique et libertés* de 1978, projet soumis dès 1970. À l’époque on s’offusquait du croisement du fichier des cartes d’identité et des régimes de retraite ([plus de détails](https://fr.wikipedia.org/wiki/Loi_informatique_et_libert%C3%A9s#Le_projet_SAFARI_et_la_cr.C3.A9ation_de_la_CNIL))  

![Alex Türk](images/alexturk-cnil.jpg)

## Conclusion partie 1

On voit émerger la contre-culture hacker à l’ombre des financements gigantesques des armées et des États dans un contexte de guerre froide et de forte croissance économique. L’informatique institutionnelle est évidement une industrie prospère (IBM, DEC, AT&T, PTT & cie). Rappelons que si de nombreux travaux concernent la téléphonie, Internet n’existe pas encore. Pas même sous forme embrayonnaire.
