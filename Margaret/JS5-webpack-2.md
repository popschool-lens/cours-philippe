# Mise en application de WebPack 2

Philippe Pary, 2018

## Objectifs

Transformer un projet existant et l’adapter à WebPack

1. Mise en place du boilerplace
2. Copier le code au bon endroit
3. Installer les dépendances
4. Adapter le code
5. Samba !

Pour ça on va s’appuyer sur le CV trop ambitieux de Gwenaëlle ! (dans sa version du 20 décembre 2018 : https://github.com/Sheeptune/Resume/tree/d11b143d5d54ede85e9ab1fe2cc7b57e53dfdd8c)

## Récupérer le BoilerPlate …

Alleï, comme d’habitude on va commencer par un git clone du projet

    $ git clone git@github.com:loicpennequin/Pop-School---boilerplate-Webpack.git cv-gwen

Et on suit la procédure du `README.md` avec un `npm install`, mise à jour du `README.md` et `rm -Rf .git`. Simple, basique

## Récupérer le code du CV de Gwenaëlle

Dans un autre dossier, on va récupérer le code de Gwenaëlle

    $ git clone git@github.com:Sheeptune/Resume.git

Et on va faire quelques copier/coller intelligents…

On a deux dossiers où on doit mettre le code:
* `src`: source JS/CSS de l’application et les assets correspondants (images, fonts…)
* `dist`: code à distribuer où on peut mettre directement l’HTML vu notre configuration et les assets correspondants (images)

On y va, on parcours le dossier _Resume_

_pro-tip: on va peut-être devoir copier/coller deux fois un même dossier pour pas se prendre le chou. Mais lequel ? :)_

## Installer les dépendances

Regardons le `head` du fichier `dist/index.php` de Gwenaëlle. On constate 4 grandes dépendances:
* Bootstrap
* jQuery
* fontawesome
* zenscroll

alleï, je vous le donne. Mais normalement vous le trouvez par recherches longues et épuisantes sur le net. Et en finissant par demander conseil à quelqu’un (à Loïc Pennequin, donc)

    $ npm install --save bootstrap jquery popper.js zenscroll @fortawesome/fontawesome-free

## Modifier le code

### head du index.php

On vire tous les liens JS et CSS, on les remplace par:

    <script type="text/javascript" src="assets/app.js" defer></script>
    <link rel="stylesheet" href="assets/app.css">

### modification de index.js

Le fichier `src/js/index.js` est le point d’entrée de WebPack. On doit le modifier pour prendre en compte les fichiers CSS et les fichiers JS

    import 'bootstrap';
    import 'zenscroll';
    import navbaractive from './main.js';
    import carroussel from './myscript.js';
    import switchnav from './switch-nav.js';
    import 'bootstrap/dist/css/bootstrap.min.css';
    import '@fortawesome/fontawesome-free/css/all.css';
    import '../styles/bootstrap_custom.css';

    navbaractive();
    carroussel();
    switchnav();

### modification des fichiers JS

On englobe de fonctions le code JS des fichiers main, myscript et switch-nav, et on ajoute un `export`

Exemple d’englobage:

    function carroussel() {
        // ici le code
    }

    export default carroussel;

À faire avec `navbaractive`, `carroussel` et `switchnav`

## Here we go again

Normalement, ça devrait marcher… hein… Alors on brûle un cierge à Sainte Rita et on lance:

    $ npm run build

Mais ça ne marche pas ! Rhooooo, lisez le message d’erreur. Corrigez et relancez:

    $ npm run build

_et pourquoi pas npm run dev ? Excellente question ! Car le projet tient sur du code PHP qui n’est pas géré par WebPack. On pourrait s’en passer, mais je voulais garder le cours léger…_

Et si ça builde, on lance le serveur PHP

    $ php -S localhost:8080 -t dist

Dans le navigateur, on doit y voir plus clair

Comme on n’utilise pas `npm run dev`, on devra relancer `npm run build` à chaque modification dans `src`. Glups

## Tudo bem ?

_Mon JS est pas terrible, mon CSS est catastrophique, la situation est excellent, je publie_ [Ferdinand Foch](https://dicocitations.lemonde.fr/citations/citation-47326.php)

Le code, il juste marche. Mais on a vu l’erreur CSS, et il s’avère qu’il sera difficile à maintenir dans le temps.

### Small is beautifull

On a une première solution: splitter le code CSS en plusieurs fichiers (comme c’est déjà fait pour le JS) qu’on linkera dans `index.js`

Le but est que le fichier `bootstrap_custom.css` puisse être supprimé car il ne contient plus aucune ligne !

➡ allez dans le dossier `src/styles/` et supprimez tous les CSS de _bootstrap_, _fontawesome_ & cie. Ils ne servent plus à rien  
➡ allez dans le dossier `src/js/` et supprimez tous les JS de _bootstrap_, _jquery_, idem, ils ne servent plus  
➡ certaines images sont trop grandes, vous le voyez via la sortie de `npm run build`, redimensionnez les via _Photoshop_, _Gimp_ ou _imageMagick_  
➡ prenez les grandes sections du fichier `bootstrap_custom.css` et faites en des fichiers séparés  
➡ incorporez les dans `index.js`  
➡ `npm run build` et on vérifie que tout se passe bien

### Tudo bem !

http://getbem.com

Bem est une méthodologie CSS simple à comprendre et efficace.

On prépare les noms de classes avec une catégorisation hiérarchique:
* Blocks: les grandes balises structurantes, aussi appelées _balises sectionnantes_ (`header`, `section` …)
* Elements: les éléments composants les blocks (`div`,`p`,`button`,`input`…) qui seront toujours contenus dans un bloc
* Modifiers: les modifications stylistiques des blocks et des élèments

On nomme les choses simplement:
* `.block` pour les blocs. Exactement comme vous le faites aujourd’hui (ex: `.header`, `.portfolio`)
* `.block__element`, `.__element` pour les éléments (ex: `.header__link`, `.portfolio__img`)
* `.block__element--modifier`, `.block--modifier`, `.__element--modifier` pour les modifiers (ex: `.header__link--big`, `.__link--big`…)

Appliquer cette méthodologie sauve des vies, pensez-y !
