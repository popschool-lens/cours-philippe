# Droits d’auteur, respect de la vie privée : le RGPD

Philippe Pary, 2017© CC-BY-SA
Pour PopSchool Lens. http://pop.eu.com

## Simple

Vous faites pas chier, tout est sur <https://www.cnil.fr>. Vous pouvez dormir durant ce cours, tant que vous oubliez pas que tout est sur <https://www.cnil.fr>. Mais vraiment, limite vous pouvez aller vous rouler un joint, mais attention aux pertes de mémoire, faudrait pas oublier <https://www.cnil.fr>

<https://www.cnil.fr>

Merci et à la semaine prochaine !

…

Si vous êtes toujours là …

## Référentiel général de protection des données

Aussi connu sous le nom de GDPR (general data protection regulation)

Vous pouvez consulter l’excellente page de la CNIL sur le sujet:
<https://www.cnil.fr/fr/principes-cles/rgpd-se-preparer-en-6-etapes>

Vous êtes concerné si vous enregistrez ou traitez des données à caractère personnel

➡ une simple création de compte, même _anonyme_ vous met dans le périmètre

Vous avez quelques obligations:

* Rectification et suppression des données par l’utilisateur
* Obtenir le consentement éclairé de l’utilisateur
* Expliquer clairement ce que vous faites sans le jargon juridique
    * le jargon juridique doit exister et correspondre aux explications claires
* Autoriser la CNIL à débarquer à l’improviste pour examiner vos données et vos logiciels
* Ne collecter que les données nécessaires à votre activité

Si vous vivez du traitement et du commerce de données personnelles ➡ **consultez un avocat** ⚠

Ceci s’applique du moment où vous traitez les données de citoyens ou d’entreprises européennes où que vous soyez dans le monde, quelque soit la nationalité de l’entreprise.

Les amendes peuvent aller jusqu’à 4% du chiffre d’affaire mondial

