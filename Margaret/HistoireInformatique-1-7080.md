# Culture générale informatique

Cours distillé au long cours, Philippe Pary 2016. CC-BY-SA (most recent)

## Objectifs

Ce cours doit introduire aux enjeux du droit d’auteur et du respect de la vie privée. Il présente donc l’informatique, et particulièrement son histoire, en mettant en avant les tensions qui existent autour de la notion de droit d’auteur et du droit à l’intimité et à la vie privée.

Il suit une trame essentiellement historique. La lecture de *hackers* de Steven Levy est chaudement recommandée.

## Partie 2 : les années 70 et 80

Cette section de l’histoire informatique nous intéresse particulièrement : les notions de droit d’auteurs s’imposent, la question du respect de la vie privée émerge, les bases technique d’Internet apparaissent

### La généralisation de l’informatique en dehors des très grandes organisations

Les années 60 ont vu les coûts du matériel informatique se réduire. Toutes les facultés sont équipées et des entreprises, même d’envergure modeste, commencent à s’équiper.

On commence à voir apparaître du matériel à destination du grand public (processeur 8080 d’Intel, ordinateur Altair …)

La généralisation de l’informatique voit apparaître un grand nombre de bidouilleurs se lançant dans l’informatique. La silicon valley apparaît, retenez le Xerox research center de Palo Alto (invention de la souris, du disque dur, de la notion d’interface graphique …)

L’intérêt pour le matériel s’affaiblit face à la montée de l’intérêt sur le logiciel. Un ordinateur vaut 2000$, l’importance relative du logiciel devient conséquente. Création d’Unix, et du C, en 1970 par Kenneth Thomson et Denis Ritchie.

### Les pirates de la silicon valley

C’est le nom d’un téléfilm instructif sur cette période de l’histoire et ses conséquences. Il évoque l’apparation d’Apple (Steve Wozniak et Steve Jobs, débutant dans la bidouille illégale de lignes téléphoniques, exploitation des recherches de Xerox) ou de Microsoft (Paul Allen et Bill Gates, exploitation des recherches d’Apple)

La culture hacker historique, libertaire, s’oppose alors à une nouvelle culture hacker entrepreunariale

L’idée qu’un logiciel puisse être distribué uniquement par une entreprise apparaît.

### An open letter to lobbyist

La version de Microsoft du langage BASIC, l’une des meilleures disponibles à l’époque, s’échange sous le manteau. Bill Gates écrit en février 1976, ele est largement relayée dans la presse informatique.  
Elle pose les bases de l’argumentation du logiciel propriétaire pour qui elle constitue un acte fondateur. S’il émergeait avant, il a pris consistance à cette occasion.

> Most directly, the thing you do is theft.

![Lien vers le texte de la lettre](http://www.blinkenlights.com/classiccmp/gateswhine.html)

### Croisements de fichiers

La généralisation de l’informatique entraîne la généralisation des données personnelles collectées.  
Marqué par le souvenir récent de la seconde guerre mondiale (les fichiers élaborés par la République servirent à la traque des juifs, des communistes etc.), en 1976 est votée la loi informatique et liberté.

L’enjeu du respect de la vie privée apparaît, c’est essentiellement une affaire de gouvernement contre les citoyens pour le moment. En effet, sans connexion, les données de chacun sont échangées par disquettes et ne sortent pas des ordinateurs.

### Michael Haert, le projet GUTENBERG

Lancé en 1971, le projet vise à numériser tous les livres disponibles dans le domaine public. Initialement manuelle, la copie passe à présent par des logiciels de reconnaissance optique des caractères (OCR)  

### rms, le logiciel libre et le projet GNU

Au début des années 80, la notion de partage des logiciels semble une cause perdue, dévolue aux seules facultés avec aucun avenir sérieux en dehors du monde universitaire.

Au MIT, Richard Stallman, suite à un problème d’imprimante (tout commence par une imprimante qui fait chier en informatique) se décide à inverser la tendance. Il pose en 1984 la définition du logiciel libre. Son avancée la plus importante est de s’appuyer sur le système droit d’auteur pour le combattre.

0. Liberté d’usage : pas de restriction à l’utilisation d’un logiciel
1. Liberté d’étude : droit d’étudier le code source du logiciel pour comprendre son fonctionnement
2. Liberté de distribution : droit de partager des copies du logiciel autour de soi
3. Liberté d’amélioration : droit de modifier le logiciel

Il fondera également le projet GNU dont le but est d’offrir tous les logiciels informatique en version libre.  
La Free Software Foundation, qu’il fonde, publie une licence logicielle libre valide juridiquement appelée la GPL (General Public Licence)  
Autour de 1990, toutes les bases du logiciel libre sont posées

## Les bases d’internet

Pendant ce temps à Vera Cruz … L’armée constate la généralisation des réseaux informatiques et leur caractére vital. Elle finance largement des recherches pour créer un réseau informatique apte à survivre à une attaque nucléaire : ça sera ARPANET. Elle laisse les universités s’approprier ces travauxpour leur usage.  
Autour de 1990, toutes les bases d’Internet sont posées

## Conclusion

Les années 70 et 80 ont vu un mouvement d’enclosure, le partage généralisé disparaît. Le logiciel propriétaire émerge.  
Le logiciel libre apparaît en réaction.  
Les bases d’Internet sont posées
