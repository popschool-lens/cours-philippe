# Panorama des technologies du web et des environs

Philippe Pary, 2018

CE DOCUMENT EST TOTALEMENT À LA RACHE

<https://www.la-rache.com/>

## Le trio de base

### HTML

1991, Sir Tim Berners Lee, au CERN à Genève (mais le bureau où ça s’est passé est techniquement en France)

Officiellement, c’est le W3C qui maintient. De facto c’est le WHATWG (qui regroupe les grands acteurs du web : Google, MoFo etc.)

### CSS

bla bla

### JS

bla bla


## Les frameworks front

### CSS: Bootstrap / Materialize

Il doit en exister d’autres non ?

### JavaScript: VueJS, React, Angular

* VueJS: simple, proche de VanillaJS
* React: à la mode, techo de chez FB
* Angular: à la mode dans la grande distribution
* AngularJS: c’est raide

## Les langages Back

### PHP

Fin des années 90, créé en réaction à Perl. C’est un langage qui n’existe que pour le web

### Pyhon

Langage des mathématiciens. Prend de plus en plus d’importance via le big data / open dat

### ruby

Langage japonais reconnu pour ses qualités mais qui n’a jamais vraiment percé en Europe

### Perl / C / etc.

Vous imaginez pas ce avec quoi on peut coder du back. Le C et le perl étaient les langages back du web du début des années 90.

Le C revient en force avec WebAssembly (applications binaires lancées via le navigateur web)

### JavaScript (NodeJS)

Abomination, apostasie, ces choses là ne devraient pas exister. Mais elles existent.

## Les frameworks back

### PHP: Symfony / Laravel

Symfony c’est le framework français qui monte

Laravel c’est le framework US en place

Il existe d’autres frameworks, mais vous pouvez les ignorer

### Python: Django

Python n’est pas prévu pour le web, Django lui apporte ce qu’il faut. Les back-offices Django se ressemblent tous, ça en est triste

### ruby: rails

ruby n’est pas un langage web, il faut passer par le framework ruby

### JavaScript: NodeJS, express, Vulcan, socket.io

Sacrilège ! Blasphème !

## Les bases de données

## MySQL / MariaBD

Base de données historique du web. Le rachat par Oracle a provoqué un fork, MariaDB

C’est une base de données qui fut simple à prendre en main (l’existence de PHPMyAdmin y a largement contribué) mais qui présente des défauts dès qu’on attaque les choses sérieuses: gros volumes de données, sécurité, rapidité …

## PostgresSQL

LE serveur de base de données du futur. C’est rapide, fiable, ça gère les gros volumes de données, ça gère les données spatiales (vous imaginez pas le bordel que c’est !)

Ça monte en puissance

## MongoDB

Base de données de type _noSQL_ (not only SQL) la plus populaire

## Les technologies voisines

### Java

C’est le langage des années 90, il y a encore énormément de demande là-dessus. Le stack de code de la grande distribution dépend de ça.

Tomcat est le serveur web associé aux sites webs développés en java

### Cobol

Langage de programmation des années 80. Le stack de code des banques et assurances qui tourne avec ça est toujours monstrueux et il y a une pénurie énorme de main d’œuvre par manque de formation. Ça recrute à mort sur des salaires importants.

### SVN

L’ancêtre de git. C’est plus simple à comprendre mais c’est nettement moins bien

### Docker / Kubernetes

docker est un système de containerrs, des applications pré-installées avec toutes leurs dépendances. Ça simplifie le déploiement de logiciels webs multiples et ayant des dépendances disparates. Kubernetes permet de gérer facilement des instances Dockers

### virtualbox / proxmox

Ce sont des solutions de virtualisations : ça permet de créer des ordinateurs virtuels sur votre ordinateur à des fins de test (VirtualBox) ou d’hébergement (Proxmox)

## Les stacks

### LAMP

Linux Apache MySQL PHP. C’est le stack par défaut du web qui perd de puissance mais reste ultra-majoritaire.

Il existe des tas de logiciels qui prennent le nom LAMP pour être téléchargés : au mieux ce sont des logiciels moisis, au pire des virus. Un stack LAMP s’installe à la main ou via une image docker

## MEAN

MongoDB, Express (back), Angular (front), node (back). Stack en cours de déploiement chez l’AFM (association famille Mulliez) en remplacement des fronts Java/Tomcat

## Les outils

### npm, webpack

NPM est node packages manager, un gestionnaire de librairies JS très à la mode, utilisé partout

webpack est un environnement de développement offrant plein d’outils utiles au quotidien (minification, tests etc.)

## composer

Gestionnaire de librairies de PHP

## PHPUnit, Jasmine, Mocha …

Outils de tests unitaires

## Eclipse, emacs

IDE, logiciel à tout faire. Très utilisé en SSII. C’est le mal

## Atom, SublimeText, VisualStudio, Vim, Notepad++

Des éditeurs de texte

