# Installation de VPS, partie 3

# Objectif

Il reste encore deux-trois petites choses oubliées par les deux sessions précédentes. Je les ajoute

## Sécurité, encore et toujours

### Virer la « backdoor » d’OVH

OVH a oublié un compte avec les droits `sudo` et dont il avait besoin pour l’installation, oh zutalors.

Ça donne un accès très facile à votre serveur pour OVH. On va corriger ça:

    # userdel debian
    # rm -R /home/debian

### Y voir plus clair sur ce qui se passe sur la machine

On va installer quelques logiciels d’audit qui permettraient, en cas de besoin, de mieux savoir ce qui se passe sur notre serveur

    # apt install auditd

## Accès MySQL

J’ai oublié de vous donner des droits plus avancés pour PHPMyAdmin…

    # mysql -u root mysql

Puis dans le client MySQL qui est lancé

    GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'localhost' WITH GRANT OPTION;
    FLUSH PRIVILEGES;

Et c’est bon. Si vous étiez déjà connectés à PHPMyAdmin, vous devez vous déconnecter et vous reconnecter.

## Quelques paquets oubliés, une configuration cassée

Il manque quelques logiciels pourtant utiles

    # apt install git less dnsutils whois unzip letsencrypt

Les locales sont cassées…

    # dpkg-reconfigure locales

Dans la liste des configurations, cochez `FR.UTF-8 UTF-8`


