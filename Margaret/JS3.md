# Cours de JavaScript, travail avec les API

Philippe Pary, 2018

## Objectifs du cours

Ayé, on avance vers les choses pratiques. Aucune nouvelle motion, juste la mise en œuvre de ce qu’on a vu.

Prévoyez des dolipranes, il va falloir réfléchir beaucoup

➡ comprendre ce qu’est une `API`  
➡ comprendre la notion de CRUD  
➡ réussir à récupérer les données d’une `API` et les afficher  
➡ réussir à envoyer des modifications/suppression de données vers une `API`  

## Une API ?

Une `API` est une interface de programmation applicative. C’est une boîte noire qui suit certaines règles (`REST`, `SOAP`, `GraphQL`…) qui va servir à gérer les données et fournir des services (tirer un chiffre au hasard par exemple)

Les `API` reposent sur des **entry points** (ou un seul entry point pour `GraphQL``). Ce sont des adresses documentées, on n’a pas à les deviner par nous-même.

Les `API` reposent également sur les **verbes HTTP** qui sont des paramètres transmis à chaque requête HTTP:
* `GET`, lecture/accès. C’est la requête par défaut quand vous tapez une URL dans un navigateur web
* `POST`, envoi (sert pour créer ou compléter une ressource)
* `DELETE`, suppression
* `PUT`, modification (la différence avec `POST` est subtile)
* `PATCH`, correction (ahem… donc `POST`, `PUT` ET `PATCH`). Heureusement, on le croise peu

Allons visiter l’API des exercices futurs: http://api-students.popschool-lens.fr/api/

### Premier exercice

À l’aide de l’interface graphique proposée par _API Platform_, mettez à jour votre fiche en indiquant votre sexe et votre date de naissance

## Les explorateurs d’API

Les vrai.e.s, les dur.e.s, les tatoué.e.s utiliseront le logiciel `httpie` : https://httpie.org/

Les autres, les hipsters, les moules du bulbes utiliseront le logiciel `postman` : https://www.getpostman.com/

Ces logiciels vont permettront d’explorer l’API. Ça se fait avec la documentation sous les yeux évidement

### Second exercice

Au travers du logiciel de votre choix, `httpie` pour mes chouchous, `postman` pour la plèbe, visionnez les données de l’API évoquée plus haut.

## Utiliser les données

Vous l’avez repéré, les données transitent en `JSON`. Vous connaissez.

En mixant `fetch`, votre connaissance de `JSON` et un peu de tout le reste, on va se faire la plus belle application de gestion des étudiants du monde.

**Nota bene** vous n’avez pas à envoyer d’`id` à la création d’une ressource (élève ou promotion)

### Troisième exercice

Afficher les promotions

➡ Récupérez la liste des promotions via un `fetch` et affichez la via un `console.log`  
➡ Améliorez la fonction, affichez la liste des promotions sur la page web  
➡ Rendez-moi ça incroyablement beau  

### Quatrième exercice

Ajouter un champ pour créer une promotion

➡ Vous ajoutez un champ de formulaire, quand on clique dessus, ça crée une promotion

* pensez à faire des essais via `httpie` ou `postman` avant
* lisez la MDN et n’hésitez pas à vous faire aider pour la rédaction de la requête `fetch` de création !

### Cinquième exercice

Créer une page où on peut modifier ou supprimer une promotion

➡ On va récupérer l’`id` de la promotion à modifier via un paramètre `GET` (ie un paramètre dans l’URL). Voir https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams/URLSearchParams (on, il n’y a pas de VF, on a atteint le bout de ce qui est documenté en français)  
➡ Créez un lien pour chaque promotion sur la page qui les liste  
➡ Pensez à mettre un `confirm` sur la suppression, évidement…  
➡ Faites vous aider pour la rédaction du `fetch` mais cette fois-ci ça devrait aller un peu mieux …

### Sixième exercice

La même chose, mais avec les élèves. Trop facile

➡ Créez une page affichant tous les élèves d’une promotion. Évidement, on récupère encore l’`id` via l’URL  
➡ Créez une page affichant un élève en particulier. Là encore, `id` de l’élève en `GET`  
➡ Faites les liens depuis la liste des promotions (pour avoir les élèves de la promotion) et depuis la liste des élèves (pou avoir la fiche d’un élève)  
➡ Sur la page listant les élèves d’une promotion, ajoutez un champ pour créer un nouvel élève  
➡ La fiche de chaque élève doit pouvoir, à l’instar de la fiche de chaque promotion, permettre la modification et la suppression de l’élève concerné  
➡ Évidement, tout ça sera superbe. Ceux qui s’en sentent le courage peuvent faire l’application sur une seule page 
