# Installation de VPS, partie 2

# Objectif

![Un chaton ?](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Ailurus_fulgens_RoterPanda_LesserPanda.jpg/393px-Ailurus_fulgens_RoterPanda_LesserPanda.jpg)

Après avoir mis en place le VPS, nous allons finir la configuration et commencer à utiliser notre serveur de développement

# Apache et fichiers HTML

![WTF is it ?](https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Lesser_panda_standing.jpg/640px-Lesser_panda_standing.jpg)

## Préparer le chargement du code

Nous allons charger nos premières pages webs.
Par défaut, Apache HTTPd utilise `/var/www/html/` comme dossier racine du serveur web

&gt; Le dossier racine du serveur web est l’endroit où le / de *http://<ipmonserveur>/* amène.

On va reproduire le fonctionnement des postes et lier `/var/www/html` vers le dossier `~/projets`

    $ ln -s /var/www/html projets
    # chown -R monutilisateur:monutilisateur /var/www/html

Et on va modifier le fichier `/etc/apache2/envvars` pour remplacer `www-data` par `nomutilisateur` pour la variable `APACHE_RUN_USER`

    # nano /etc/apache2/envvars

Et relancer le serveur pour que les changements soient pris en compte

    # service apache2 restart

### Et on ouvre la bête

Nous allons regarder un peu la configuration d’apache. Elle tient, dans notre cas, sur deux fichiers :

* /etc/apache2/apache2.conf
* /etc/apache2/sites-available/default.conf

*NB: les modules (mods) d’apache, la configuration (conf) et les sites webs (sites) sont rangés dans des dossiers &lt;type&gt;-available et s’activent avec les commandes a2enmod, a2enconf, a2ensite. Un redémarrage d’Apache est nécessaire pour les modules (# service apache2 restart) et un rechargement pour les configurations et sites webs (# service apache2 reload)*

## Charger les fichiers

![C’est pas trop dur C’est pas trop dur ?](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flat_panda.jpg/450px-Flat_panda.jpg)

On va charger le dossier du projet personnel

Par SCP, depuis votre ordinateur :

    $ scp -r <cheminversledossier> <identifiant>@<ipmonserveur>:~/projets

* scp est un cp via SSH. -r c’est récursif : on copie tout le dossier

Vous pouvez aussi utiliser FileZilla

## Vérifier que ça marche

Avec votre navigateur, aller sur *http://&lt;ipduserver&gt;/&lt;nomdudossier&gt;/*

Ça affiche votre projet. Bravo ! Votre première publication … en local. Vous verrez, ça sera plus ou moins pareil pour mettre en ligne.

## Qu’est-ce qu’on se fait chier

Par contre, lancer scp à chaque fois qu’on modifie un truc, surtout quand on est un gros n00b et qu’on code à tatons, c’est juste chiant.

On va donc utiliser Nautilus pour *monter* le dossier distant comme un dossier local. Trop fun.


![Documentation sur ubuntu-fr.org](https://doc.ubuntu-fr.org/ssh#monter_un_repertoire_distant_navigation_via_sftp_secure_file_transfer_protocol)

Pour les maceux et ceusses sous Windows, je veux bien que vous vous documentiez et me donniez le résultat de vos recherches.

# Un peu de sécurité avec phpmyadmin

Vous êtes sur le grand internet et ça craint ! Vous devez déjà avoir dans vos logs des tentatives d’attaque …

    # less /var/log/apache2/access.log
    # less /var/log/apache2/error.log

On va éviter les soucis et rendre l’accès à PHPMyAdmin plus complexe …

    # nano /etc/phpmyadmin/apache.conf

Modifiez l’alias de `/phpmyadmin` à autre chose, n’importe quoi. `/serveur-bdd` par exemple

Vous accéderez maintenant à PHPMyAdmin par cette adresse

# Développer en remote

![is it a bird ? is it a plane ?](https://upload.wikimedia.org/wikipedia/commons/b/b5/Pandas_vermelho.jpg)

## Quelques conseils

* Plutôt que de faire un copier, on fera un git clone !
* Et on n’oubliera pas de commit
* Et encore moins de pusher
* Les plus subtils d’entre vous pourront utiliser l’intégration continue

## Quelques constats

Le montage du dossier permet d’utiliser ses outils simples (visual studio, interface graphique). 

Ceci étant, sur le serveur, si on maîtrise un peu les outils d’adultes (vim, emacs), c’est comme si on travaillait à la maison. Mais c’est une mauvaise idée de modifier les fichiers depuis le serveur.

Le must est d’utiliser un git, sur lequel on commite, on pushe et on pulle.

![No, it’s a firefox !](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Red_Panda_Face.jpg/640px-Red_Panda_Face.jpg)
