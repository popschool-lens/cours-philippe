# Configuration du Virtual Private Server

Philippe Pary, 2018

# Intro

# VPS

Un VPS est un serveur virtuel. Vous vous y connectez comme sur un vrai serveur mais en réalité il n’y a pas de matériel associé. C’est un serveur virtuel qui est géré par un logiciel appelé superviseur sur une infrastracture logicielle complexe et à haute disponbilité.

C’est pas cher, théoriquement c’est mieux d’un point de vue écologique. Ça s’administre exactement comme un vrai serveur.

Bref, vous aller rapidement apprendre la ligne de commande (indispensable pour gérer correctement une machine), le logiciel `SSH` (qui permet l’accès distant sécurisé), quelques notions d’administration système et à gérer ça avec votre nom de domaine.

Il est probable qu’au quotidien vous vendiez des espaces mutualisés aux plus petits clients : ceux-ci sont plus simples à gérer. Tout peut se faire via l’interface d’administration d’OVH. Après être passé à PopSchool et avoir géré un VPS, gérer un espace mutualisé vous paraîtra enfantin ! :)

# Debian

![Logo Debian](https://www.debian.org/logos/openlogo.svg)

Nous avons fait le choix d’installer le VPS avec [le système d’exploitation libre Debian](https://www.debian.org/)

Debian est un projet communautaire auto-géré qui vise à produire un système d’exploitation libre. Il est régit par un contrat social et il dispose d’une forte culture interne. C’est un système d’exploitation très populaire pour créer des serveurs webs, un standard de fait.

Par delà les aspects militants et culturels, Debian présente quelques points techniques forts:

1. installation des logiciels par système `dpkg` (_Debian Packages_) et les logiciels `apt`, `aptitude` etc.
2. division du projet en 3 branches: `stable` (très sécurisé, pour les serveurs), `testing` (quelques soucis mineurs, acceptable pour l’usage personnel), `sid` (_still in development_) et `incoming` (pour les développeurs de Debian)
3. dans chaque branche les logiciels sont classés en trois catégories: `main` (les logiciels libres), `contrib` (logiciels libres avec dépendances non-libres) et `non-free` (logiciels propriétaires)

# Post-installation

Vous avez reçu un mail d’OVH avec vos identifiants, notamment un compte `root`.

## configuration du DNS

1. Dans l’interface OVH, allez dans la section `Web`
2. Dans la section `Web`, dans le menu de gauche `Domaine`. Cliquez sur votre nom de domaine
3. Allez dans l’onglet `Zone DNS`
4. Modifiez l’entrée avec votre nom de domaine et ayant pour `Type` la valeur `A`. Renseignez l’IPv4 indiquée dans le mail reçu par OVH
5. (facultatif) Vous pouvez ajouter une entrée de type `AAAA` et mettre l’adresse IPv6 (il n’y a jamais eu IPv5) et vous serez très modernes :)

Le `DNS` (_Domain Name System_) transforme les noms de domaines en adresses IP. Les adresse IP sont des chiffres binaires sur lesquels on peut faire des calculs, ce sont des adresses dites _routables_. Les IPv4 sont constituées de 4 séries de 4 octets (0 à 255). Les IPv6 … c’est les autres, vous vous documenterez.

Quelques adresses ou plages ne sont pas routables. Citons en quelques unes:
* 127.0.0.1 et ::1 désignent toujours votre ordinateur/serveur
* Les adresses en 192.168.X.X, 10.X.X.X, fc00:: ou encore fe80:: sont des adresses locales

Vous aurez remarqué si vous avez créé l’entrée `AAAA` la possibilité de créer un sous-domaine facilement.

Notons également que le système DNS contient des temps de propagation et des systèmes de cache. Pour être certain, comptez 24H de propagation pour l’Europe, 48H pour le reste du monde. En réalité, ça peut être bien plus rapide … ou bien plus long :)

## première connexion au serveur

Vous allez vous connecter en `SSH` à votre serveur. `SSH` est un protocole léger et sécurisé d’accès à distance (_Secured SHell_)

Nous allons utiliser le logiciel `OpenSSH` qui est une implémentation de `SSH` et pour ça… on doit l’installer !

Gnu/Linux:

    # apt install openssh-client

(le `#` indique une commande à faire en administrateur, donc via `sudo` ou via l’utilisateur `root`)

MacOS:

    # brew install openssh-client

(le `#` indique une commande à faire en administrateur, donc via `sudo` ou via l’utilisateur `root`)

Windows:

Soit vous avez activé Debian/Ubuntu et dans ce cas vous suivez les consignes Gnu/Linux, soit vous vous démerdez avec PuTTY

Et pour établir la connexion

    $ ssh root@monsuperdomaine.kim

Si ça ne marche pas encore (délais de propagation DNS)

    $ ssh root@ipdemonserveur

Pour ceusses avec PuTTY : démerden sie sich

## ajouter quelques paquets

Lancez la commande suivante depuis l’utilisateur `root` ou en la précédant de la commande `sudo`. Le `#` indique qu’il s’agit d’une commande à lancer en tant qu’administrateur, vous ne devez pas le recopier.

    # apt update

C’est un réflexe à avoir : mettez à jour la liste des paquets avant d’installer de nouveaux logiciels.

Ensuite, tapez la commande suivante (sans le `#` qui indique que c’est une commande administrateur)

    # apt install sudo vim htop multitail screen bash-completion

* sudo : sudo, pour lancer des commandes administrateur à la volée
* vim (ou emacs) : pour bricoler à l’arrache des fichiers
* htop : top amélioré
* multitail : pour surveiller plusieurs logs (journaux) à la fois
* screen : pour pouvoir lancer des commandes et ne pas les perdre si on se déconnecte brutalement
* bash-completion : c’est la vie. Ça permet de remplir avec la touche `tab` tous les paramètres des programmes

## créer un utilisateur pour ne pas accéder directement en root

    # useradd -s /bin/bash -m monutilisateur
    # passwd monutilisateur
    # usermod -a -G sudo,adm monutilisateur

Vous ne recopiez pas les `#` qui indiquent des commandes à faire en administrateur.

Tentez de vous connecter en SSH avec ce nouveau compte utilisateur et tentez la commande `sudo ls /root` pour vérifier que le nouveau soit bien sudoer.

Après ça, modifiez le mot de passe root: ce dernier est passé en clair par mail … il est compromis

    # passwd

## avoir un accès sécurisé

Installez un client SSH sur votre machine, puis générez et copiez une clef privée.

* [Pour Windows](https://docs.microsoft.com/fr-fr/azure/virtual-machines/linux/ssh-from-windows) ou vous utilisez le `WLS`

Pour Gnu/Linux et MacOS on passe les commandes suivantes (le `$` indique une commande utilisateur, inutile de le recopier)

Puis on passe les commandes suivantes (le `$` indique une commande utilisateur, inutile de le recopier)

    $ ssh-keygen
    $ ssh-copy-id utilisateurduserveur@adresseduserveur
    $ ssh utilisateurduserveur@adresseduserveur

Si aucun mot de passe n’est demandé, c’est que ça marche !

On retire la connexion root via SSH (`# nano /etc/ssh/sshd_config`, inutile de recopier le `#`, modifier la ligne `PermitRootLogin no`), on relance le service (`# service ssh reload`, le `#` indique une commande administrateur, il n’est pas à recopier)

* la clef SSH : c’est une bonne pratique. Idéalement, on désactiverait l’accès par mot de passe
* Accès root désactivé : là par contre, ça doit être un réflexe. Pas d’accès root à distance !
* Du coup vous vous connectez avec votre nouvel utilisateur !

Tentez également de vous connecter en *SFTP*. Installez le logiciel *FileZilla* et vérifiez que vous arriviez bien à vous connecter.

## On installe le serveur web

Notre serveur est mignon, mais rien ne marche !
1. On installe apache2 : serveur web
2. On installe mysql-server : serveur de base de données
3. On installe php7 : interpréteur de scripts PHP
4. On installe phpmyadmin : client MySQL en PHP. **Mettez un mot de passe robuste !!!**

    # apt install apache2 php7.0 mysql-server phpmyadmin

Vérifiez que vous pouvez accéder à PHPMyAdmin : `http://adresseduserveur/phpmyadmin`

# Tout ceci mérite quelques explications

## Debian

* Debian sans interface graphique ne prend pas plus de 10Go. À 30Go, on est larges
* C’est un serveur de dev : l’interface graphique c’est sur votre PC. Le serveur doit servir des pages web, c’est tout. On peut installer un environnement de dev sur sa machine (mais on veut vous apprendre à utiliser un serveur plutôt que votre machine)
* Le fait de tout administrer en ligne de commande est un gain de performance: vous pouvez vous connecter en SSH même avec peu de débit

## Apache http server

![Logo Apache http server](https://httpd.apache.org/images/httpd_logo_wide_new.png)

Apache2 (Apache http server de son vrai nom) est un serveur web populaire sur Internet (~60% des serveurs webs). Il est connu mais lent et subit régulièrement des failles de sécurité.
Un serveur web reçoit des requêtes HTTP et renvoie des fichiers, notamment HTML.
Apache dispose de modules : dir, userdir, proxy et PHP.

**Apache n’interprête pas les scripts PHP lui-même. Apache est un « passe plats »**

Nginx est un serveur web léger de plus en plus populaire. Vous pouvez l’utiliser sur base de volontariat.

## MySQL

![Logo MySQL](https://www.mysql.fr/common/logos/logo-mysql-170x115.png)

MySQL est un serveur de base de données (BDD, ou Database alias DB). Un système de gestion de base de données (SGBD) basé sur SQL (Structured Query Language) vise à simplifier la gestion des données.
MySQL est une technologie populaire. Acquise récemment par Oracle (bouh, Oracle, caca), il existe un fork, MariaDB, qui pourrait remplacer MySQL dans les années à venir si Oracle venait à menacer MySQL.
MySQL est souvent critiqué pour des performances, ses erreurs de calcul (argl) ou son comportement erratique (re-argl)

Un SGDB, c’est plus simple et plus rapide que de tenter de gérer vous même l’enregistrement de vos données dans des fichiers textes. Imaginez gérer une base d’un million d’adresses mail dans un fichier texte.

MariaDB est le fork récent de MySQL. Il corrige de nombreux défauts de MySQL tout en étant compatible avec lui. C’est ce que Debian utilise maintenant.

PostgreSQL est un serveur de base de données robuste, complet et rapide de plus en plus populaire.

## PHP7

![Logo PHP](http://php.net/images/logos/php-med-trans.png)

PHP est un langage de programation scripté (ie non-compilé) orienté web unanimement reconnu comme inefficace, lent, bourré de failles de sécurité, sans cohérence syntaxique, manquant de fonctionnalités basiques … Logiquement, il domine donc les autres langages utilisés webs comme python ou ruby.
PHP génère de l’HTML, du CSS, du JavaScript ou des fichiers à transmettre (images, PDF …). En gros, PHP génère de l’HTML.

Apache, si le module PHP est activé, lance PHP pour interprêter la page et servir le code HTML généré au visiteur. Si le module PHP n’est pas activé, la page PHP est transmise telle quelle, avec le code PHP apparaissant alors aux yeux du visiteur.

PHP7 est la dernière version de PHP. La version précédente était PHP5. Il n’y a pas eu de PHP6 (comme il n’y a pas eu d’IPv5). Cherchez pas.

## PHPMyAdmin

![Logo PHPMyAdmin](https://wiki.phpmyadmin.net/wiki/images/b/b7/Pma_logo.png)

PHPMyAdmin (PMA pour les intimes) est un client MySQL visuel rédigé en PHP. Il vous permet de manipuler plus aisément vos données que si vous utilisez le client en ligne de commande.
Certaines commandes sont ainsi triviales (use <database>, select * from <table> …) et se font en un seul clic. Limite : si vous abusez de PHPMyAdmin, vous ne progresserez pas en SQL. Vivez PMA comme une assistance pour apprendre le SQL : découvrir « comment on fait », mieux voir où se situe une erreur dans une requête etc.

**Soyez conscients qu’ouvrir un accès à PHPMyAdmin fragilise la sécurité de votre serveur**
