# Cours HTML et CSS : diverses notions

©2018 Philippe Pary

## Sélecteurs CSS

https://flukeout.github.io/

On a plusieurs types de sélecteurs CSS. Voici les 3 principaux

### Sélecteurs par balise

    a {
        text-decoration:none;
    }

Ce code fait disparaître le soulignage des balise `a`.

On s’en sert assez peu, délaissant ces points au framework CSS et à l’environnement de travail.

### Sélecteurs par classe

    .center {
        text-align: center;
    }

Ce code centre le texte et les images d’un élèment de type block qui aurait l’attribut `class` avec comme valeur `center` (`class="center"`)

C’est le principal sélecteur, on s’en sert sans arrêt

### Sélecteur par id

    #ronaldo {
        display: none;
    }

Ce code cachera tout élément dont l’attribut `id` vaut `Ronaldo` (`id="ronaldo`)

**Les attributs id sont uniques, il ne peut pas y avoir deux fois id="ronaldo" dans une page**

On s’en sert pratiquement pas en CSS, même si vous serez très tenté de le faire. Les `id` servent surtout avec JavaScript

## Positionner les élèments

### texte, images … les élèments de type inline

Le texte et les images (qui par défaut sont traîtées comme du texte …), bref tous les élèments de type **inline** se positionnent horizontalement avec `text-align`

Verticalement … bah vous allez voir la suite

### propriétés CSS de position

https://developer.mozilla.org/fr/docs/Web/CSS/position

`position` permet de déplacer un élément. On a plusieurs valeurs possibles:

* `static` : comportement par défaut
* `relative`: déplace l’élément par rapport à ses positions par défaut
* `absolute` : positionné par rapport à la taille globale de la page. Va de paire avec les propriétés CSS `top`, `right`, `bottom` et `left`
* `fixed` : un peu comme absolute mais par rapport à la taille de l’élément parent
* `sticky` : un élément collé à la partie visible de la page. Voyez la MDN pour plus de détails


### propriétés CSS d’affichage

https://developer.mozilla.org/fr/docs/Web/CSS/display

On peut choisir de modifier l’affichage d’un élément via l’attribut `display`

Voici quelques valeurs, parmi tant d’autres:

* `block` : c’est le comportement des éléments de type … block (`div`, `p`, `h1` …)
* `inline` : c’est le comportement du texte (`img`, `span`…)
* `inline-block` : si si … vous vous documenterez …
* `flex` : http://flexboxfroggy.com/
* `grid` : http://cssgridgarden.com/#fr

## name droping en vrac

* `color` : la couleur de texte, en hexadécimal (`#ffa334` ou via un nom de couleur CSS, tous plus WTF les uns que les autres : `salmon`, `navajowhite`, `rebeccapurple` …) https://developer.mozilla.org/fr/docs/Web/CSS/Type_color
* `background-color` : la même chose avec la couleur de fond
* `border` : gestion des bordures https://developer.mozilla.org/fr/docs/Web/CSS/border
* `font-size`: taille de l’écriture https://developer.mozilla.org/fr/docs/Web/CSS/font-size
* `width` : largeur de l’élément. Les tailles peuvent être en pixels (`px`), pourcentages (`%`) et plein d’autres tailles https://developer.mozilla.org/fr/docs/Web/CSS/width
* `height` : pareil, pour la hauteur
* `box-sizing` : taille des éléments de type `block`. C’est un peu plus avancé, gardez en tête que ça existe : https://developer.mozilla.org/tr/docs/Web/CSS/box-sizing

Il existe des pseudo-classes, des transitions, des valeurs, des fonctions … on en cause plus tard mais vous pouvez déjà vous en servir ou chercher à vous en servir
