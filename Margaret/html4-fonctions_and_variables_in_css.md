# HTML5: fonctions, pseudo-éléments et variables en CSS3

2018, Philippe Pary

## Ouais parce qu’en fait c’est pas un cours d’HTML

Mais c’était plus raccord avec le titre de mes cours. Cette fois, c’est un cours purement sur CSS. CSS3.

## Les pseudos éléments

Les pseudos éléments sont des selecteurs qui vont concerner une zone précise de chaque éléments. Ils sont différents des pseudos classes que vous connaissez déjà (`:hover` par exemple)

En voici la liste stable:

* `::after` : tout ce qui se trouve après l’élément. Très utile avec `content:` pour ajouter un texte ou une image de fin (voyez l’icône des liens externes de Wikipédia)
* `::before` : bah pareil, mais avant
* `::first-letter` : vous êtes les moines copistes du XXIe siècle et vous aimez faire des jolies enluminures
* `::first-line` : je suppose que ça peut être utile …
* `::selection` : style pour les éléments surlignés par l’utilisateur (on s’en sert pour les définitions en roll over)
* `::cue` : les sous-titres de l’API WebVTT. Je ne m’en suis jamais servi, je ne saurais vous en parler

## Les fonctions CSS

CSS3 introduit de nombreuses fonctions. Ces dernières couvrent pas mal de sujets:
* taille des éléments
* colorisation et autres effets visuels
* transformation _physique_
* usage des variables

Vous en trouverez la liste ici : <https://developer.mozilla.org/en-US/docs/tag/CSS%20Function>

### Les fonctions de taille des éléments

#### fit-content()

<https://developer.mozilla.org/fr/docs/Web/CSS/fit-content>

`fit-content(x)` dit _utilise le maximum d’espace possible à condition de ne pas dépasser x_

On va être honnête, pour ma part j’en ai jamais eu l’utilité. J’attends qu’un ancien en ait eu l’utilité et me l’explique …

#### repeat()

<https://developer.mozilla.org/fr/docs/Web/CSS/repeat>

`repeat()` s’utilise avec `grid`. Vous l’avez vu avec les _grid garden_ (<http://cssgridgarden.com/>), ça permet de répeter une largeur donnée.

C’est tellement lié à `grid` que ce n’est pas vraiment une fonction… juste un raccourci d’écriture pour `grid` quoi. `grid` `grid` `grid`, `grid`, `grid` (<https://www.youtube.com/watch?v=anwy2MPT5RE>)
o

### Couleur et autres effets visuels

#### rgba()

<https://developer.mozilla.org/fr/docs/Web/CSS/Type_color>

Pas vraiment une fonction, `rgba(r, g, b, a)` est équivalente à `#RRGGBBAA`

Pas grand chose à dire. Il existe `hsl(h, s, l, a)` mais je n’y ai jamais rien compris :/

#### les fonctions de dégradés

Les dégradés seront du plus bel effet sur vos skyblogs. Plus sérieusement, bien utilisés et en exploitant la transparence, ça déchire tout

<https://developer.mozilla.org/en-US/docs/Web/CSS/linear-gradient>

`linear-grandiant(45deg, #FFFFAA, #FFAAFF)` forme un contraste allant du jaune poussin au rose peppa pig

<https://developer.mozilla.org/en-US/docs/Web/CSS/repeating-linear-gradient> et <https://developer.mozilla.org/en-US/docs/Web/CSS/repeating-radial-gradient>

#### Opacité

<https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/opacity>

On peut jouer sur l’opacité de tout un élément. **ATTENTION** même le texte sera affecté par la transparence !

#### Luminosité

<https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/brightness>

Très bon utilisé avec des effets de transition

#### Contraste

<https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/contrast>

Très bon utilisé avec des effets de transition

#### Saturation

<https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/saturate>

Vous aimez les transitions ?

#### Sepia

<https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/sepia>

Non, mais c’est pas fini cet inventaire foutraque ?

#### Inversion des couleurs

<https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/invert>

… sérieusement qui a besoin de ça dans la vraie vie ? C’est pas plus simple de mettre directement les bonnes couleurs ?

#### Niveau de gris

<https://developer.mozilla.org/fr/docs/Web/CSS/filter-function/grayscale>

Tout est possible

#### Rotation de teinte

<https://developer.mozilla.org/fr/docs/Web/CSS/filter-function/hue-rotate>

Pour faire du moche avec du beau

#### Flou

<https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/blur>

Une seule chose à dire <https://www.youtube.com/watch?v=WDswiT87oo8>

### Transformation physique

Là, il y en a vraiment trop. Je ne ferai pas d’inventaire exhaustif, je vais me concentrer sur quelques cas …

#### Rotation

<https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/rotate>

`rotate(90deg)` applique une rotation de 90° dans le sens des aiguilles d’une montre. `rotate(360deg)` ne sert à rien, j’espère ne rien vous apprendre :)

#### Perspective

<https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/perspective>

Je n’ai jamais eu vent de personne qui ne l’aie utilisé, pourtant j’en vois tout à fait l’utilité. On peut imaginer, avec des effets de transitions, faire des caroussels de ouf. Et je ne balance qu’une petite idée à la con, les _perspectives_ ouvertes par cette fonction sont infinies

#### Scale

<https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/scale>

`transform: scale(2)` double la taille de votre élément.

Très utile combiné à des effets de transition, sinon ça fait vraiment bizarre

#### Translate

<https://developer.mozilla.org/fr/docs/Web/CSS/transform-function/translate>

`transform: translate(10px, -5px)` déplacerait de 10 pixels vers la droite et 5 vers le haut votre élément.

#### Skew

<https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/skew>

C’est une déformation par un angle. Sans grand intérêt à mon sens vu toutes les fonctions dont on dispose (notamment `perspective()`)

#### Drop shadow

<https://developer.mozilla.org/en-US/docs/Web/CSS/filter-function/drop-shadow>

Vous devez certainement déjà connaître, ça donne une ombre portée à une élément.

### Calcul et variables

Les fonctions de calcul sont pratique et surtout, elles peuvent être combinées !

Par exemple : `min(calc(100% - --content-width), calc(80% - 16em))`. Wow.

#### calc()

<https://developer.mozilla.org/fr/docs/Web/CSS/calc>

`calc()` permet des opérations avec des unités de mesure différentes. Ultra utile au quotidien : ça fait combien 100% moins 200 pixels hein ?

#### min(), max(), clamp()

* <https://developer.mozilla.org/en-US/docs/Web/CSS/min>
* <https://developer.mozilla.org/en-US/docs/Web/CSS/max>
* <https://developer.mozilla.org/en-US/docs/Web/CSS/clamp>

Sans surprise `min()` et `max()` viennent trouver un minimum et un maximum entre deux valeurs, ce qui permet encore une fois de mélanger les unités 🍾

`clamp()` quand à lui trouve la valeur moyenne dans 3 valeurs données.

## Les variables

Une variable est un emplament où va se trouver une valeur qui change constamment. On dit qu’elle va varier, bref qu’elle est variable

_Souvent variable varie et bien fol qui s’y fie_ François Ier, informaticien précurseur (c’est à dire qui était informaticien avant l’invention de l’interface graphique)

À quoi ça sert d’avoir des choses qui varient tout le temps ? Si ça varie, ça bouge, si ça bouge, c’est dynamique. On va pouvoir créer des sites webs dynamiques qui changent si on clique ou si on rentre des choses dans des formulaires. Hourra ! 🎉🎉🎉

On peut créer une variable très simplement dans du CSS:

    .myDiv {
        --bg-color: red;
        --width: 250px;
    }

Évidement, ça n’a que peu d’intérêt sans utiliser un peu de JavaScript ou un langage de programmation. Mais dès que vous allez dynamiser vos pages, vous ne pourrez plus vous en passer :)

### et la fonction var()

<https://developer.mozilla.org/fr/docs/Web/CSS/var()>

`var(--bg-color, lime)` va créer une variable CSS, `var(--width)` va récupérer un valeur en cas de fonctions imbriquées



