## Petit cours autonome pour auditer la sécurité d’un code existant

Philippe Pary 2019

## Introduction

Jusqu’à présent, Daishi et moi-même vous avons appris à coder proprement. Ça vous amène à avoir du code contenant moins de failles de sécurité 💪

Mais il y en a quand même. Beaucoup même 😨

Ce cours vise à vous donner des bases sur l’audit de sécurité. Il est complété par des cours sur les tests unitaires et fonctionnels ✅

## Audit de sécurité ?

Auditer, c’est écouter les gens parler pour savoir ce qu’ils disent 👂

Pensez-y: Audit ➡ auditif

Le but est d’un audit de code est de trouver les éventuelles failles de sécurité, les erreurs logiques, vérifier si les tests sont adaptés.

Il y a donc plusieurs phases. À chacune de ces phases, il est intéressant d’établir un document écrit expliquant ce que vous avez constaté et commençant à établir des recommandations pour l’amélioration.

1. appréhender le logiciel pour connaître sa finalité, les technologies employées, sa structure … Ça consiste à lire la documentation, installer et essayer le logiciel et explorer son code.
2. lancer les tests unitaires et fonctionnels. Ne partez jamais du principe que d’autres les ont lancés avant vous… vous pourriez être surpris de l’ampleur du désastre 😮 tests qui échouent, tests qui ne se lancent même plus, tests non-adaptés…
3. lancer des tests d’audit de code. Ce sont des outils qui vont analyser le code ou réaliser des opérations sur le logiciel pour vérifier deux trois cas classiques amenant à des soucis de fonctionnement ou de sécurité.
4. essayer de rencontrer les développeurs du logiciel, échanger avec eux sur ce que vous venez de constater. Leurs retours seront précieux ! Et ne partez jamais du principe qu’ils ont mal fait leur travail. Derrière les pires logiciels se trouvent souvent les pires conditions de travail et des humains qui se sentent mal vis à vis de ce qu’ils ont livré.
(bon, il arrive parfois qu’on tombe sur des pieds nickelés, mais c’est rare)

## Mise en œuvre 

### 1. Appréhender le logiciel

Demandez à un autre élève un de ses exercices d’ECF: chronomètre, utilisation de l’API student … l’ECF SQL n’a pas vraiment d’intérêt par contre.

Sans échanger avec l’étudiant concerné:
* Lisez la documentation
* Tentez d’installer le logiciel
* Explorez le code source sans y passer plus d’une demi-heure

Prenez des notes simples sur ce que vous constatez, aussi bien sur un aspect positif que négatif. Par exemple:
* _la documentation est claire_ ou _la documentation n’apporte que des informations fonctionnelle, aucun détail technique_  …
* _l’installation du logiciel est compliquée voir impossible_
* _le logiciel ne fonctionne pas une fois mis en ligne_
* _le code source est bien commenté_

### 2. Lancer les tests unitaires et fonctionnels

Pas de mise en application de cette section. 

Elle est présente pour que vous ayez en tête que de tels tests existent et qu’ils sont importants.

Les tests unitaires et fonctionnels se présentent sous la forme de code qui va tester le comportement de votre logiciel. Par exemple vérifier que votre fonction de calcul d’un prix avec taxe soit correcte :

    /* testGetTaxedPrice checks if the 
     * function to get the taxed
     * price of a product is correct
     * this function is getTaxedPrice()
     * of a Product object */
    public function testGetTaxedPrice() {
        // We create a fake new product
        $prd = new Product();
        $prd->setReference('prd');
        $prd->setLabel('Product');
        // Tax is 10% which is written 0.1
        $prd->setTax(0.1);
        // Product's price without VAT
        // with VAT it should be 20
        $prd->setPriceSell(18.18181818);
        /* We test if the difference between 20
         * and the calculated Taxed Price
         * is lower to 0.0005
         */
        $this->assertTrue(($prd->getTaxedPrice() - 20) < 0.005,
                sprintf('Invalid taxed price %f', $prd->getTaxedPrice()));
    }

Voir https://framagit.org/pasteque/pasteque-server/blob/master/tests/Model/ProductTest.php

Des tests sont donc du code pour tester du code

### 3. Audit de code

Voici le cœur du cours ! 💚 Les outils d’audit de code

Nous allons les lancer manuellement 👇

Dans la vraie vie ils pourront être lancés par une plate-forme d’intégration continue tel Jenkins, Atlassian ou Circle.ci ⚙  
Ce sont des logiciels qui vont lancer eux-mêmes les tests et prévenir (mail, canal slack, SMS) quelqu’un en cas de soucis. Ils peuvent lancer les tests sur base régulière (tous les jours à 1H du matin) ou sur un événement (`push` sur un repo github)

Nous allons nous limiter à trois outils: un auditeur de code PHP, un auditeur de code front-end et des fichiers `linters`

#### Audit de code en PHP

Nous allons étudier PHPCS: PHP code sniffer

https://github.com/FloeDesignTechnologies/phpcs-security-audit

Pour installer le logiciel:

    $ git clone https://github.com/FloeDesignTechnologies/phpcs-security-audit.git
    $ cd phpcs-security-audit
    $ composer install

Simple, basique. Et maintenant nous allons auditer un de vos projets PHP 🔎

    $ vendor/bin/phpcs --extensions=php,inc,lib,module,info --standard=example_base_ruleset.xml /path/to/my/php/project/

Intéressez-vous à quelques uns des messages d’erreur en cherchant sur internet. Vous pouvez particulièrement chercher ce qui concerne:

* `RFI`: remote file insertion. Une vilaine faille qui peut vous faire rejoindre un botnet
* `eval`: execution en shell de code. Ça peut potentiellement prendre le contrôle de votre serveur

Notez aussi quelques points d’importance:
* les failles trouvées dans des dossiers de tests n’ont théoriquement pas d’importance: les tests ne sont presque jamais utilisé en production
* vous êtes impuissants avec les failles trouvées dans vos librairies externes comme `Twig`
* _débugguer c’est le verbe pour dire qu’on enlève des bugs. Le verbe pour dire qu’on ajoute des bugs s’appelle programmer_

Si le projet que vous avez choisi pour effectuer votre audit est en `PHP`, passez le à la moulinette.

👉 vous aurez à lancer cet audit pour les ECF PHP (API et Admin)

#### Audit front-end

Nous allons nous appuyer sur le scanner de `SecureApps`. C’est un logiciel en ligne, hébergé chez un tiers, c’est loin d’être la panacée.  
Mais il est gratuit et complet, ce qui est rare dans le domaine de l’audit de code …

https://scanner.secapps.com/

Lancez le scan un à un (il est trèèèèès long ⏱) sur vos projets en ligne

Intéressez-vous aux erreurs.  
Les premières affichées tiennent surtout de l’administration système que l’on aborde presque pas à PoppSchool. Les plus tardives nous intéressent énormément par contre…

* Injection SQL: l’horreur. C’est la possibilité pour l’attaquant d’accéder OKLM à votre base de données 😎 Avec le compte utilisateur de base de données de l’application, il peut tout faire. Oui, y compris des `DROP TABLES`. Ou injecter des faux articles dans un Wordpress contenant du code JavaScript pour miner du bitcoin (et encore, ça c’est gentil)
* 

### Les linters

`lint` en anglais, c’est une touffe d’herbe. Un `linter` est un logiciel de défrichage pour le code. Ça analyse _statiquement_ le code pour vous débusquer des erreurs facilement évitables:

* variables déclarées mais jamais utilisées
* erreurs d’indentation
* manières de procéder qui fonctionnent mais sont connues comme failles de sécurité

Ça vous évitera de constater vos _;_ ou accolades manquantes uniquement quand vous exécuterez votre code. Ça fait gagner du temps…

Bref, utiliser un `linter`  est une putain de bonne idée. Et bonne nouvelle : tous les IDE modernes intègrent des fonctions de `lint` !

#### Linter JavaScript

Pour Visual Studio Code, installez le plugin _ESLint_ de Dirk Baeumer.

Dans votre projet, installez un fichier `.eslint` qui contiendra les règles du logiciel.

Je vous propose d’installer l’ESLint d’AirBnB qui est très à la mode 

    $ npx install-peerdeps --dev eslint-config-airbnb-base

Puis créer un fichier `.eslintrc.js` (ou le modifier) à la racine de votre projet contenant le code suivant

    module.exports = { "extends": "airbnb-base" };

#### Linter PHP

Rien à faire, il est là de base ! VisualStudio embarque le linter automatique de `PHP` (que vous pourriez lancer en ligne de commande via `$ php -l monfichier.php`)

### Terminons l’audit

Allez voir le collègue dont vous avez étudié le code. Faites-lui part en bienveillance de vos notes prises lors de l’étude du logiciel. Prenez également des notes **vous ne devez juger le logiciel comme il était** : si votre collègue réalise des corrections séance tenantes, faites comme si elles n’avaient pas eu lieu.

Au besoin, recommencez la procédure depuis l’étape 1, par exemple si vous n’étiez pas parvenus à installer le logiciel…

Rédigez un joli document à partir de vos notes:

* Une introduction générale
* Le constat que vous avez réalisé
* Les recommandations que vous proposez, notées entre 1 et 5 (1 peu important, 5 très important)
* Une conclusion générale

Ne cherchez pas à être compliqué ou à faire des phrases. Ce document peut tout à fait tenir en une seule page. L’introduction peut être une seule phrase. N’oubliez pas que vous vous exercez sur des projets étudiants qui sont des projets très modestes !

Dans la vraie vie, vos audits seront bien plus importants !
