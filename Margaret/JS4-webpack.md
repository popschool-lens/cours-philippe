# Cours de Webpack

Loïc Pennequin & Philippe Pary, 2018

**NOTA BENE** L’article Webpack n’existe pas sur https://fr.wikipedia.org je vous invite à le créer en le traduisant depuis https://en.wikipedia.org !

## Qu’est-ce que Webpack ?

![logo webpack](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Webpack.png/214px-Webpack.png)

https://webpack.js.org/

Webpack est _bundler_ JavaScript.

Outre des fonctionnalités de programmations indisponibles en JavaScript (c’est même son but premier en fait…), il va surtout nous servir de grand chef d’orchestre pour combiner des tas de fonctionnalités de webpack ou d’outils externes :

* babel: support des vieux navigateurs
* minification: réduire la taille des fichiers
* sass: préprocesseur CSS que certains d’entre-vous pourraient kiffer
* environnement de développeement: prise en compte en live de vos modifications

Et ça c’est pour ce qu’on fera dans le cadre de notre cours. Les possibilités sont presque infinies

Webpack s’appuie sur NodeJS, un environnement pour exécuter du JavaScript en dehors d’un navigateur (dans un terminal par exemple)

## Installer node

On va commencer par installer Node sur nos machines :

https://nodejs.org/en/download/

## Installer le boilerplate PopSchool

Un _boilerplate_ c’est un fait-tout (une casserole quoi) qu’on va sortir dès qu’on se lance dans un projet d’importance.

Loïc Pennequin et moi-même avons préparé un boilerplate tout fait pour vous: https://github.com/loicpennequin/Pop-School---boilerplate-Webpack

Je vous invite à forker ce projet et à personnaliser ce boilerplate en fonction de vos goûts et de vos découvertes au travers du web :)

Un fois le projet cloné, vous avez une commande à lancer:

    $ npm install

Elle va installer tous les modules JavaScript (dont Webpack). On ne les a pas inclus dans le dépôt github car c’est plus léger sans ça,  et ça vous fait avoir les dernières mises à jour. Certains dév JS avancées peuvent aussi avoir certaines dépendances installées au niveau du système !

## Réfléchir

Allez jeter un œil aux fichiers. On repère:

* le dossier `dist` va contenir le site web à publier. Vous y mettrez les fichiers statiques (HTML, images…). ⚠ le dossier `dist/assets` va contenir le  code dynamique (JS et CSS) et sera vidé à chaque fois qu’on lancera le serveur
* le dossier `src` va contenir le code JS et CSS qui sera généré dynamiquement par Webpack
* le dossier `node_modules` qui contient les dépendances (webpack, babel et leurs propres dépendances)
* le fichier `package.json`, qu’on va ouvrir
* le fichier `webpack.config.js` qu’on va ouvir
* le fichier `.babelrc` qu’on va ouvrir

Résumons ce qui a été dit:

* `package.json` contient la configuration de `npm`: liste des dépendances et commandes de démarrage (section `scripts`)
* `webpack.config.js` contient la configuration de webpack et toutes les tâches qu’on veut avoir (minification, babel …)
* `.babelrc` contient la configuration babel pour la rétro-compatibilité. Au plus vous allez chercher loin, au plus votre JS sera obèse …

## Agir

Modifiez un peu les fichiers HTML, CSS et JS

Lancez le serveur de développement:

    $ npm run dev

Modifiez des fichiers JS/CSS, voyez les changements live. Si vous modifiez l’HTML, rechargez la page (il y a des astuces pour ne pas avoir à le faire, je vous laisse chercher :))

Observez aussi le contenu de `dist/assets`

Lancez la build pour la production:

    $ npm run build

Pas de navigateur qui se lance. Par contre le contenu de `dist` contient votre site web prêt à être publié !
