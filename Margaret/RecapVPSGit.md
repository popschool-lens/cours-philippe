# Récapitulatif VPS et Bash

Philippe Pary, 2018

## Les mots de passe qu’on a créé ou modifié …

* Le mot de passe utilisateur du VPS qui sert pour devenir administrateur avec `sudo` (ou vous connecter en SSH sans clef RSA)

* Le mot de passe `root` du VPS dont on se sert pour devenir `root` (alias administrateur) avec `su`. Honnêtement, vous pouvez l’oublier …

* Le mot de passe de `phpmyadmin` qui va vous servir de compte administrateur du système de base de données MySQL/MariaDB

* Le mot de passe de votre clef `RSA` qui peut vous servir à vous connecter à votre VPS ou à Github sans faire transiter de mot de passe sur Internet

## Ajoutons la clef RSA sur Github !

Allez sur Github.com, dans vos préférences, vous avez la possibilité d’ajouter une clef RSA.

Copiez le contenu du fichier `id_rsa.pub` qui est dans votre dossier `.ssh`

`.ssh` est un dossier caché situé à la racine de votre dossier personnel, vous pourriez avoir à les afficher (CTRL+H) pour le voir.

➡ tous les dossiers qui commencent par un `.` sont des dossiers cachés sur les système Unix (Gnu/Linux, MacOS, Android …)

Maintenant, vous pouvez utiliser Github en SSH. Le mot de passe qui vous sera demandé est celui de votre clef RSA maintenant (comme pour vous connecter à votre serveur)

## Installons un wordpress neuf sur le serveur !

1. Téléchargez Wordpress depuis https://fr.wordpress.org
2. Connectez-vous via FileZilla à votre serveur, et copiez le `.tar.gz` ou `.zip` dans le dossier `~/projets` / `/var/www/html`
3. Connectez vous à votre serveur avec SSH. Allez dans le dossier où se trouve l’archive Wordpress et décompressez :
    * `tar xzfv wordpress-X.X.X.tar.gz`
    * `unzip wordpress-X.X.X.zip`
4. Connectez vous à PHPMyAdmin et créez un nouvel utilisateur avec une base de données éponyme
5. Avec votre navigateur web, rendez vous sur `http://monnomdedomaine.com/wordpress` et lancez l’installation de Wordpress !
