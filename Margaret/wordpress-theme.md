# Création de thème Wordpress

Philippe Pary 2019

## 👶 Plus simple, parfois mieux

Avant d’envisager la création d’un thème, vous devez envisager la possibilité d’utiliser un thème existant que vous auriez simplement customisé via l’interface ou la création d’un child theme.

Ce sont les solutions _1 minute_ et _10 minutes_. Si vous avez _10 jours_ devant vous, alors on peut y aller !

## 10 minutes à perdre

Je vous souhaite un jour de créer des thèmes ex-nihilo.

En attendant, vous allez consacrer 10 minutes à trouver un thème au plus proche de ce que vous souhaitez faire. Typiquement, celui sur lequel vous vous seriez basé si vous faisiez un _child theme_

## Comment qu’on fait ?

1. Regardez la licence du thème que vous avez retenu
2. Si vous n’avez pas le droit de modifiez, trouvez en un autre
3. Copiez/collez le thème
4. Modifiez les en-têtes de `style.css` pour indiquer que c’est un nouveau thème

⚠ Mettez les informations sur le thème que vous copiez (nom de l’auteur, mail de l’auteur, nom du thème, version du thème, URL du thème) dans la section _Description_

Ça a deux avantages: d’abord c’est sympa, mais surtout ça permettra à ceux qui reprendraient le thème d’ici quelques années de savoir de quoi vous vous êtes inspiré.

Ça peut être utile si c’est pour corriger un bug: le thème que vous pompez l’a peut-être déjà corrigé :)

## Avantages et inconvénients du theme pompé

    ------------------------------------------------------
    |            ❤           |            💔             |
    ------------------------------------------------------
    | Rapide à créer          | Failles de sécurité      |
    | Customisation illimitée |  héritées du thème pompé |
    | Plus besoin du thème    | On n’a pas les mises à   |
    |  pompé                  |  jour du thème pompé …   |
    ------------------------------------------------------

## Dissection d’un thème wordpress …

📚 RTFM : <https://codex.wordpress.org/Stepping_Into_Templates>

### Style.css

C’est le vrai point d’entrée du thème. C’est dans le commentaire en haut de ce fichier qu’on va pouvoir définir le nom du thème et plein d’autres méta-informations.

Accessoirement, on y trouve le CSS …

### index.php

C’est le fichier qui sera chargé par défaut

### single.php

C’est le fichier chargé pour un article seul

### page.php

C’est le fichier chargé pour une page

👉 C’est le template de votre page d’accueil si c’est une page statique

### home.php

⚠ faux-ami ⚠

C’est le fichier chargé pour afficher une liste d’articles

👉 C’est le template de votre page d’accueil si c’est la liste des derniers articles

### header.php

C’est le doctype, le `<html>`, le `<head>` et le haut du du `<body>`

Vous ne devez pas y mettre les fichiers CSS ou Javascript, ils sont à charger via `functions.php`

### footer.php

C’est le bas du `<body>` et également le `</html>`

### et d’autres

`category.php` pour les catégories, `search.php`, `sidebar.php`, `archive.php`, `image.php`, `404.php` … sont quelques fichiers prévus par Wordpress

### Last but not least: functions.php

`functions.php` est un fichier très important.

* Vous y chargez vos fichiers CSS et JS
* Vous y définissez les zones où afficher les menus pour l’interface d’administration
* Vous y définissez les valeurs personnalisables dans l’éditeur clicodrome  pour les thèmes
* Vous pouvez y ajouter votre propre code (aggrégateur RSS, tirage au sort …)

etc.


## La boucle Wordpress

📚 RTFM : <https://codex.wordpress.org/The_Loop>

_The loop_ est un concept central de wordpress.

Et ça ressemble grosso-modo à ça 

    while (have_posts()) : the_post();
    …
    endwhile;

À l’intérieur de cette boucle, vous avez des fonctions pour interagir ou récupérer des informations de ce que vous affichez à l’aide de _tags_

* `the_content()`
* `the_author()`
* `the_category()`
…

📚 RTFM : <https://codex.wordpress.org/Template_Tags>

## Ajouter du code spécifique

Avec ce qu’on a vu, vous pouvez commencer à faire des choses, comme charger un template différent selon la catégorie de l’article:

    <?php
    if ( is_category( '9' ) ) {
        get_template_part( 'single2' ); // looking for posts in category with ID of '9'
    } else {
        get_template_part( 'single1' ); // put this on every other category post
    }
    ?>

Ce n’est qu’un exemple, Wordpress présente un lot de fonctions gigantesque que vous pouvez étendre avec du VanillaPHP. Vous n’avez par contre pas d’accès direct à la base de données … si vous voulez agir sur la base de données, il faut créer un plugin.

## Plus de documentation et des exemples

### Documentation

- [WP Marmite - YouTube - YouTube](https://www.youtube.com/channel/UCU_gPhU-eAI56oUeFzVyUUQ)
- [WordPress theme - The Anatomy, an Infographic - Yoast](https://yoast.com/wordpress-theme-anatomy/)
- [Template_Hierarchy.png (PNG Image, 1453 × 1443 pixels) - Scaled (52%)](https://codex.wordpress.org/images/1/18/Template_Hierarchy.png)
- [Theme Development « WordPress Codex](https://codex.wordpress.org/Theme_Development)
- [Including CSS & JavaScript | Theme Developer Handbook | WordPress Developer Resources](https://developer.wordpress.org/themes/basics/including-css-javascript/)
- [JavaScript Best Practices | Theme Developer Handbook | WordPress Developer Resources](https://developer.wordpress.org/themes/advanced-topics/javascript-best-practices/)
- [The Loop in Action « WordPress Codex](https://codex.wordpress.org/The_Loop_in_Action)
- [Class Reference/WP Query « WordPress Codex](https://codex.wordpress.org/Class_Reference/WP_Query)
- [Conditional Tags « WordPress Codex](https://codex.wordpress.org/Conditional_Tags)
- [Shortcode API « WordPress Codex](https://codex.wordpress.org/Shortcode_API)
- [Function Reference/get the tag list « WordPress Codex](https://codex.wordpress.org/Function_Reference/get_the_tag_list)
- [Function Reference/get the category list « WordPress Codex](https://codex.wordpress.org/Function_Reference/get_the_category_list)

### Exemples

With ❤ from Daishi:

* <https://github.com/jibundeyare/src-wordpress-minimal-theme>
* <https://github.com/jibundeyare/src-wordpress-blog-theme>

