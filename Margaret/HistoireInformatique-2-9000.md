# Culture générale informatique

Cours distillé au long cours, Philippe Pary 2018. CC-BY-SA (most recent)

## Objectifs

Ce cours doit introduire aux enjeux du droit d’auteur et du respect de la vie privée. Il présente donc l’informatique, et particulièrement son histoire, en mettant en avant les tensions qui existent autour de la notion de droit d’auteur et du droit à l’intimité et à la vie privée.

Il suit une trame essentiellement historique. La lecture de *hackers* de Steven Levy est chaudement recommandée.

## Partie 3 : les années 90 et 2000

Cette section de l’histoire informatique nous intéresse particulièrement : le logiciel libre émerge en même temps qu’Internet. Après la question du droit d’auteur dans les années 70-80, la question du respect de la vie privée émerge. Le code libre à disposition devient gigantesque

### La naissance des géants

Les années 90 et 2000 voient émerger de nouveaux géants de l’informatique.

IBM, le géant historique, subit un procès anti-trust aux États-Unis qui aboutit à son démentellement dans les années 80, entraînant notamment la fin de son monopole sur l’architecture PC. Microsoft profite de cet environnement, et des vissicitudes de son rival Apple, pour s’imposer en situation de monopole sur le marché des postes de travail (monopole acquis définitivement avec Windows 95). Ce monopole ne sera remis en cause qu’avec l’émergence des smartphones et tablettes (iOS et Android)

Apple, après des errements entre 1985 et 1995, s’impose comme le symbole de l’innovation auprès du grand public : iMac, iPod, iPhone, iPad, iTunes etc.

D’autres grandes sociétés informatiques apparaissent: CapGémini, Atos, Oracle, SAP …

À la fin des années 90 et au cours des années 2000 apparaissent les géants lié au web : Yahoo!, Google, Amazon, facebook etc.

### Structuration du logiciel libre

Le monde du partage se structure également. 1991 voit apparaître le noyau Linux. Avec l’appui de Pixar, Debian est fondé en 1993. Red Hat est fondé en 1994.

Les logiciels libres structurent l’apparition d’Internet (80% de parts de marché pour Apache au début des années 2000)

L’EFF est fondée en 1990, la FFII en 1999.

On peut citer le projet Mozilla (1998) et l’instauration de la Mozilla Foundation, née des ruines de NetScape ravagé par Microsoft, en 2003

On notera l’apparition d’IMDB en 1990, de Wikipédia en 2001 ou d’OpenStreetMap en 2004.

### Internet

Le WWW est créé en 1991 et entraîne une rapide démocratisation d’Internet (FDN créé en 1993, premières offres grand public par France Télécom en 1996, Free en 1999)

On passe de 8,5 millons d’internautes en 2000 (14,4% de la population) à 55 millions (84%) en 2016

À l’émergence d’Internet, nombre d’entreprises ont tenté d’imposer un système fermé, généralement inspiré du modèle économique du Minitel (Compuserve, AOL, MSN …)

Passé cette période, Internet croit comme un réseau décentralisé. Le milieu des années 2000 voit émerger la tendance centralisatrice actuelle.

### Lutte contre le terrorisme et méga-corporations contre vie privée

Le changement de braquet dans la lutte contre le terrorisme international à partir de 2001 entraîne de nombreuses mesures attentatoires aux libertés individuelles (Patriot Act aux USA)  

Dans le même temps, les géants du web, voyant passer des pans entiers de la vie privée des utilisateurs, se mettent à la valoriser (publicité, statistiques etc.) attirant les convoitises intéressées (développement scientifique, renseignements publics et privés)  

Ces dérives sont alors incomprises du grand public (aire pré-snowden, wikileaks fondé en 2006, la quadrature du net en 2008 etc.)

La prise de conscience émergera plus tard (révélations Snowden en 2013)

### On en retient

Les années 90 sont celles de la structuration économique de l’informatique et de l’émergence d’Internet & logiciel libre (les deux sont intrinsèquement liés)

Internet s’impose au courrant des années 2000 mais de nombreuses dérives ont émergé, la prise de conscience est plus tardive.
